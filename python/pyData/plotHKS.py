#coding:utf-8
import usefulMethods as um
try:
    import matplotlib
    matplotlib.use('TkAgg')
    import matplotlib.pyplot as plt
except:
    print "import matplotlib failed"

path = '/work/Masterarbeit/python/hksFiles/'

def plotLabels(plt, xlabel, ylabel, title, hksFile):
    plt.ticklabel_format(style='sci', axis='y', scilimits=(0, 100))
    # plt.xlabel(r"log(t)")
    # plt.ylabel(r"HKS")
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    if not title:
        plt.title(hksFile)
    else:
        plt.title(title)
    return plt

def calcDiffs(hks1, hks2):
    result = []
    for entry in range(0, len(hks1)):
        result.append((hks1[entry]-hks2[entry])/hks2[entry])
        # result.append((hks1[entry] - hks2[entry]))
    return result

def singlePlot(pixelList, tstart, tend, tinterval, eigval, eigvec, deletedInd, sigmamat):
    delCounter = 0
    minVal = 1000
    maxVal = 0
    yVal = 0.85 #### y-coord. of text in picture
    colorCounter = 0
    colors = ['r', 'b', 'g', 'c', 'm', 'y', 'k']
    if (sigmamat.ndim == 2):
        for i in range(0, len(sigmamat)): #y
            for j in range(0, len(sigmamat[0])): #x
                #print "betrachte pixel (" + str(j) + ", " + str(i) + ")"
                px, delCounter = hkSignature(j, i, 0, tstart, tend, tinterval, delCounter, eigval, eigvec, deletedInd, sigmamat)
                #maxVal = max(px[1])
                #print "maxVal = " + str(maxVal)
                if [j,i] in pixelList and px!=0:
                    #print "pixel nr (" + str(j) + ", " + str(i) + ")"
                    #print "min: " + str(min(px))
                    #print "max: " + str(max(px))
                    #print "ist in liste"
                    plt.plot(px, colors[colorCounter])
                    #plt.title('(%s, %s)'%(str(j), str(i)))
                    plt.figtext(0.7, yVal, r'(%s,%s)'%(str(j), str(i)), fontsize=15, color=colors[colorCounter])
                    colorCounter += 1
                    yVal -= 0.05
                    plt.xlim(0,tend+tend*0.04) #math.exp(tend)
                    plt.ylim((-0.001, 0.004))
                    plt.savefig("singlePlot.png") #("singlePlot-(%s,%s).png"%(str(j), str(i)))
                    #plt.clf()
    else: ### dim 3
        ### find min and max values of hks:
        for i in range(0, len(sigmamat)): #z
            for j in range(0, len(sigmamat[0])): #y
                for k in range(0, len(sigmamat[0][0])): #x
                    px, delCounter = hkSignature(k, j, i, tstart, tend, tinterval, delCounter, eigval, eigvec, deletedInd,
                                             sigmamat)
                    if [i, j, k] in pixelList:
                        #print "voxel nr (" + str(k) + ", " + str(j) + ", " + str(i) + ")"
                        # print "min: " + str( min(px[1]))
                        # print "max: " +str(max(px[1]))
                        # if max(px[1]) > maxVal:
                        #     maxVal = max(px[1])
                        # if min(px[1]) < minVal:
                        #     minVal = min(px[1])
                        plt.plot(px[0], px[1], colors[colorCounter])
                        plt.figtext(0.7, yVal, r'(%s,%s,%s)'%(str(i), str(j), str(k)), fontsize=15, color=colors[colorCounter])
                        colorCounter += 1
                        yVal -= 0.05
                        plt.xlim(0,tend+tend*0.04) #math.exp(tend)
                        plt.ylim((-0.001, 0.00017))
                        plt.savefig("singlePlot3D.png")

                # print str((j+1)*len(sigmamat[0][0])) + "pixel geschafft"


def plotHKSforPointsWithDiff(hksFile, diffs1, diffs2=[], title='', textLeft=False, labelsHKS=[], labelsDiffs1=[], labelsDiffs2=[]):
    #### read hks from file
    hks = um.readHKS(path+'pointsHKS'+hksFile)
    dim = hks[0]
    numOfVoxels = hks[1]
    if numOfVoxels == 0:
        print "Error: List was empty"
        return 0
    numOfHeatKernels = hks[2]
    timePoints = hks[3]

    minVal = 1000
    maxVal = 0
    yVal = 0.85  #### y-coord. of text in picture
    xVal = 0.7  #### x-coord. of text in picture
    if textLeft:
        xVal = 0.15
    colorCounter = 0
    colors = [(1,0.85,0),(0, 0.392157, 0),(0,0,0),(0,1,0),(0.627451, 0.12549, 0.941176),(0,0.7,1),(1,0,0)]#['r', 'b', 'g', 'c', 'm', 'y', 'k']
    if diffs2:
        f, (ax1, ax2, ax3) = plt.subplots(3, sharex=True)
    else:
        f, (ax1, ax2) = plt.subplots(2, sharex=True) #, sharex=True, sharey='row'

    for i in range(0, len(hks[4])):
        ax1.plot(timePoints, hks[4][i][1], color=colors[colorCounter])  # colors[colorCounter]
        maxOfHeatKernel = max(hks[4][i][1])
        minOfHeatKernel = min(hks[4][i][1])
        if  maxOfHeatKernel > maxVal:
            maxVal = maxOfHeatKernel
        if minOfHeatKernel < minVal:
            minVal = minOfHeatKernel
        # plt.title('(%s, %s)'%(str(j), str(i)))
        if (dim == 2):
            plt.figtext(xVal, yVal, r'(%s,%s)' % (str(hks[4][i][0][0]), str(hks[4][i][0][1])), fontsize=15, color=colors[colorCounter])
        if (dim == 3):
            if not labelsHKS:
                plt.figtext(xVal, yVal,
                        r'(%s,%s,%s)' % (str(hks[4][i][0][0]), str(hks[4][i][0][1]), str(hks[4][i][0][2])),
                        fontsize=15,
                        color=colors[colorCounter]) #
            else:
                plt.figtext(xVal, yVal,
                        r'%s' % (labelsHKS[i]), fontsize=15, color=colors[colorCounter])  #
        colorCounter += 1
        yVal -= 0.035
        plt.xlim(0, timePoints[-1] + timePoints[-1] * 0.04)  # math.exp(tend)
        ax1.set(ylim=((minVal - minVal * 0.08, maxVal + maxVal * 0.04)))

    ax1.set(ylabel=r"HKS")

    colorCounter = 0
    yVal = 0.42
    xVal = 0.5
    for entry in diffs1:
        hks1, hks2 = 0, 0
        for hksentry in hks[4]:
            if hksentry[0]==entry[0]:
                hks1 = hksentry[1]
            if hksentry[0]==entry[1]:
                hks2 = hksentry[1]
        hksdiff = calcDiffs(hks1, hks2)
        ax2.plot(timePoints, hksdiff, color=colors[colorCounter])
        if labelsDiffs1:
            plt.figtext(xVal, yVal,
                        r'%s' % (labelsDiffs1[colorCounter]), fontsize=15, color=colors[colorCounter])  #
        colorCounter+=1
        yVal -= 0.035
    ax2.set(ylabel=r"Differences", xlabel=r'log(t)')

    if diffs2:
        colorCounter = 0
        yVal = 0.4
        for entry in diffs2:
            hks1, hks2 = 0, 0
            for hksentry in hks[4]:
                if hksentry[0] == entry[0]:
                    hks1 = hksentry[1]
                if hksentry[0] == entry[1]:
                    hks2 = hksentry[1]
            hksdiff = calcDiffs(hks1, hks2)
            ax3.plot(timePoints, hksdiff, color=colors[colorCounter])
            if labelsDiffs2:
                plt.figtext(xVal, yVal,
                            r'%s' % (labelsDiffs2[colorCounter]), fontsize=15, color=colors[colorCounter])  #
            colorCounter += 1
            yVal -= 0.035
        ax3.set(ylabel=r"Differences", xlabel=r'log(t)')
    # f.subplots_adjust(hspace=0)
    plt.savefig("singlePlot"+hksFile+".png")  # ("singlePlot-(%s,%s).png"%(str(j), str(i)))
    plt.clf()



def plotHKSforPoints(hksFile, title='', textLeft=False, labelsHKS=[]):
    #### read hks from file
    hks = um.readHKS(path+'pointsHKS'+hksFile)
    dim = hks[0]
    numOfVoxels = hks[1]
    if numOfVoxels == 0:
        print "Error: List was empty"
        return 0
    numOfHeatKernels = hks[2]
    timePoints = hks[3]
    # print timePoints[-1]

    minVal = 1000
    maxVal = 0
    yVal = 0.85  #### y-coord. of text in picture
    xVal = 0.7  #### x-coord. of text in picture
    if textLeft:
        xVal = 0.15
    colorCounter = 0
    colors = [(1,0.85,0),(0, 0.392157, 0),(0,0,0),(0,1,0),(0.627451, 0.12549, 0.941176),(0,0.7,1),(1,0,0)]#['r', 'b', 'g', 'c', 'm', 'y', 'k']
    # colors = [(1,0.85,0),(0,1,0),(1,0,0),(0.627451, 0.12549, 0.941176)]
    for i in range(0, len(hks[4])):
        plt.plot(timePoints, hks[4][i][1], color=colors[colorCounter]) #colors[colorCounter]
        maxOfHeatKernel = max(hks[4][i][1])
        minOfHeatKernel = min(hks[4][i][1])
        if  maxOfHeatKernel > maxVal:
            maxVal = maxOfHeatKernel
        if minOfHeatKernel < minVal:
            minVal = minOfHeatKernel
        # plt.title('(%s, %s)'%(str(j), str(i)))
        if (dim == 2):
            plt.figtext(xVal, yVal, r'(%s,%s)' % (str(hks[4][i][0][0]), str(hks[4][i][0][1])), fontsize=15, color=colors[colorCounter])
        if (dim == 3):
            # plt.figtext(xVal, yVal,
            #             r'(%s,%s,%s)' % (str(hks[4][i][0][0]), str(hks[4][i][0][1]), str(hks[4][i][0][2])),
            #             fontsize=15,
            #             color=colors[colorCounter]) #
            if not labelsHKS:
                plt.figtext(xVal, yVal,
                            r'(%s,%s,%s)' % (str(hks[4][i][0][0]), str(hks[4][i][0][1]), str(hks[4][i][0][2])),
                            fontsize=15,
                            color=colors[colorCounter])  #
            else:
                plt.figtext(xVal, yVal,
                            r'%s' % (labelsHKS[i]), fontsize=15, color=colors[colorCounter])  #
        colorCounter += 1
        yVal -= 0.05
        plt.xlim(0, timePoints[-1] + timePoints[-1] * 0.04)  # math.exp(tend)
        plt.ylim((minVal-minVal*0.08, maxVal+maxVal*0.04))

    plotLabels(plt, r"log(t)", r"HKS", title, hksFile)
    plt.savefig("singlePlot"+hksFile+".png")  # ("singlePlot-(%s,%s).png"%(str(j), str(i)))
    plt.clf()





def plotHKSforPointsFromComplete(hksFile, pointList, titlefile, titleplot='', textLeft=False, figtextVals = []):
    #### read hks from file
    hks = um.readHKS(hksFile)
    dim = hks[0]
    numOfVoxels = hks[1]
    numOfHeatKernels = hks[2]
    timePoints = hks[3]

    minVal = 1000
    maxVal = 0
    yVal = 0.85  #### y-coord. of text in picture
    xVal = 0.65   #### x-coord. of text in picture
    if textLeft:
        xVal = 0.15
    colorCounter = 0
    colors = [(1,0,0),(0, 0.392157, 0),(0.627451, 0.12549, 0.941176),(0,0.7,1),(0,0,0),(0,1,0),(1,0.85,0),(0.5,0.5,0)]#['r', 'b', 'g', 'c', 'm', 'y', 'k']

    # for i in range(0, len(hks[4])):
    #     if list(hks[4][i][0]) in pointList:

    for j in range(0, len(pointList)):
        for i in range(0, len(hks[4])):
            if list(hks[4][i][0]) == pointList[j]:
                print "punkt " + str(hks[4][i][0]) + " war in liste"
                plt.plot(timePoints, hks[4][i][1], color=colors[colorCounter])
                maxOfHeatKernel = max(hks[4][i][1])
                minOfHeatKernel = min(hks[4][i][1])
                if  maxOfHeatKernel > maxVal:
                    maxVal = maxOfHeatKernel
                if minOfHeatKernel < minVal:
                    minVal = minOfHeatKernel
                # plt.title('(%s, %s)'%(str(j), str(i)))
                if (dim == 2):
                    plt.figtext(xVal, yVal, r'(%s,%s)' % (str(hks[4][i][0][0]), str(hks[4][i][0][1])), fontsize=15, color=colors[colorCounter])
                if (dim == 3):
                    if not figtextVals:
                        plt.figtext(xVal, yVal,
                                    r'(%s,%s,%s)' % (str(hks[4][i][0][0]), str(hks[4][i][0][1]), str(hks[4][i][0][2])),
                                    fontsize=15,
                                    color=colors[colorCounter])
                    else:
                        plt.figtext(xVal, yVal,
                                    figtextVals[j],
                                    fontsize=15,
                                    color=colors[colorCounter])
                colorCounter += 1
                yVal -= 0.05
    # print maxVal
    plt.ticklabel_format(style='sci', axis='y', scilimits=(0, 0))
    plt.xlim(0, timePoints[-1] + timePoints[-1] * 0.04)  # math.exp(tend)
    plt.ylim(0, maxVal+maxVal*0.04) ##((minVal-minVal*0.08, maxVal+maxVal*0.04))
    plt.title(titleplot)

    plt.xlabel(r"log(t)" )
    plt.ylabel(r"HKS")
    plt.savefig("plots/singlePlotFromComplete" + titlefile + ".png")  # ("singlePlot-(%s,%s).png"%(str(j), str(i)))
    print ""
    plt.close()
    # plt.clf()

texts = ['Nose', 'Chin', 'Forehead', 'Eye socket', 'Cheekbone', 'Mastoid Process']

# s = '1_2_826_0_1_3680043_2_1125_1_41151057872594974555753564137181835spacing08-08-08'
# s = '1_2_826_0_1_3680043_2_1125_1_41151057872594974555753564137181835spacing08-08-08only1s'
# plotHKSforPointsFromComplete(path+'completeHKSsigmamat'+s, [[149, 26, 206],[154, 62, 84],[148, 39, 275],[115, 43, 240],[92, 84, 180],[83, 170, 179]],titlefile=s,titleplot='835spacing08-08-08')  #

s = '1_2_826_0_1_3680043_2_1125_1_81585748112514251817903140826310781spacing08-08-08'
# s = '1_2_826_0_1_3680043_2_1125_1_81585748112514251817903140826310781spacing08-08-08lessDense'
s = '1_2_826_0_1_3680043_2_1125_1_81585748112514251817903140826310781spacing08-08-08only1s'
# plotHKSforPointsFromComplete(path+'completeHKSsigmamat'+s, [[150, 32, 117],[158, 73, 13],[148, 33, 177],[116, 47, 154],[100, 83, 93],[95, 176, 99]],titlefile=s,titleplot='Data Set 5 with Uniform Density', figtextVals=texts)

s = '1_2_826_0_1_3680043_2_1125_1_52973447532876540775084415735195358spacing07-07-07'
# s = '1_2_826_0_1_3680043_2_1125_1_52973447532876540775084415735195358spacing07-07-07lessDense'
s = '1_2_826_0_1_3680043_2_1125_1_52973447532876540775084415735195358spacing07-07-07only1s'
# plotHKSforPointsFromComplete(path+'completeHKSsigmamat'+s, [[178, 43, 161],[170, 73, 24],[181, 47, 215],[139, 57, 196],[107, 92, 123],[102, 192, 134]],titlefile=s,titleplot='Data Set 4 with Uniform Density', figtextVals=texts)

s = '1_2_826_0_1_3680043_2_1125_1_43544977205268791933160390680029048spacing077-077-077'
s = '1_2_826_0_1_3680043_2_1125_1_43544977205268791933160390680029048spacing077-077-077only1s'
# plotHKSforPointsFromComplete(path+'completeHKSsigmamat'+s, [[153, 23, 140],[160, 70, 17],[151, 33, 191],[115, 48, 183],[87, 94, 106],[75, 185, 119]],titlefile=s,titleplot='Data Set 7 with Uniform Density', figtextVals=texts)

s = '1_2_826_0_1_3680043_2_1125_1_6684150113536839605716867710154046spacing072-072-072'
s = '1_2_826_0_1_3680043_2_1125_1_6684150113536839605716867710154046spacing072-072-072only1s'
# plotHKSforPointsFromComplete(path+'completeHKSsigmamat'+s, [[221, 44, 140],[193, 105, 17],[234, 42, 211],[184, 45, 185],[128, 95, 131],[103, 190, 148]],titlefile=s,titleplot='Data Set 3 with Uniform Density', figtextVals=texts)

s = '1_2_826_0_1_3680043_2_1125_1_44650512280840673505894811634773432spacing063-063-063'
s = '1_2_826_0_1_3680043_2_1125_1_44650512280840673505894811634773432spacing063-063-063only1s'
# plotHKSforPointsFromComplete(path+'completeHKSsigmamat'+s, [[185, 29, 157],[188, 86, 17],[184, 32, 211],[142, 49, 199],[104, 105, 126],[110, 212, 131] ],titlefile=s,titleplot='Data Set 1 with Uniform Density', figtextVals=texts)

s = '1_2_826_0_1_3680043_2_1125_1_54286138000349764725037931753924822spacing076-076-076'
s = '1_2_826_0_1_3680043_2_1125_1_54286138000349764725037931753924822spacing076-076-076only1s'
# plotHKSforPointsFromComplete(path+'completeHKSsigmamat'+s, [[158, 29, 166],[157, 68, 16],[157, 33, 206],[124, 47, 194],[99, 81, 113],[88, 182, 105]],titlefile=s,titleplot='Data Set 6 with Uniform Density', figtextVals=texts)

s = '1_2_826_0_1_3680043_2_1125_1_78514869166001886767502631794273095spacing065-065-065'
# s = '1_2_826_0_1_3680043_2_1125_1_78514869166001886767502631794273095spacing065-065-065lessDense'
s = '1_2_826_0_1_3680043_2_1125_1_78514869166001886767502631794273095spacing065-065-065only1s'
# plotHKSforPointsFromComplete(path+'completeHKSsigmamat'+s, [[187, 38, 143],[185, 91, 18],[184, 35, 191],[146, 50, 179],[112, 100, 117],[107, 209, 124]],titlefile=s,titleplot='Data Set 2 (Uniform) with 939,246 Voxels', figtextVals=texts)  #Data Set 2 with 939,246 Voxels

s = "1_2_826_0_1_3680043_2_1125_1_78514869166001886767502631794273095spacing0658"
# s = "1_2_826_0_1_3680043_2_1125_1_78514869166001886767502631794273095spacing0658only1s"
plotHKSforPointsFromComplete(path+'completeHKSsigmamat'+s, [[185, 36, 140],[184, 93, 16],[184, 35, 180],[147, 49, 177],[110, 100, 116],[106, 205, 123]],titlefile=s,titleplot='Data Set 2 (Varied) with 904,850 Voxels', figtextVals=texts)  # [124, 207, 139]
s = "1_2_826_0_1_3680043_2_1125_1_78514869166001886767502631794273095spacing06535"
s = "1_2_826_0_1_3680043_2_1125_1_78514869166001886767502631794273095spacing06535only1s"
# plotHKSforPointsFromComplete(path+'completeHKSsigmamat'+s, [[186, 37, 141],[184, 93, 17],[186, 36, 180],[147, 51, 179],[111, 102, 117],[108, 209, 124]],titlefile=s,titleplot='Data Set 2 (Uniform) with 919,789 Voxels', figtextVals=texts)

a=[[178,43,161],[168,76,20],[181,47,215],[106,79,145],[242,84,134],[154,53,194],[207, 58, 188]]
b=['Nose',      'Chin',     'Forehead',   'Cheekbone L','Cheekbone R','Eye Socket L','Eye Socket R']
# plotHKSforPoints(s, title=' ', labelsHKS=b)
# plotHKSforPointsWithDiff(s, [[(106,79,145),(242,84,134)],[(154,53,194),(207, 58, 188)]], diffs2=[[(106,79,145),(178,43,161)], [(154,53,194),(178,43,161)]] , title=' ', labelsHKS=b, labelsDiffs1=['Cheekbones', 'Eye Sockets'], labelsDiffs2=['Cheekbone L - Nose', 'Eye Socket L - Nose'])
# plotHKSforPointsWithDiff(s, [[(106,79,145),(242,84,134)],[(154,53,194),(207, 58, 188)], [(106,79,145),(154,53,194)], [(106,79,145),(178,43,161)], [(178,43,161),(168,76,20)]], title=' ', labelsHKS=b, labelsDiffs1=['Cheekbones', 'Eye Sockets', 'Cheekbone L - Eye Socket L', 'Cheekbone L - Nose', 'Nose - Chin'])
#plotHKSforPointsFromComplete('/work/Masterarbeit/python/hksFiles/completeHKSsigmamat'+s, [[170,21,122],[191,38,152],[152,37,157],[174,30,178],[113,57,120],[214,61,106]]) ##gehört zu: 430spacing08-08-08
#plotHKSforPointsFromComplete('/work/Masterarbeit/python/hksFiles/completeHKSsigmamat'+s, [[6,0,1],[13,14,1],[47,0,1],[54,14,1]])
# plotHKSforPointsFromComplete('/work/Masterarbeit/python/hksFiles/completeHKSsigmamat'+s, [[149,32,117],[159,74,13],[148,33,182],[98,68,111],[208,64,118],[125,45,153],[181,50,161]])
# plotHKSforPointsFromComplete('/work/Masterarbeit/python/hksFiles/completeHKSsigmamat'+s, [[200,300,200],[200,200,200],[200,201,200],[200,250,200]])
# plotHKSforPointsFromComplete('/work/Masterarbeit/python/hksFiles/completeHKSsigmamat'+s, [[200,200,100],[200,200,200],[200,200,199],[200,200,150]])



"""##################    AMBOSS   ##################"""

s = "amboss"
# s = "ambossDensity"
# s = "ambossDensity0-54"
# plotHKSforPointsFromComplete(path+'completeHKSsigmamat'+s, [[258, 240, 177],[258, 240, 223],[231, 256, 223], [231, 256, 177], [231, 224, 223], [231, 224, 177]],s+'Spitzen', titleplot='Anvil Small with Density, Tip Points')
# plotHKSforPointsFromComplete(path+'completeHKSsigmamat'+s, [[240, 240, 190],[240, 240, 210]],titlefile=s+'Mitte',titleplot='Anvil Small with Density, Centre Points')  #  , textLeft=True
# plotHKSforPointsFromComplete(path+'completeHKSsigmamat'+s, [ [258, 240, 177],[231, 224, 177],[240, 240, 190],[237, 245, 210]],titlefile=s+'MIX',titleplot='Small Anvil with Uniform Density', figtextVals=['Tip', 'Tip', 'Surface Centre', 'Close to Surface Centre'])  # ,[258, 240, 223],[231, 256, 223], [231, 256, 177], [231, 224, 223],
# plotHKSforPoints(s+'Mitte', title='Anvil with Density, Centre', textLeft=True)
# plotHKSforPoints(s+'Spitzen', title='Anvil with Density, Tips',)

#
# s = "ambossRoty45"
# plotHKSforPointsFromComplete(path+'completeHKSsigmamat'+s, [[237, 240, 230],[270, 240, 197],[217, 256, 210],[250,256,177],[217, 224, 210],[250, 224, 177]],s+'Spitzen')  #,[230, 223, 178]
# # plotHKSforPointsFromComplete(path+'completeHKSsigmamat'+s, [[247, 240, 193],[233, 239, 207]],s+'Mitte')
#
# s = 'ambossRefl1-1-1-0'
# plotHKSforPointsFromComplete(path+'completeHKSsigmamat'+s, [[211, 236, 203],[241, 267, 188],[231, 212, 195],[261, 243, 180],[232, 226, 224],[263, 256, 209]],s+'Spitzen')
# # plotHKSforPointsFromComplete(path+'completeHKSsigmamat'+s, [[234, 233, 203],[247, 246, 197]],s+'Mitte')


"""##################    AMBOSS BIG   ##################"""

s = ""
s = "ambossBig"
s = "ambossBigDensity"
s = "ambossBigDensity547"
# plotHKSforPointsFromComplete(path+'completeHKSsigmamat'+s, [[297, 240, 130],[297, 240, 270],[212, 191, 130], [212, 191, 270], [212, 288, 270], [212, 289, 130]],titlefile=s+'Spitzen',titleplot='Big Anvil with Uniform Density, Tip Points')  #
# plotHKSforPointsFromComplete(path+'completeHKSsigmamat'+s, [[297, 240, 130],[297, 240, 270],[212, 191, 130], [212, 191, 270], [212, 288, 270], [212, 289, 130]],titlefile=s+'Spitzen',titleplot='Big Anvil with Varied Density, Tip Points')  #
# plotHKSforPointsFromComplete(path+'completeHKSsigmamat'+s, [[297, 240, 130],[297, 240, 270],[212, 191, 130], [212, 191, 270], [212, 288, 270], [212, 289, 130], [240, 240, 170],[240, 240, 230]],titlefile=s+'MIX',titleplot='Big Anvil with Varied Density, Tip Points')  # [224, 267, 234]
# plotHKSforPointsFromComplete(path+'completeHKSsigmamat'+s, [[240, 240, 170],[240, 240, 230]],s+'Mitte',titleplot='Big Anvil with Varied Density, Centre Points')  #
# plotHKSforPointsFromComplete(path+'completeHKSsigmamat'+s, [[240, 240, 170],[240, 240, 230]],s+'Mitte','Big Anvil with Uniform Density, Centre Points')
# plotHKSforPoints(s+'Mitte', title='Big Anvil with Density, Centre')
# plotHKSforPoints(s, title='Big Anvil with Density, Mix')
# plotHKSforPoints(s+'Spitzen', title='Big Anvil with Density, Tips', textLeft=True)


s = "ambossBigRotx90"
s = "ambossBigDensityRotx90"
# plotHKSforPointsFromComplete(path+'completeHKSsigmamat'+s, [[296, 270, 239],[212, 270, 190],[212, 270, 288], [296, 130, 239], [212, 130, 288], [212, 130, 190]],s+'Spitzen')
# plotHKSforPointsFromComplete(path+'completeHKSsigmamat'+s, [[240, 230, 239],[240, 170, 239]],s+'Mitte')

s = "ambossBigRoty45"
s = "ambossBigDensityRoty45"
# plotHKSforPointsFromComplete(path+'completeHKSsigmamat'+s, [[340, 290, 380],[401, 240, 320],[341, 191, 380], [242, 289, 281], [303, 239, 221], [242, 191, 281]],s+'Spitzen')
# plotHKSforPointsFromComplete(path+'completeHKSsigmamat'+s, [[290, 240, 290],[334, 240, 331]],s+'Mitte')   # 332, 236, 331

s = "ambossBigRotz30Roty56"
s = "ambossBigDensityRotz30Roty56"
# plotHKSforPointsFromComplete(path+'completeHKSsigmamat'+s, [[379, 299, 463],[435, 299, 382],[407, 386, 421], [291, 383, 344], [264, 298, 386], [320, 298, 303]],s+'Spitzen')
# plotHKSforPointsFromComplete(path+'completeHKSsigmamat'+s, [[325, 327, 365],[374, 330, 399]],s+'Mitte')

s='ambossBigRotx13'
s='ambossBigDensityRotx13'
# plotHKSforPointsFromComplete(path+'completeHKSsigmamat'+s, [[297, 294, 317],[212, 248, 328],[211, 344, 305], [296, 263, 181], [212, 310, 170], [212, 215, 192]],s+'Spitzen')
# plotHKSforPointsFromComplete(path+'completeHKSsigmamat'+s, [[239, 273, 219],[242, 281, 278]],s+'Mitte')

s='ambossBigRotx13Roty5'
s='ambossBigDensityRotx13Roty5'
# plotHKSforPointsFromComplete(path+'completeHKSsigmamat'+s, [[323, 294, 332],[240, 247, 350],[237, 344, 327], [312, 263, 196], [225, 313, 193], [228, 216, 214]],s+'Spitzen')
# plotHKSforPointsFromComplete(path+'completeHKSsigmamat'+s, [[258, 271, 240],[265, 285, 298]],s+'Mitte')

"""##################    BUNNY   ##################"""

s = 'bunnySmall'
# plotHKSforPointsFromComplete(path+'completeHKSsigmamat'+s, [[11, 48, 25],[0, 46, 9], [35, 28, 3],[32, 1, 9], [19, 0, 8],[26, 9, 49]],titlefile=s+'MIX',titleplot='Bunny Small', figtextVals=['left ear', 'right ear', 'snout', 'left paw', 'right paw', 'tail'])
# plotHKSforPointsFromComplete(path+'completeHKSsigmamat'+s+'discont', [[11, 48, 25],[0, 46, 9], [35, 28, 3],[32, 1, 9], [19, 0, 8],[26, 9, 49]],titlefile=s+'MIX',titleplot='Bunny Small', figtextVals=['left ear', 'right ear', 'snout', 'left paw', 'right paw', 'tail'])


s = 'bunnyBig'
# plotHKSforPointsFromComplete(path+'completeHKSsigmamat'+s, [[33, 145, 78],[2, 141, 28], [106, 82, 12],[98, 3, 29], [62, 2, 21],[73, 17, 146]],titlefile=s+'MIX',titleplot='Bunny Big', figtextVals=['left ear', 'right ear', 'snout', 'left paw', 'right paw', 'tail'])
# plotHKSforPointsFromComplete(path+'completeHKSsigmamat'+s+'discont', [[33, 145, 78],[2, 141, 28], [106, 82, 12],[98, 3, 29], [62, 2, 21],[73, 17, 146]],titlefile=s+'MIX',titleplot='Bunny Big', figtextVals=['left ear', 'right ear', 'snout', 'left paw', 'right paw', 'tail'])




""" best k matches for uniform vs non uniform densities plotted on CT data"""

voxelspac063 = 918591
voxelspac065 = 939246
voxelspac072 = 913979
voxelspac07 = 905640
voxelspac08 = 934806
voxelspac076 = 929342
voxelspac077 = 916512

unispac063 = [18,147,21,4,21,39]
unispac065 = [4,300,270,28,240,6]
unispac072 = [8,1400,22,56,74,95]
unispac07 = [10,59, 85, 4, 136, 29]
unispac08 = [6, 83, 8, 28, 51, 28]
unispac076 = [11,500,38,11,92,38]
unispac077 = [5,67,157,8,12,197]

spac063 = [34,2000,133,85,365,246]
spac065 = [4, 650, 65, 17, 357, 13]
spac072 = [25, 2000, 2, 310, 382, 76]
spac07 = [11,47,11,30,88,28]
spac08 = [4, 2000, 40, 83, 104, 94]
spac076 = [21, 2000, 117, 8, 201, 245]
spac077 = [9, 950, 116, 59, 500, 500]

voxellist = [voxelspac063, voxelspac065, voxelspac072, voxelspac07,voxelspac08,voxelspac076, voxelspac077 ]
unilist = [unispac063, unispac065,unispac072, unispac07,unispac08, unispac076, unispac077 ]
nonunilist = [spac063, spac065, spac072,spac07,  spac08,spac076,spac077]

a = [float(nonunilist[i][0])/unilist[i][0] for i in range (7)]
b = [float(nonunilist[i][1])/unilist[i][1] for i in range (7)]
c = [float(nonunilist[i][2])/unilist[i][2] for i in range (7)]
d = [float(nonunilist[i][3])/unilist[i][3] for i in range (7)]
e = [float(nonunilist[i][4])/unilist[i][4] for i in range (7)]
f = [float(nonunilist[i][5])/unilist[i][5] for i in range (7)]

labels = ['Nasal Bone', 'Chin', 'Forehead', 'Eye Socket', 'Cheekbone', 'Mastoid Process']


l = [a,b,c,d,e,f]
t = [1,2,3,4,5,6,7]


# colors = ['ro-', 'bs-', 'g^-', 'y*-', 'c>-', 'm+-', 'gx-']
# # plt.plot(t, t, 'r--', t, t, 'bs', t, t, 'g^')
# for k in range(6):
#     plt.plot(t, l[k], colors[k], label=labels[k])
# plt.plot(t,[1,1,1,1,1,1,1],'k--')
# legend = plt.legend(loc=(0.2,0.5), shadow=True, fontsize='x-large')
# plt.axis([0.5, 7.5,0,27])
# plt.show()

