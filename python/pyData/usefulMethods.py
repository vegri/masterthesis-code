#import pickle
import numpy as np
import struct
from array import array
import math
import binvox_rw
try:
    import scipy.ndimage
except:
    print "scipy not available"
from testMatrizen import sigmamatX2

pi = math.pi

def createRandomSigmamat(lowestInt, largestInt, xDim, yDim, zDim):
    return np.random.random_integers(lowestInt, largestInt, (zDim, yDim, xDim))

def sortEigenvalAndVec(val, vec):
    idx = val.argsort()
    val = val[idx]
    vec = vec[:,idx]
    return val, vec

def openFile(name):
    with open(name, "rb") as file_:
        return np.load(file_)

def saveList(name, data):
    f = open(name, 'w')
    f.write("\n".join(str(x) for x in data))
    f.close()

def saveFeatList(name, data, isInt=True):
    with open(name, 'w') as file:
        file.write(str(len(data)/4))
        file.write('\n')
        for i in range(0, len(data),4):
            file.write(str(data[i]) + " " + str(data[i+1]) + " " + str(data[i+2]) + " " + str(data[i+3]))
            file.write('\n')


def readList(name, isInt):
    mylist = []
    with open(name) as f:
        if isInt:
            for line in f:
                if line.strip():
                    n = int(float(line))
                    mylist.append(n)
        else:
            for line in f:
                if line.strip():
                    n = float(line)
                    mylist.append(n)
        return mylist


def saveBinList(name, data, isInt=True):

    with open(name, 'wb') as stream:
        length = array('i', [len(data)])
        length.tofile(stream)
        if isInt:
            a = array('i', data)
        else:
            a = array('f', data)
        a.tofile(stream)


def readBinList(name, isInt=True):
    with open(name, 'rb') as stream:
        lenarray = array('i')
        lenarray.fromfile(stream, 1)
        result = array('f')
        if isInt:
            result = array('i')
        result.fromfile(stream, lenarray[0])  # lenarray+1
        print "len result:"
        print len(result)
        return list(result)


def saveFeatRec(name, featlist):
    with open(name+'.feat', 'wb') as stream:
        length = len(featlist) #(dim+1)*
        lengtharr = array('i', [length]) #for every entry 3 or 2 coords (int) + 1 feature (int)
        lengtharr.tofile(stream)
        a = array('i', featlist)
        a.tofile(stream)

def readFeatRec(name):
    with open(name+'.feat', 'rb') as stream:
        lenarray = array('i')
        lenarray.fromfile(stream, 1)
        result = array('i')
        result.fromfile(stream, lenarray[0])  # lenarray+1
        return lenarray, list(result)    

def saveToHKS(name, hks):
    with open(name+'.hks', 'wb') as stream:
        ### save dimension (integer)
        dimension = array('i', [hks[0]])
        dimension.tofile(stream)

        ### save number of non-zero voxels (integer)
        numberOfVoxelsConsidered = array('i', [hks[1]])
        numberOfVoxelsConsidered.tofile(stream)

        ### save number of heatkernels per voxel (defined through time interval) (integer)
        numberOfHeatKernelsPerVoxel = array('i', [hks[2]])
        numberOfHeatKernelsPerVoxel.tofile(stream)

        ### save time steps (t0, t1, ...., tend) (array of float, length is numberOfHeatKernelsPerVoxel)
        timePoints = array('d', hks[3])
        timePoints.tofile(stream)

        for voxel in range(0, hks[1]): #numberOfVoxelsConsidered
            ### save voxel coordinates x,y (z if 3D) (2-3 integers)
            xValue = array('i', [hks[4][voxel][0][0]])
            yValue = array('i', [hks[4][voxel][0][1]])
            xValue.tofile(stream)
            yValue.tofile(stream)

            if hks[0] == 3:
                zValue = array('i', [hks[4][voxel][0][2]])
                zValue.tofile(stream)

            ### save heatkernel signature vor voxel x/y/z
            heatKernelSig = array('d', hks[4][voxel][1])
            heatKernelSig.tofile(stream)

        ### save min/max value of ALL heatkernel signatures
        minVal = array('d', [hks[5]])
        maxVal = array('d', [hks[6]])
        minVal.tofile(stream)
        maxVal.tofile(stream)

def readHKS(name):
    hks = []
    with open(name+'.hks', 'rb') as stream:
        dim = array('i')
        dim.fromfile(stream, 1)
        numOfVoxels = array('i')
        numOfVoxels.fromfile(stream, 1)
        numOfKernels = array('i')
        numOfKernels.fromfile(stream, 1)

        timePoints = array('d')
        timePoints.fromfile(stream, numOfKernels[0])

        for i in range(0, numOfVoxels[0]):
            xVal = array('i')
            yVal = array('i')
            zVal = array('i')
            xVal.fromfile(stream, 1)
            yVal.fromfile(stream, 1)
            coords = (xVal[0], yVal[0])
            if dim[0]==3:
                zVal.fromfile(stream, 1)
                coords = (xVal[0], yVal[0], zVal[0])
            heatKernel = array('d')
            heatKernel.fromfile(stream, numOfKernels[0])
            hks.append((coords, list(heatKernel)))

        minVal = array('d')
        maxVal = array('d')
        minVal.fromfile(stream, 1)
        maxVal.fromfile(stream, 1)

    return dim[0], numOfVoxels[0], numOfKernels[0], list(timePoints), hks, minVal[0], maxVal[0]

def saveEigvec(name, vec):
    with open(name, 'wb') as stream:
        dimension = array('i', [len(vec)])
        numberOfVecs = array('i', [len(vec[0])])
        dimension.tofile(stream)
        numberOfVecs.tofile(stream)
        # print dimension
        # print numberOfVecs
        # print vec

        for row in range(0, len(vec)): #dimension
            vecRow = array('d', vec[row])
            vecRow.tofile(stream)


def readEigvec(name):
    vec = []
    with open(name, 'rb') as stream:
        dim = array('i')
        dim.fromfile(stream, 1)
        numOfVecs = array('i')
        numOfVecs.fromfile(stream, 1)
        for row in range(0, dim[0]):
            result = array('d')
            result.fromfile(stream, numOfVecs[0])  # lenarray+1
	    vec.append(list(result))
        return dim[0], numOfVecs[0], vec    	


def calcEigenstuffSVD(operator, save=False, filenameVal="eigvals.txt", formatVal='%1.10f', filenameVec="eigvals.txt", formatVec='%1.5f'):
    vec, val, vec2 = np.linalg.svd(operator, full_matrices=True)
    if save:
        np.savetxt(filenameVal, val, fmt=formatVal)
        np.savetxt(filenameVec, vec, fmt=formatVec)
    return val, vec

def formatValues(val, fmt='.10f'):
    for i in range(0, len(val)): #format(1.2345, '.2f')
        val[i] = format(val[i], fmt)
        return val

def matrixVecMult(_x, _y, _z, mat):
    x = mat[0][0]*_x + mat[0][1]*_y + mat[0][2]*_z
    y = mat[1][0]*_x + mat[1][1]*_y + mat[1][2]*_z
    z = mat[2][0]*_x + mat[2][1]*_y + mat[2][2]*_z
    return x, y, z

def rotMatX(angle):
    cos = math.cos(angle)
    if cos < 1e-12 and cos > -1e-12:
        cos = 0
    sin = math.sin(angle)
    if sin < 1e-12 and sin > -1e-12:
        sin = 0
    return [[1,0,0],[0, cos, -sin], [0, sin, cos]]

def rotMatY(angle):
    cos = math.cos(angle)
    if cos < 1e-12 and cos > -1e-12:
        cos = 0
    sin = math.sin(angle)
    if sin  < 1e-12 and sin > -1e-12:
        sin = 0
    return [[cos,0,sin],[0,1,0],[-sin, 0, cos]]

def reflectionMat(x,y,z): ## only for axes going through the origin
    return [[(2*x*x-1), 2*y*x, 2*z*x],[2*x*y, (2*y*y-1), 2*z*y],[2*x*z, 2*y*z, (2*z*z-1)]]


def reflectionMat2(x,y,z, a, b, c, d): ## reflection on surface of form ax+by+cz=d
    lambd = (d-a*x-b*y-c*z)/(1.*(a*a+b*b+c*c))
    intersectX = x+lambd*a
    intersectY = y+lambd*b
    intersectZ = z+lambd*c
    reflectedX = 2*intersectX - x
    reflectedY = 2*intersectY - y
    reflectedZ = 2*intersectZ - z
    return reflectedX, reflectedY, reflectedZ

def rotateWithScipy(filename, destfilename, angle, axes=(1,0)):
    """
    rotates around planes, not axis. axis: 0-z; 1-y; 2-x (just how the access to a 3d-array is, array[z][y][x].
    meaning rotation around 1,0 is the y-z plane
    rot um x-achse: (1,0)
    rot um y-achse: (2,0)
    rot um z-achse: (1,2)
    """
    print "axes: " + str(axes)
    data = readBinList('/work/Masterarbeit/python/'+filename)
    xdim, ydim, zdim = data[0], data[1], data[2]
    data = data[3:]
    data = np.array(data)
    data = data.reshape(zdim, ydim, xdim)
    result = scipy.ndimage.interpolation.rotate(data, angle, axes=axes)  # , mode='nearest'
    print "dimensions: "
    print result.shape
    forQT, forHKS = [], [result.shape[2],result.shape[1],result.shape[0]]
    counternonzero = 0
    for z in range(0,result.shape[0]):
        for y in range(0,result.shape[1]):
            for x in range(0,result.shape[2]):
                val = result[z][y][x]
                if val != 0:
                    counternonzero += 1
                    forQT.extend([str(x) + ' ' + str(y) + ' ' + str(z) + ' ' + str(val)])
                    forHKS.append(val)
                else:
                    forHKS.append(0)

    with open('/work/Masterarbeit/qtTest/' + destfilename + '.txt', 'w') as f:
        f.write(str(counternonzero))
        f.write('\n')
        for entry in forQT:  # writetofile:
            f.write(entry)
            f.write('\n')

    saveBinList('/work/Masterarbeit/python/' + destfilename, forHKS, isInt=True)

    return forHKS, forQT, counternonzero

def readAndConvertBinVox(name):
    forQT,forHKS = [], []
    counterNonzero = 0
    with open(name+'.binvox', 'rb') as f:
        model = binvox_rw.read_as_3d_array(f)
        print "translate: " + str(model.translate)
        print "dimensions: " + str(model.dims)
        dim = model.dims[0]
        forHKS = [dim, dim, dim]
        for z in range(0, dim):
            for y in range(0, dim):
                for x in range(0, dim):
                    if model.data[z][y][x]:
                        counterNonzero += 1
                        forQT.extend([str(x) + ' ' + str(y) + ' ' + str(z) + ' ' + '1'])
        forHKS.extend(list(model.data.astype(int).reshape(dim**3)))
    print "nonzero voxels: " + str(counterNonzero)
    with open('/work/Masterarbeit/qtTest/' + name + '.txt', 'w') as f:
        f.write(str(counterNonzero))
        f.write('\n')
        for entry in forQT:  # writetofile:
            f.write(entry)
            f.write('\n')
    saveBinList('/work/Masterarbeit/python/' + name, forHKS, isInt=True)
#
# # l=[[96,104,96],[104,104,104],[100,100,95],[105,100,100]]
l=[[120,70,120],[120,170,120],[85,74,80],[78,81,77],[81,163,78]]
# # t = [0, 171, 0] # rotX90
# t = [0, 0, 63]  # rot Y45
# # t= [0, 171, 0]  # refl-0-1-0-0
# t = [191, 191, 192]
#
# for entry in l:
#     # x,y,z=reflectionMat2(entry[0], entry[1], entry[2], 1,1,1,0)     #  rotMatY(pi*0.25)
#     x, y, z = matrixVecMult(entry[0], entry[1], entry[2], rotMatY(pi*0.25))
#     # print entry, int(round(x))+t[0],int(round(y))+t[1],int(round(z))+t[2]
#     print entry, int(round(x)), int(round(y)), int(round(z))



# readAndConvertBinVox('bunnySmall')
# readAndConvertBinVox('bunnyBig')



"""test scipy rotate: density values preserved?"""
# print sigmamatX2
# l = list(sigmamatX2.flatten())
# l.insert(0, len(sigmamatX2))
# l.insert(0, len(sigmamatX2[0]))
# l.insert(0, len(sigmamatX2[0][0]))
# print l
# saveBinList('sigmamatX2', l)
# l1, l2, _ = rotateWithScipy('sigmamatX2', 'sigmamatX2rotx90', 90)
# result = scipy.ndimage.interpolation.rotate(sigmamatX2, 90, mode='nearest')
# print "result:"
# print result
# print l1
# print l2