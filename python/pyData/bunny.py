
# from test import makeHistogramm
import numpy as np
import matplotlib.pyplot as plt
import usefulMethods as um






def saveIfBunny(x,y,z, data, counterNonzero, counterzero, forHKS, resultlist, stepsize):
    if (data[z][y][x] > 35000):
        counterNonzero += 1
        resultlist.extend([str(x/stepsize) + ' ' + str(y/stepsize) + ' ' + str(z/stepsize) + ' ' + '1'])
        forHKS.append(1)
        # forHKS[z][y][x] = 1
        return counterNonzero, counterzero
    else:
        forHKS.append(0)
        counterzero += 1
        return  counterNonzero, counterzero

def reflectBunny(x,y,z, minX, minY, minZ, data, counterNonzero, resultHKS, resultQT):
    if (data[z][y][x] > 35000):
        counterNonzero += 1
        # xNew,yNew,zNew = um.matrixVecMult(x,y,z, um.reflectionMat(1,0,0))
        xNew, yNew, zNew = um.reflectionMat2(x, y, z, 0, 1, 0, 0)
        if xNew<minX:
            minX = xNew
        if yNew < minY:
            minY = yNew
        if zNew < minZ:
            minZ = zNew
        # resultQT.extend([str(xNew) + ' ' + str(yNew) + ' ' + str(zNew) + ' ' + '1'])
        resultHKS.extend([(xNew,yNew,zNew)])
    return counterNonzero, minX, minY, minZ

def rotateBunny(x,y,z, data, counterNonzero, resultHKS, resultQT):
    if (data[z][y][x] > 35000):
        counterNonzero += 1
        xNew,yNew,zNew = um.matrixVecMult(x, y, z, um.rotMatY(um.pi * 0.25))
        resultQT.extend([str(xNew) + ' ' + str(yNew) + ' ' + str(zNew) + ' ' + '1'])
        resultHKS.extend([(xNew, yNew, zNew)])
    return counterNonzero

def makeForHKSsavable(changedList, transVec=0):
    result = np.zeros((ydim, ydim, ydim),int)
    for entry in changedList:
        if transVec == 0:
            result[int(entry[2])][int(entry[1])][int(entry[0])] = 1
        else:
            result[int(entry[2] + transVec[2])][int(entry[1]) + transVec[1]][int(entry[0]) + transVec[0]] = 1
    result = result.reshape(ydim**3)
    return result

def atestOfmakeForHKSsavable(changedList, resultQT):
    changedList = np.array(changedList)
    changedList = changedList.reshape((ydim,ydim,ydim))
    for z in range(0, len(changedList)):
        for y in range(0, len(changedList[0])):
            for x in range(0, len(changedList[0][0])):
                if changedList[z][y][x]==1:
                    resultQT.extend([str(x) + ' ' + str(y) + ' ' + str(z) + ' ' + '1'])
    return resultQT




# # filename = 'bunny/230'
# # data = np.fromfile(filename,dtype=np.uint16)
# # a = data.reshape(512,512)
# # plt.imshow(a)
# # plt.show()
# #

# name = 'bunnyRot90xAxis'
# # name = 'bunnyRot45yAxis'
# # name = 'bunnyRefl'
name='bunny'
filename='bunny'

# rotatedBunny = []



def createImagesFromCT(dirname):
    for i in range(1, 362):
        data = np.fromfile(dirname + str(i), dtype=np.uint16)
        allData.extend(data)
    #     """for saving pictures"""
        a = data.reshape(512,512)
        plt.imshow(a)
        plt.show()
        fig = plt.gcf()
        fig.savefig('bunny' + str(i))
        plt.clf()

def writeFilesQTandHKS(filename, forQT, forHKS, counterNonzero, writeForQT=True, writeForHKS=True):
    if writeForQT:
        with open('/work/Masterarbeit/qtTest/' + filename + '.txt', 'w') as f:
            f.write(str(counterNonzero))
            f.write('\n')
            for entry in forQT:  # writetofile:
                f.write(entry)
                f.write('\n')
    if writeForHKS:
        um.saveBinList('/work/Masterarbeit/python/' + filename, forHKS, isInt=True)

def readBunny(filename):
    result = um.readBinList('/work/Masterarbeit/python/'+filename)
    xdim, ydim, zdim = result[0], result[1], result[2]
    result = result[3:]
    result = np.array(result).reshape(zdim, ydim, xdim)
    return result


def createDataFromCT(filename, dirname='bunny/', stepsize=2, xdim=255, ydim=410, zdim=230, writeForQT=True, writeForHKS=True):
    """
    creates data for bunnyhead
    """
    counterNonzero = 0
    counterzero = 0
    allData = []
    for i in range(1, 362):
        data = np.fromfile(dirname + str(i), dtype=np.uint16)
        allData.extend(data)

    a = np.array(allData)
    allData = a.reshape(361, 512, 512)
    forHKS = [xdim/2+1,ydim/2,zdim/2]
    forQT = []
    # forHKS2 = [ydim,ydim,ydim]
    maIX, maIY, maIZ = -1000, -1000, -1000 # max indices
    mIX, mIY, mIZ = 1000, 1000, 1000 # min indices
    result = np.zeros((zdim,ydim,xdim), dtype=int)

    for z in range(0, zdim,stepsize):
        for y in range(0, ydim,stepsize):
            for x in range(0, xdim,stepsize):
                if x > 192:
                    if y < 320 and y > 210 and z < 115 and z > 5:
                        counterNonzero, counterzero = saveIfBunny(x, y, z, allData, counterNonzero, counterzero, forHKS,
                                                                  forQT,stepsize)
                    else:
                        forHKS.append(0)
                elif y > 309:
                    if x < 105 and x > 25 and z > 5 and z < 95 and y < 381: # and y < 381:
                        counterNonzero, counterzero = saveIfBunny(x, y, z, allData, counterNonzero, counterzero, forHKS,
                                                                  forQT,stepsize)

                    elif x < 97 and x > 65 and z > 19 and z < 64 and y >= 381 and y < 385:
                        counterNonzero, counterzero = saveIfBunny(x, y, z, allData, counterNonzero, counterzero, forHKS,
                                                                  forQT,stepsize)
                    elif x < 81 and x > 71 and z > 29 and z < 58 and y == 385:
                        counterNonzero, counterzero = saveIfBunny(x, y, z, allData, counterNonzero, counterzero, forHKS,
                                                                  forQT,stepsize)
                    elif x < 79 and x > 73 and z > 31 and z < 56 and y == 386:
                        counterNonzero, counterzero = saveIfBunny(x, y, z, allData, counterNonzero, counterzero, forHKS,
                                                                  forQT,stepsize)
                    elif x < 77 and x > 75 and z > 33 and z < 54 and y == 387:
                        counterNonzero, counterzero = saveIfBunny(x, y, z, allData, counterNonzero, counterzero, forHKS,
                                                                  forQT,stepsize)
                    elif x < 75 and x > 77 and z > 35 and z < 52 and y == 388:
                        counterNonzero, counterzero = saveIfBunny(x, y, z, allData, counterNonzero, counterzero, forHKS,
                                                                  forQT,stepsize)
                    elif x < 73 and x > 79 and z > 37 and z < 50 and y >= 389 and y<395:
                        counterNonzero, counterzero = saveIfBunny(x, y, z, allData, counterNonzero, counterzero, forHKS,
                                                                  forQT,stepsize)

                    else:
                        forHKS.append(0)
                else:
                    counterNonzero, counterzero = saveIfBunny(x, y, z, allData, counterNonzero, counterzero, forHKS, forQT,stepsize)
                # counterNonzero = rotateBunny(x,y,z,allData, counterNonzero, rotatedBunny, writetofile)
                # counterNonzero, mIX, mIY, mIZ = reflectBunny(x, y, z, mIX, mIY, mIZ, allData, counterNonzero, rotatedBunny, writetofile)
    print "counternonzero: " + str(counterNonzero)
    print "done with saving QT, now saving for HKS"
    writeFilesQTandHKS(filename, forQT, forHKS, counterNonzero, writeForQT=writeForQT, writeForHKS=writeForHKS)

print "done with rotating/reflecting"


"""necessary for translating when rotating/reflecting with own methods"""
# # transVec = [0,0,0]
# # if mIX<0:
# #     transVec[0] = abs(mIX)
# # if mIY<0:
# #     transVec[1] = abs(mIY)
# # if mIZ<0:
# #     transVec[2] = abs(mIZ)
# # print "transvec:"
# # print transVec
# #
# # # com = (int(float(comX/counterNonzero)),int(float(comY/counterNonzero)),int(float(comZ/counterNonzero)))
# # # comNew = um.matrixVecMult(com[0], com[1], com[2], um.reflectionMat(1,0,0))
# # # transVec = (com[0]-comNew[0], com[1]-comNew[1], com[2]-comNew[2])
# #
# # forHKS2 = list(makeForHKSsavable(rotatedBunny, transVec))
# # atestOfmakeForHKSsavable(forHKS2, writetofile)
# # forHKS2.insert(0, ydim)
# # forHKS2.insert(1, ydim)
# # forHKS2.insert(2, ydim)
# #
# # print mIX, mIY, mIZ
# #
# # print "len forhks2: " + str(len(forHKS2))
# # print "done with actualising forHKS"
# #
# print "len writetofile: " + str(len(writetofile))
# # print "len rotatedBunny: " + str(len(rotatedBunny))


"""rotate with scipy """
# name = "bunnySmall"
name = "bunnyBig"
filename = name+'Rotx90'
um.rotateWithScipy(name,filename, 90, axes=(1,0))
# filename = name+'Roty45'
# um.rotateWithScipy(name, filename, 45, axes=(2,0))

# interfilename = name+'Rotz30'
# filename = name+'Rotz30Roty56'
# um.rotateWithScipy(name,interfilename,30, axes=(1,2))
# um.rotateWithScipy(interfilename,filename,56, axes=(2,0))

# interfilename = name+'Rotx13'   ### ACHTUNG gibt mit mode nearest bei scipy rotate abgefahrene artefakte!
# filename = name+'Rotx13Roty5'
# um.rotateWithScipy(name,interfilename,13)
# um.rotateWithScipy(interfilename,filename,5, axes=(2,0))


# createDataFromCT('bunnyFromCT')


# # data = np.array(forHKS[3:])
# # data = data.reshape((zdim, ydim,xdim))




