#coding: utf-8

import unittest
import hks as hks
import createOperator3DSparse as cosp
import createOperator3DNumpy as conp
import createOperator2D as cop
import usefulMethods as um
# import mainFile as mf
# import calcEigenstuff as ceig
from scipy import sparse, io
from scipy.sparse.linalg import eigsh
from numpy import linalg as LA
from testMatrizen import *
import os
import math


global path
path = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
# print "path:"
# print path


# def calcHK(val, vec, t, index):
#     hk = 0
#     for i in range(0, len(vec[0])):
#         hk += math.exp(val * math.exp(t)) * vec[index][i]
#     return hk

class CreateHKStest(unittest.TestCase):

    @staticmethod
    def calcHK(val, vec, t, index):
        hk = 0
        trace = 0
        for i in range(0, len(val)):
            if val[i] < -1e-13:
                tmp =  math.exp(val[i] * (math.exp(t)-1))
                # print tmp
                trace += tmp
                hk += tmp * vec[index][i]*vec[index][i]
        return hk/trace

    def test_determineRowNumber(self):
        self.assertEqual(hks.determineRowNumber(1, 3, 3, sigmamatX4), 2 * 4 * 4 - 1)
        self.assertEqual(hks.determineRowNumber(1, 3, 0, sigmamatX4), 7)
        self.assertEqual(hks.determineRowNumber(0, 2, 2, sigmamatX4), 20)

        self.assertEqual(hks.determineRowNumber(1, 3, 1, sigmamatX3), 22)

    def test_determNumOfDelRows(self):
        delInd = [0,1,2,3,4,6,9]
        self.assertEqual(hks.determNumOfDelRows(0, 0, delInd), (1, True))
        self.assertEqual(hks.determNumOfDelRows(5, 5, delInd), (5, False))
        self.assertEqual(hks.determNumOfDelRows(5, 4, delInd), (5, False))
        self.assertEqual(hks.determNumOfDelRows(6, 5, delInd), (6, True))
        self.assertEqual(hks.determNumOfDelRows(7, 0, delInd), (6, False))


    def test_heatKernelSignature(self):
        """ rechne hks 'per hand' aus und vergleiche mit gespeicherten hks (achtung, einschränkung bei eigenwerten! """
        s='sigmamatX7'
        val = um.readList(path+'/eigvals/eigval'+s, isInt=False)
        dim, numofvecs, vec = um.readEigvec(path+'/eigvecs/eigvec'+s)
        # print val
        # print vec
        result = um.readHKS(path+'/hksFiles/completeHKS'+s)
        # print result[3]
        timepoints, hks = [], []
        for i in np.arange(0, 5, 0.02):
            timepoints.append(i)
        for i in range(0, 4):
            hks.append([CreateHKStest.calcHK(val,vec,t,i) for t in timepoints])
        # for entry in result[4]:
        #     print entry[1]
        # print ""
        # for entry in hks:
        #     print entry
        for i in range(0, len(hks)):
            self.assertTrue(np.allclose(result[4][i][1], hks[i]))

        s = 'sigmamatX6'
        val = um.readList(path + '/eigvals/eigval' + s, isInt=False)
        dim, numofvecs, vec = um.readEigvec(path + '/eigvecs/eigvec' + s)
        result = um.readHKS(path + '/hksFiles/completeHKS' + s)
        timepoints, hks = [], []
        for i in np.arange(0, 10, 0.05):
            timepoints.append(i)
        for i in range(0, 100):
            hks.append([CreateHKStest.calcHK(val, vec, t, i) for t in timepoints])
        for i in range(0, len(hks)):
            self.assertTrue(np.allclose(result[4][i][1], hks[i]))

        s = '1_2_826_0_1_3680043_2_1125_1_81585748112514251817903140826310781spacing08-08-08only1s'
        val = um.readList(path + '/eigvals/eigval' + s, isInt=False)
        dim, numofvecs, vec = um.readEigvec(path + '/eigvecs/eigvec' + s)
        result = um.readHKS(path + '/hksFiles/completeHKSsigmamat' + s)
        timepoints, hks = [], []
        timepoints, hks = [], []
        for i in np.arange(0, 10.5, 0.07):
            timepoints.append(i)
        for i in range(0, 100):
            hks.append([CreateHKStest.calcHK(val, vec, t, i) for t in timepoints])
        for i in range(0, len(hks)):
            self.assertTrue(np.allclose(result[4][i][1], hks[i]))

    @staticmethod
    def runTests():
        suite = unittest.TestLoader().loadTestsFromTestCase(CreateHKStest)
        unittest.TextTestRunner(verbosity=2).run(suite)


class CreateOperatorTest(unittest.TestCase):

    def test_completeCOOMatrix(self):
        data = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
        rows = [0, 0, 0, 0, 1, 1, 1, 2, 2, 3]
        cols = [0, 1, 2, 3, 1, 2, 3, 2, 3, 3]
        cosp.completeCOOMatrix(data, rows, cols)
        op = sparse.coo_matrix((data, (rows, cols)))
        test = np.array([[1,2,3,4],[2,5,6,7],[3,6,8,9],[4,7,9,10]])
        self.assertTrue(np.allclose(op.toarray(), test))

    def test_deleteZeroColsCOO(self):
        cols = [1,3,5,3,4,5,5]
        cols2 = [0,1,3,1,2,3,3]
        cosp.deleteZeroColsCOO(cols, [0,2,6,7])
        self.assertEqual(cols, cols2)

        cols = [0, 2, 3, 5, 6, 2, 3, 5, 6, 3, 5, 6, 5, 6, 6]
        cols2 = [0, 1, 2, 3, 4, 1, 2, 3, 4, 2, 3, 4, 3, 4, 4]
        cosp.deleteZeroColsCOO(cols, [1,4])
        self.assertEqual(cols, cols2)
	
    def test_getCoordinates(self):
        x, y, z = 13, 6, 3
        testarray = []
        getCoordArray = []
        for zdim in range(0,z):
            for ydim in range(0,y):
                for xdim in range(0,x):
                    testarray.append((xdim,ydim,zdim))
        for line in range(0, x*y*z):
            xgc, ygc, zgc = cosp.getCoordinates(line,x, y)
            getCoordArray.append((xgc, ygc, zgc))
        # print testarray
        # print getCoordArray
        self.assertEqual(testarray, getCoordArray)
		
	


    def test_createSparseOperatorCOO(self):
        """ Achtung: die methoden aus conp sind noch nicht an die neue version von cosp angepasst, wo vakuum voxel komplett ignoriert werden, also vergleich nicht möglich """
        currentmatrix = sigmamatX7
        datax7 = [-4.0, 2.0, 2.0, -2.0, -2.0, 2.0, -4.0, 2.0, 2.0, 2.0]
        rowsx7 = [0, 0, 0, 1, 2, 2, 3, 1, 3, 3]
        colsx7 = [0, 1, 3, 1, 2, 3, 3, 0, 0, 2]

        # _, operator = conp.createOperator3D(currentmatrix)
        delInd, data, rows, cols = cosp.createSparseOperatorCOO(currentmatrix, 2, 2, 2)

        # result = sparse.coo_matrix((data, (rows, cols)))
        self.assertTrue(np.allclose(datax7, data))
        self.assertTrue(np.allclose(rowsx7, rows))
        self.assertTrue(np.allclose(colsx7,cols))

        currentmatrix = sigmamatX4
        _, data, rows, cols = cosp.createSparseOperatorCOO(currentmatrix, 4, 4, 2)
        # _, operator = conp.createOperator3D(currentmatrix)
        result = sparse.coo_matrix((data, (rows, cols)))
        # print result.toarray()
        for i in range(0,len(result.toarray())):
            # print i
            self.assertTrue(np.allclose(result.toarray()[i], operatorX4[i]))
        #self.assertTrue(np.allclose(result.toarray(), operatorX4))

        # currentmatrix = sigmamatX5
        # _, data, rows, cols = cosp.createSparseOperatorCOO(currentmatrix)
        # result = sparse.coo_matrix((data, (rows, cols)))
        # # _, operator = conp.createOperator3D(currentmatrix)
        # self.assertTrue(np.allclose(result.toarray(), operator))

    @staticmethod
    def runTests():
        suite = unittest.TestLoader().loadTestsFromTestCase(CreateOperatorTest)
        unittest.TextTestRunner(verbosity=2).run(suite)


# class CalcEigenstuffTest(unittest.TestCase):
#
#     def test_calcEigenstuff(self):
#
#         currentmatrix = sigmamatX7
#         _, operator = conp.createOperator3D(currentmatrix)
#         val, vec = LA.eig(operator)
#         val, vec = um.sortEigenvalAndVec(val, vec)
#         val = val[1:]                   ## delete largest eigenvalue
#         vec = np.delete(vec, 0, 1)      ## delete vector belonging to largest eigenvalue
#         # print val
#         # print vec
#         # print ""
#         val2, vec2 = ceig.makeEigen("sigmamatX7")
#         # print "now sorting"
#         # print val2
#         # print vec2
#         """problem an dieser stelle: die vorzeichen. np.negative bringt auch nichts, weil die vz nicht komplett
#         umgehkehrt sind, sondern nur teilweise. also muss ich eigentlich nur den betrag vergleichen? also alle negativen
#         vz auf positiv setzen? muss wohl zweimla durch die arrays durchlatschen um das zu erreichen, aber sehe gerade keine
#         andere möglichkeit... ideen was man nutzen könnte:
#         http://stackoverflow.com/questions/10335090/numpy-replace-negative-values-in-array
#         anderes problem: will ich wirklich nur die beträge vergleichen? sind die vorzeichen nicht auch wichtig? dann müsste
#         ich aber spalte für spalte vergleichen, was recht zeitaufwändig ist. andererseits will ich eh nur kleine matrizen vergleichen,
#         weil alles andere zu lange dauert (also die berechnung der werte über la.eig)
#         TODO!!"""
#
#     # self.assertTrue(np.allclose(val, val2))
#
#     def test_saveEigvec(self):
#         val, vec1 = ceig.makeEigen("sigmamatX4", saveOp=False)
#         um.saveEigvec("testvec", vec1)
#         vec2 = um.readEigvec("testvec")
#         dim = vec2[0]
#         for i in range(0, dim):
#             self.assertTrue(np.allclose(vec1[i], vec2[2][i]))
#
#     @staticmethod
#     def runTests():
#         suite = unittest.TestLoader().loadTestsFromTestCase(CalcEigenstuffTest)
#         unittest.TextTestRunner(verbosity=2).run(suite)

class CreateOperator2DTest(unittest.TestCase):

    def test_settingHeatFlowToZeroPixelToZero(self):
        testmat = np.array([
            [3, 2, 1, 2],
            [0, 2, 3, 0],
            [0, 0, 4, 0]
        ])
        operator = np.array([
            [-2.5, 2.5, 0, 0, 0, 0, 0],
            [2.5, -6, 1.5, 0, 2, 0, 0],
            [0, 1.5, -5, 1.5, 0, 2, 0],
            [0, 0, 1.5, -1.5, 0, 0, 0],
            [0, 2, 0, 0, -4.5, 2.5, 0],
            [0, 0, 2, 0, 2.5, -8, 3.5],
            [0, 0, 0, 0, 0, 3.5, -3.5]
            ])

        delInd, operatorFromMethod = cop.createOperator2D(testmat)
        # print "operator from method:"
        # print operatorFromMethod

        self.assertTrue(np.allclose(operatorFromMethod, operator))

    @staticmethod
    def runTests():
        suite = unittest.TestLoader().loadTestsFromTestCase(CreateOperator2DTest)
        unittest.TextTestRunner(verbosity=2).run(suite)

# class CreateOperator3DCooTest(unittest.TestCase):
#     def test_settingHeatFlowToZeroPixelToZero(self):
#         testmat = np.array([
#             [[3, 2, 4],
#             [0, 2, 0],
#             [0, 1, 0]],
#             [[2, 3, 4],
#             [0, 1, 0],
#             [0, 0, 0]]
#         ])
#         # testdataNoDel = [-5.0, 2.5, 2.5,2.5,-10.0,3.0,2.0,2.5,3.0,-7.0,4.0,2.0,-5.0,1.5,1.5,1.5,-1.5,2.5,-5.0,2.5,2.5,1.5,-10.5,3.5,2.0,4.0,3.5,-7.5,1.5,2.0,-3.5]
#         # testrowsNoDel = [0,0,0, 1,1,1,1,1,  2,2,2,  4,4,4,4,  7,7, 9,9,9,10,10,10,10,10,11,11,11,13,13,13]
#         # testcolsNoDel = [0,1,9, 0,1,2,4,10, 1,2,11, 1,4,7,13, 4, 7,0,9,10,1,9,10,11,13,2,10,11,4,10,13]
#
#         """ problem: die listen von createsparseoperator sind nicht sortiert, da die matrix am ende komplettiert wird; sortieren ist sauschwer, da
#         nach den rows sortiert werden muss, da die einträge aber doppelt vorkommen und diese dann nicht so sortiert werden wie es müsste"""
#
#         testdata = [-5.0, 2.5, 2.5,2.5,-10.0,3.0,2.0,2.5,3.0,-7.0,4.0,2.0,-5.0,1.5,1.5,1.5,-1.5,2.5,-5.0,2.5,2.5,1.5,-10.5,3.5,2.0,4.0,3.5,-7.5,1.5,2.0,-3.5]
#         testrows = [0,0,0,1,1,1,1,1,2,2,2,3,3,3,3,4,4,5,5,5,6,6,6,6,6,7,7,7,8,8,8]
#         testcols = [0,1,5,0,1,2,3,6,1,2,7,1,3,4,8,3,4,0,5,6,1,5,6,7,8,2,6,7,3,6,8]
#         print "len of testdata: " + str(len(testdata))
#         print "len of testrows: " + str(len(testrows))
#         print "len of testcols: " + str(len(testcols))
#
#         deletedInd, data, rows, cols = cosp.createSparseOperatorCOO(testmat)
#         # print "len of testdataNoDel: " + str(len(testdataNoDel))
#         # print "len of testrowsNoDel: " + str(len(testrowsNoDel))
#         # print "len of testcolsNoDel: " + str(len(testcolsNoDel))
#         print "len of data: " + str(len(data))
#         print "len of rows: " + str(len(rows))
#         print "len of cols: " + str(len(cols))
#         # print operatorFromMethod
#
#         ### sort result for comparability to testdata
#         inds = range(len(rows))
#         inds.sort(key=rows.__getitem__)
#         datasort = map(data.__getitem__, inds)
#         rowssort = map(rows.__getitem__, inds)
#         colssort = map(cols.__getitem__, inds)
#
#         print testdata
#         print datasort
#         print testrows
#         print rowssort
#         print testcols
#         print colssort
#         self.assertTrue(np.allclose(datasort, testdata))
#         self.assertTrue(np.allclose(rowssort, testrows))
#         self.assertTrue(np.allclose(colssort, testcols))
#         # self.assertTrue(np.allclose(datasort, testdataNoDel))
#         # self.assertTrue(np.allclose(rows, testrowsNoDel))
#         # self.assertTrue(np.allclose(cols, testcolsNoDel))
#
#     @staticmethod
#     def runTests():
#         suite = unittest.TestLoader().loadTestsFromTestCase(CreateOperator3DCooTest)
#         unittest.TextTestRunner(verbosity=2).run(suite)


# print "cwd:"
# print os.getcwd()

CreateHKStest.runTests()
CreateOperatorTest.runTests()
# CalcEigenstuffTest.runTests()
# CreateOperator2DTest.runTests()
#CreateOperator3DCooTest.runTests()