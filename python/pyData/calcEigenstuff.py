#coding:utf-8
from scipy import sparse, io
from scipy.sparse import eye
from scipy.sparse.linalg import eigsh, LinearOperator, minres, cg
import usefulMethods as um
import time
import os
import datetime
import sys

"""takes sigmamat-string as input, searches for data/rows/cols files with this name
and generates - if k is not specified - at most 300 eigenvalues and -vectors and saves
them to file"""

global path
path = os.path.dirname(os.path.realpath(__file__))
path = os.path.abspath(os.path.join(path, os.pardir))


#@profile
def calcEigenstuff(operator, name, saveEig, k, sigm, all):
    val=[]
    vec=[]
    if(all==True):
        val, vec = eigsh(operator, operator.shape[0]-1)
        return val, vec
    if operator.shape[0]-1 <= k:
        print "less than %d eigenvalues" %k
        val, vec = eigsh(operator, operator.shape[0]-1, sigma=sigm, which='LM')
    else:
        print "more than %d eigenvalues - clipped" %k
	#OP = operator - sigm*eye(operator.shape[0]) ## op for shift-invert mode
	#OPinv = LinearOperator(matvec=lambda v: minres(OP, v, tol=1e-5)[0], shape=operator.shape, dtype=operator.dtype)
	#OPinv = LinearOperator(matvec=lambda v: cg(OP, v, tol=1e-5)[0], shape=operator.shape, dtype=operator.dtype) ##dauert doppelt so lange für medium
	#w, v = eigsh(m, sigma=sigma, k=1, which='LM', tol=1e-5, OPinv=OPinv)
        val, vec = eigsh(operator, k, sigma=sigm, which='LM')  #, OPinv=OPinv
	print "Eigenvector/values calculated"
    if saveEig:
        um.saveList(path + "/eigvals/eigval"+name, val)
        um.saveEigvec(path + "/eigvecs/eigvec"+name, vec)
	print "Saved vectors/values"
    return val, vec

#@profile
def makeSparseOperator(data, rows, cols, name, saveOp):
     sparseOp = sparse.coo_matrix((data, (rows, cols)))
     if saveOp:
        io.mmwrite(path + "/mtxFiles/" + name, sparseOp)
     return sparseOp

#@profile
def makeEigen(name, k=300, sigm=0.000001, all=False, saveEig=True, saveOp=False):
    rows = um.readBinList(path + "/cooData/rows" + name, isInt=True)
    cols = um.readBinList(path + "/cooData/cols" + name, isInt=True)
    data = um.readBinList(path + "/cooData/data" + name, isInt=False)
    print "Read data"
    operator = makeSparseOperator(data, rows, cols, name, saveOp)
    print "Created sparse matrix"
    return calcEigenstuff(operator, name, saveEig, k, sigm, all)

# filename=sys.argv[1]
# print "filename: " + filename

# filenames = ['cubeBig', 'cubeBigRot90xAxis', 'cubeBigRot45yAxis', 'cubeBigRefl-0-1-0-0', 'cubeBigRefl-1-1-1-0']
# filenames = ['ambossBigRotx90', 'ambossBigRoty45', 'ambossBigRotz30Roty56']  #
# filenames = ['ambossBigDensityRotx90', 'ambossBigDensityRoty45', 'ambossBigDensityRotz30Roty56']
# filenames = ['bunnySmallRotx90', 'bunnySmallRoty45', 'bunnySmallRotz30Roty56', 'bunnyBigRotx90', 'bunnyBigRoty45', 'bunnyBigRotz30Roty56']
# filenames = ['1_2_826_0_1_3680043_2_1125_1_78514869166001886767502631794273095spacing065-065-065only1s','1_2_826_0_1_3680043_2_1125_1_44650512280840673505894811634773432spacing063-063-063only1s', '1_2_826_0_1_3680043_2_1125_1_6684150113536839605716867710154046spacing072-072-072only1s', '1_2_826_0_1_3680043_2_1125_1_43544977205268791933160390680029048spacing077-077-077only1s']
# filenames = ['1_2_826_0_1_3680043_2_1125_1_81585748112514251817903140826310781spacing08-08-08only1s','1_2_826_0_1_3680043_2_1125_1_54286138000349764725037931753924822spacing076-076-076only1s']
# filenames = ['1_2_826_0_1_3680043_2_1125_1_41151057872594974555753564137181835spacing08-08-08']
# filenames = ['1_2_826_0_1_3680043_2_1125_1_52973447532876540775084415735195358spacing07-07-07lessDense',
#              '1_2_826_0_1_3680043_2_1125_1_81585748112514251817903140826310781spacing08-08-08lessDense',
#              '1_2_826_0_1_3680043_2_1125_1_78514869166001886767502631794273095spacing065-065-065lessDense']
filenames = ['ambossBigDensityRotx13', 'ambossBigDensityRotx13Roty5'] # 'ambossBigRotx13',    , 'ambossBigDensityRotx13', 'ambossBigRotx13Roty5'
# filenames = ['bunnyBigRotx13', 'bunnyBigRotx13Roty5']
filenames = ['ambossBigDensity547']
filenames = ['amboss']
# filenames = ['1_2_826_0_1_3680043_2_1125_1_78514869166001886767502631794273095spacing06535', '1_2_826_0_1_3680043_2_1125_1_78514869166001886767502631794273095spacing0658']

for filename in filenames:
    print filename
    startTime = time.time()
    print "Starting eigencalculation..."
    print "start: " + str(datetime.datetime.now())
    makeEigen(filename)  # , sigm=0.0001
    print "eigenwertberechnung:"
    print (time.time() - startTime)



### for cubeBig and co:

# Starting eigencalculation...
# Read data
# Created sparse matrix
# more than 300 eigenvalues - clipped
# Eigenvector/values calculated
# Saved vectors/values
# eigenwertberechnung:
# 3250.44362283
# Starting eigencalculation...
# Read data
# Created sparse matrix
# more than 300 eigenvalues - clipped
# Eigenvector/values calculated
# Saved vectors/values
# eigenwertberechnung:
# 3256.22616792
# Starting eigencalculation...
# Read data
# Created sparse matrix
# more than 300 eigenvalues - clipped
# Eigenvector/values calculated
# Saved vectors/values
# eigenwertberechnung:
# 3279.85719395
# Starting eigencalculation...
# Read data
# Created sparse matrix
# more than 300 eigenvalues - clipped
# Eigenvector/values calculated
# Saved vectors/values
# eigenwertberechnung:
# 2870.35136008
# Starting eigencalculation...
# Read data
# Created sparse matrix
# more than 300 eigenvalues - clipped
# Eigenvector/values calculated
# Saved vectors/values
# eigenwertberechnung:
# 2933.66321421

#
# erstellen delInd, data, rows, cols:
# 3.09944152832e-05
# cubeBigRot90xAxis
# read CT data (3.41163611412)
# read delInd, val, vecs (5.36839294434)
# start: 1505382024.1
# cubeBigRot45yAxis
# read CT data (5.03213596344)
# read delInd, val, vecs (5.6407520771)
# start: 1505383315.36
# cubeBigRefl-0-1-0-0
# read CT data (4.19678497314)
# read delInd, val, vecs (7.39803504944)
# start: 1505384362.38
# cubeBigRefl-1-1-1-0
# read CT data (4.43430089951)
# read delInd, val, vecs (6.77927088737)
# start: 1505385660.71
# berechnung hks:
# 1240.05103