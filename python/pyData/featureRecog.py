#coding:utf-8
from testMatrizen import *
# from test import makeHistogramm
import time
import usefulMethods as um
import math
import sys
import random
import copy
import heapq

########################################################################################################################################################################################
############################## feature recognition #####################################################################################################################################
########################################################################################################################################################################################

def euclidDist(vec1, vec2, square=False, powerFour=False):
	diffVec = [(vec1[i]-vec2[i]) for i in range(len(vec1))]
	sqrnorm = 0
	for entry in diffVec:
		sqrnorm += entry*entry
	if square:
		return sqrnorm
	if powerFour:
		return sqrnorm*sqrnorm
	return math.sqrt(sqrnorm)

def vecNorm(vec):
	sum = 0
	for entry in vec:
		sum += entry*entry
	return math.sqrt(sum)

def cosBetwSigs(vec1, vec2):
	sum = 0
	if len(vec1)!=len(vec2):
		print "error in cosBetwSigs: vectors must be same size"
		return
	for entry in range(0, len(vec1)):
		sum += vec1[entry]*vec2[entry]
	return sum/(vecNorm(vec1)*vecNorm(vec2))

def sumLists(list1, list2):
	if len(list1)!=len(list2):
		print "error: lists must have same size"
		sys.exit
	else:
		return [(list1[i]+list2[i]) for i in range(0, len(list1))]

def calcNewClusterMeans(cluster):
	newMeans = []
	for c in range(0, len(cluster)):
		newMean = cluster[c][0][1]
		for i in range(1, len(cluster[c])): ## sum all entries in cluster
			newMean = sumLists(newMean, cluster[c][i][1])
		for i in range(0, len(newMean)): ## divide through number of entries in cluster
			newMean[i] = newMean[i]/len(cluster[c])
		newMeans.append(newMean)
	return newMeans

def writeToFeatFile(cluster, name):
	result = []
	for c in range(0, len(cluster)):
		for i in range(0,len(cluster[c])):
			entry = cluster[c][i]
			result.extend((entry[0][0],entry[0][1],entry[0][2],c))
	# print result
	um.saveFeatRec('/work/Masterarbeit/qtTest/' + name, result)

def calcSqDistances(means, data):
	sqdistances = []
	sum = 0
	for entry in data:
		## compute square distances to every vertex and calc sum
		if len(means)==1:
			distance = euclidDist(entry[1], means[0], powerFour=True)
			sqdistances.append(distance)
			sum += distance
		else:
			dist = []
			for mean in means:
				dist.append(euclidDist(entry[1], mean, powerFour=True))
			minDist = min(dist)
			sqdistances.append(minDist)
			sum += minDist
	return sum, sqdistances


def chooseNextCenter(sqdistances, sum):
	r = random.uniform(0, sum)
	upto = 0
	## choose next center from distances with weightes prob func d^2/sum d_i^2
	for i in range(0, len(sqdistances)):
		weight = sqdistances[i]
		if upto + weight >= r:
			return i
		upto += weight

def chooseSeedsKPP(k, data):
	## source: https://datasciencelab.wordpress.com/2014/01/15/improved-seeding-for-clustering-with-k-means/
	## https://stackoverflow.com/questions/3679694/a-weighted-version-of-random-choice
	means = []
	## choose first mean 'randomly': take hks of end of list
	# c1 = data[-1][1]
	c1 = data[random.randint(0,len(data))][1]
	means.append(c1)
	## calc the square distances of each vertex to c1 and following new means
	for i in range(0, k-1):
		sum, sqdistances = calcSqDistances(means, data)
		means.append(data[chooseNextCenter(sqdistances, sum)][1])
		# statistics = []
		# for j in range(0,1000):
		# 	index = chooseNextCenter(sqdistances, sum)
		# 	statistics.append(sqdistances[index])
		# print "done with iteration " + str(i)
		# makeHistogramm(statistics, 'testHisto2'+str(i), h_bins=100)
	return means



def kmeansClustering(k, data, name='result', numOfIter=100):
	hks = data[4]
	means, meansFromLastIter, emptycluster, result = [], [], [], []
	""" following commented lines are the original kmeans algo with random centers in the beginning. now using k++ algo"""
	# randomInds = random.sample(xrange(0, data[1]), k)
	# for i in randomInds: ## choose k initial random means
	# 	means.append(hks[i][1]) ## only use hks, not coordinates
	# 	emptycluster.append([]) 	## empty lists for cluster
	startTime = time.time()
	means = chooseSeedsKPP(k, hks)
	print "done with initial means after " + str(time.time()-startTime)
	print "len of means: " + str(len(means))
	for i in range(0, k):
		emptycluster.append([]) 	## empty lists for cluster
	goOn = True
	counterIter = 0
	while(goOn):
		print "iteration " + str(counterIter)
		counterIter += 1
		meansFromLastIter = means
		cluster = copy.deepcopy(emptycluster)
		for entry in hks:
			minInd = -1
			min = 10000
			diffVec = []
			for meanInd in range(0, len(means)):
				norm = euclidDist(entry[1],means[meanInd])
				if norm<min:
					min=norm
					minInd=meanInd
			cluster[minInd].append(entry)
		# for i in range(0, len(cluster)):
		# 	print str(i) + ": " + str(len(cluster[i])) +", ",
		# print ""
		### calculate new means
		means = calcNewClusterMeans(cluster)
		### compare with last iteration
		diffToLastIter = 0
		for i in range(0, len(means)):
			diffToLastIter += euclidDist(means[i],meansFromLastIter[i])
		print "difference to last iter: " + str(diffToLastIter)
		if diffToLastIter<1e-9 or counterIter==numOfIter:
			goOn=False
			result = cluster
			if counterIter==numOfIter:
				print "did not converge in %s steps" %numOfIter
	writeToFeatFile(result, name)
	print "iterations: " + str(counterIter)
	for i in range(0, len(result)):
		print str(i) + ": " + str(len(result[i])) +", ",
	return result


		# also, count items of the cluster, just for interest
		#put result in file
		# break while





"""
USAGE:
hksFilename1 	= hks file of CT that will provides the points to be compared against
tol 			= list of tolerance values for each feature point (same order as feature points list!)
hksFilename2 	=
onlyHist		=
compPoints		= list of points to which similar features are to be found on the SAME CT, in [(x,y,z)] form
# featPoints	= list of points with hks to which similar features are to be found on the same OR a different CT, in [(x,y,z), (hks values)] form
name			= filename to which result is saved
"""

def findSimiliarHKSWithThreshold(hksFilename1, tol, hksFilename2='default', onlyHisto=False, compPoints=[], name='result'):
	hksList = um.readHKS('/home/vegrimm/Desktop/Masterarbeit/python/hksFiles/' + hksFilename1)
	result = []
	l2normList = []
	counterHowManyFeaturesFound = 0
	counterForStuff = 0
	meanL2norm = 0
	### find hks of featPoints
	if compPoints:
		# print "no featPoints list found, creating"
		featPoints = []
		for compPoint in compPoints:
			for hks in hksList[4]:
				if compPoint==hks[0]:
					featPoints.append(hks)
					continue
	#print featPoints
	if not featPoints:
		print "no feature points found!"
	else:
		print "featPoints list created"
	if hksFilename2!='default':
		print "compare points to hks of " + hksFilename2
		hksList = um.readHKS('/work/Masterarbeit/python/hksFiles/' + hksFilename2)
	for hks in hksList[4]:	
		counter = 1
		for i in range(0, len(featPoints)):
			l2norm = euclidDist(featPoints[i][1],hks[1])
			# l2norm = cosBetwSigs(featPoints[i][1],hks[1])
			l2normList.append(l2norm)
			if not onlyHisto:
				### is point similar to current feature point?
				if l2norm < tol[i]:
					if len(hks[0])==3:
						result.extend((hks[0][0],hks[0][1], hks[0][2],counter))
					else:
						result.extend((hks[0][0],hks[0][1],counter))
					counterHowManyFeaturesFound += 1
					break
				else:
					if counter == len(featPoints):
						if len(hks[0])==3:
							result.extend((hks[0][0],hks[0][1], hks[0][2],0)) ## append for no match
						else:
							result.extend((hks[0][0],hks[0][1],0))
				counter +=1	

	if onlyHisto:
		makeHistogramm(l2normList, name[:-4])
	else:
		meanL2norm = meanL2norm/len(hksList[4])
		print "number of features found: " + str(counterHowManyFeaturesFound)
	print "len of result: " + str(len(result))
	um.saveFeatRec('/work/Masterarbeit/qtTest/'+name, result)


def findKmostSimilarHKS(hksFilename1, k, compPoint, hksFilename2='default', name='result'):
	"""ATTENTION: compPoints cannot be more than one point, or more than one heap would be needed!!"""
	hksList = um.readHKS('/home/vegrimm/Desktop/Masterarbeit/python/hksFiles/' + hksFilename1)
	result = []
	featPoint = 0
	heap = []
	for hks in hksList[4]:
		if compPoint == hks[0]:
			featPoint=hks
	if featPoint==0:
		print "no feature points found!"
		return 1
	else:
		result.extend((featPoint[0][0], featPoint[0][1], featPoint[0][2], 2))
		print "featPoints list created"
		#print featPoint
	if hksFilename2 != 'default':
		print "compare points to hks of " + hksFilename2
		hksList = um.readHKS('/work/Masterarbeit/python/hksFiles/' + hksFilename2)
	for i in range (k):
		l2norm = euclidDist(featPoint[1], hksList[4][i][1])
		heapq.heappush(heap,(-l2norm, (hksList[4][i][0][0], hksList[4][i][0][1], hksList[4][i][0][2])))

	for i in range(k, len(hksList[4])):
		l2norm = euclidDist(featPoint[1], hksList[4][i][1])
		#print heap[0][0], l2norm
		if -l2norm>heap[0][0]:
			heapq.heappushpop(heap, (-l2norm, (hksList[4][i][0][0], hksList[4][i][0][1], hksList[4][i][0][2])))
		#result.extend((hksList[4][i][0][0], hksList[4][i][0][1], hksList[4][i][0][2], 0))
			### is point similar to current feature point?

			# if l2norm < tol[i]:
			# 	if len(hks[0]) == 3:
			# 		result.extend((hks[0][0], hks[0][1], hks[0][2], counter))
			# 	else:
			# 		result.extend((hks[0][0], hks[0][1], counter))
			# 	break
			# else:
			# 	if counter == len(featPoints):
			# 		if len(hks[0]) == 3:
			# 			result.extend((hks[0][0], hks[0][1], hks[0][2], 0))  ## append for no match
			# 		else:
			# 			result.extend((hks[0][0], hks[0][1], 0))


	for i in range(k):
		p = heapq.heappop(heap)
		result.extend((p[1][0], p[1][1], p[1][2], 1))
		print p
	for hks in hksList[4]:
		result.extend((hks[0][0], hks[0][1], hks[0][2], 0))
	# print "len of result: " + str(len(result))
	# um.saveFeatRec('/work/Masterarbeit/qtTest/' + name + str(k) +'mostSimilar', result)  # binary format
	if hksFilename2 != 'default':
		um.saveFeatList('/work/Masterarbeit/qtTest/' + name + 'compTo' +hksFilename2[-21:]+ '_'+str(k) + 'mostSimilar.feat', result)  # non-binary format
	else:
		um.saveFeatList('/work/Masterarbeit/qtTest/' + name + str(k) + 'mostSimilar.feat', result)  # non-binary format

# result = kmeansClustering(25, test, name='cluster'+s)
# chooseSeedsKPP(15, test[4])


startTime = time.time()


#############################################################
"""CTs"""
#############################################################

k = [2,3,4,5,6,7,8,9,20]
# k = [10, 11, 12, 13, 14, 15, 16, 17, 18, 19]
# k = [20, 21, 22, 23, 24]
# k = [25, 26, 27, 28, 29]
# k = [31,32,33,34]
# k = [35, 36, 37, 38, 39]
# k = [20, 25, 30, 35, 40, 50, 60]
# k = [41,42,43,44,45,46,ö47,48,49]
# k = [51,52,53,54,55,56,57,58,59]
# k = [65,66,67,68,69]
# k = [71,72,73,74,75]
# k = [76,77,78,79]
# k = [81,82,83,84]
# k = [86,87,88,89]
# k = [70,75,80,85,90,95]
# k = [91,92,93,94]
# k = [100, 130, 160, 200, 300]
# k = [101,102,103,104]
# k = [106,107,108,109]
# k = [116,117,118,119]
# k = [105,110,115,120,125]
# k = [140, 145, 150, 155]
# k = [151,152,153,154]
# k = [156,157,158,159]
# k = [194,195,196,197,198,199]
# k = [201,202,203,204,205,206,207,208,209]
# k = [210, 220, 230, 240]
# k = [242,244,246,248]
# k = [250,260,270,280,290]
# k = [305,310,315,320]
# k = [325,350,375,400,500]
# k = [330,335,340,345]
# k = [355,360,365,370]
# k = [380,385,390,395]
# k = [132,134,136,138]
# k = [50, 100]
# k = [200]
# k = [550,600,650, 700 ]
# k = [510,520,530,540]
# k = [910, 920, 930, 940]
# k = [800,900, 950, 1000, 1050, 1100, 1200]
# k = [1250,1300,1350,1400]
# k = [1500, 1700, 2000]
#
# print k

# s='completeHKSsigmamat1_2_826_0_1_3680043_2_1125_1_44650512280840673505894811634773432spacing063-063-063'
# s='completeHKSsigmamat1_2_826_0_1_3680043_2_1125_1_44650512280840673505894811634773432spacing063-063-063only1s'
# point =  (185, 29, 157) # nose
# point = (188, 86, 17)  # chin
# point = (184, 32, 211)  # forehead
# point =  (142, 49, 199) # eye socket right
# point =  (104, 105, 126) # cheekbone right
# point =  (110, 212, 131) # felsenbein right
#point =   #

s='completeHKSsigmamat1_2_826_0_1_3680043_2_1125_1_78514869166001886767502631794273095spacing065-065-065'
# s='completeHKSsigmamat1_2_826_0_1_3680043_2_1125_1_78514869166001886767502631794273095spacing065-065-065only1s'
# s = 'completeHKSsigmamat1_2_826_0_1_3680043_2_1125_1_78514869166001886767502631794273095spacing065-065-065lessDense'
point =  (187, 38, 143) # nose
# point = (185, 91, 18)  # chin
# point = (184, 35, 191)  # forehead
# point =  (146, 50, 179) # eye socket right
# point = (112, 100, 117)  # cheekbone right
# point =  (107, 209, 124) # felsenbein right

# s='completeHKSsigmamat1_2_826_0_1_3680043_2_1125_1_6684150113536839605716867710154046spacing072-072-072'
# s='completeHKSsigmamat1_2_826_0_1_3680043_2_1125_1_6684150113536839605716867710154046spacing072-072-072only1s'
# point = (221, 44, 140)  # nose
# point = (193, 105, 17)  # chin
# point = (234, 42, 211)  # forehead
# point = (184, 45, 185)  # eye socket right
# point = (128, 95, 131)  # cheekbone right
# point =  (103, 190, 148) # felsenbein right

# s='completeHKSsigmamat1_2_826_0_1_3680043_2_1125_1_52973447532876540775084415735195358spacing07-07-07'
# s='completeHKSsigmamat1_2_826_0_1_3680043_2_1125_1_52973447532876540775084415735195358spacing07-07-07only1s'
# s = 'completeHKSsigmamat1_2_826_0_1_3680043_2_1125_1_52973447532876540775084415735195358spacing07-07-07lessDense'
# point =  (178, 43, 161) # nose
# point = (170, 73, 24)  # chin
# point =  (181, 47, 215) # forehead
# point = (139, 57, 196)  # eye socket right
# point =  (107, 92, 123) # cheekbone right
# point = (102, 192, 134)  # felsenbein right

# s='completeHKSsigmamat1_2_826_0_1_3680043_2_1125_1_81585748112514251817903140826310781spacing08-08-08'
# s='completeHKSsigmamat1_2_826_0_1_3680043_2_1125_1_81585748112514251817903140826310781spacing08-08-08only1s'
# s = 'completeHKSsigmamat1_2_826_0_1_3680043_2_1125_1_81585748112514251817903140826310781spacing08-08-08lessDense'
# point = (150, 32, 117)  # nose
# point =  (158, 73, 13) # chin
# point = (148, 33, 177)  # forehead
# point = (116, 47, 154)  # eye socket right
# point = (100, 83, 93)  # cheekbone right
# point = (95, 176, 99)  # felsenbein right

# s='completeHKSsigmamat1_2_826_0_1_3680043_2_1125_1_41151057872594974555753564137181835spacing08-08-08'
# s='completeHKSsigmamat1_2_826_0_1_3680043_2_1125_1_41151057872594974555753564137181835spacing08-08-08only1s'
# point =  (149, 26, 206) # nose
# point =  (154, 62, 84) # chin
# point =  (148, 39, 275) # forehead
# point = (115, 43, 240)  # eye socket right
# point =  (92, 84, 180) # cheekbone right
# point = (83, 170, 179)  # felsenbein right

# s="completeHKSsigmamat1_2_826_0_1_3680043_2_1125_1_54286138000349764725037931753924822spacing076-076-076"
# s="completeHKSsigmamat1_2_826_0_1_3680043_2_1125_1_54286138000349764725037931753924822spacing076-076-076only1s"
# point = (158, 29, 166)  # nose
# point = (157, 68, 16)  # chin
# point = (157, 33, 206)  # forehead
# point =  (124, 47, 194) # eye socket right
# point = (99, 81, 113)  # cheekbone right
# point = (88, 182, 105)  # felsenbein right

# s="completeHKSsigmamat1_2_826_0_1_3680043_2_1125_1_43544977205268791933160390680029048spacing077-077-077"
# s="completeHKSsigmamat1_2_826_0_1_3680043_2_1125_1_43544977205268791933160390680029048spacing077-077-077only1s"
# point =  (153, 23, 140) # nose
# point =  (160, 70, 17) # chin
# point =  (151, 33, 191) # forehead
# point = (115, 48, 183)  # eye socket right
# point = (87, 94, 106)  # cheekbone right
# point = (75, 185, 119)  # felsenbein right

# s1 = "completeHKSsigmamat1_2_826_0_1_3680043_2_1125_1_78514869166001886767502631794273095spacing06535"
# point = (186, 37, 141)  # nose
# point = (184, 93, 17)  # chin
# point =  (186, 36, 180) # forehead
# point = (147, 51, 179)  # eye socket right
# point = (111, 102, 117)  # cheekbone right
# point =  (108, 209, 124) # felsenbein right
# s1 = "completeHKSsigmamat1_2_826_0_1_3680043_2_1125_1_78514869166001886767502631794273095spacing0658"
# point = (185, 36, 140)  # nose
# point =  (184, 93, 16) # chin
# point = (184, 35, 180)  # forehead
# point = (147, 49, 177)  # eye socket right
# point = (110, 100, 116)  # cheekbone right
# point =  (124, 207, 139) # felsenbein right

for i in k:
	# findKmostSimilarHKS(s,i,point,name=s[-27:]+'_')   # for only1s
	findKmostSimilarHKS(s,i,point,name=s[-21:]+'_') # for all others

# s1 = 'completeHKSsigmamat1_2_826_0_1_3680043_2_1125_1_44650512280840673505894811634773432spacing063-063-063'
# s2 = 'completeHKSsigmamat1_2_826_0_1_3680043_2_1125_1_78514869166001886767502631794273095spacing065-065-065'
# s3 = 'completeHKSsigmamat1_2_826_0_1_3680043_2_1125_1_6684150113536839605716867710154046spacing072-072-072'
# s4 = 'completeHKSsigmamat1_2_826_0_1_3680043_2_1125_1_52973447532876540775084415735195358spacing07-07-07'
# s5 = 'completeHKSsigmamat1_2_826_0_1_3680043_2_1125_1_81585748112514251817903140826310781spacing08-08-08'
# s6 = "completeHKSsigmamat1_2_826_0_1_3680043_2_1125_1_54286138000349764725037931753924822spacing076-076-076"
# s7 = "completeHKSsigmamat1_2_826_0_1_3680043_2_1125_1_43544977205268791933160390680029048spacing077-077-077"

# s1 = 'completeHKSsigmamat1_2_826_0_1_3680043_2_1125_1_44650512280840673505894811634773432spacing063-063-063only1s'
# s2 = 'completeHKSsigmamat1_2_826_0_1_3680043_2_1125_1_78514869166001886767502631794273095spacing065-065-065only1s'
# s3 = 'completeHKSsigmamat1_2_826_0_1_3680043_2_1125_1_6684150113536839605716867710154046spacing072-072-072only1s'
# s4 = 'completeHKSsigmamat1_2_826_0_1_3680043_2_1125_1_52973447532876540775084415735195358spacing07-07-07only1s'
# s5 = 'completeHKSsigmamat1_2_826_0_1_3680043_2_1125_1_81585748112514251817903140826310781spacing08-08-08only1s'
# s6 = "completeHKSsigmamat1_2_826_0_1_3680043_2_1125_1_54286138000349764725037931753924822spacing076-076-076only1s"
# s7 = "completeHKSsigmamat1_2_826_0_1_3680043_2_1125_1_43544977205268791933160390680029048spacing077-077-077only1s"

# l = [s1, s2, s3, s4, s5, s6, s7]
#
# for s2 in l:
# 	for i in k:
# 		if s!=s2:
# 			findKmostSimilarHKS(s,i,point, hksFilename2=s2, name=s[-27:]+'_')   # for only1s
# 			# findKmostSimilarHKS(s,i,point, hksFilename2=s2,name=s[-21:]+'_') # for all others


# for i in k:
# 	findKmostSimilarHKS(s,i,point, hksFilename2=s1, name=s[-27:]+'_')   # for only1s
# 	# findKmostSimilarHKS(s,i,point, hksFilename2=s6,name=s[-21:]+'_') # for all others


k = [2,3,4,5,6,7,8,9, 10,20,100, 150, 200]
# k = [6,7,8,9,10,11,12, 13, 2000]
k = [9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19]
# k = [20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 2000]
# k = [31,32,33,34]
# k = [35, 36, 37, 38, 39]
# k = [20, 25, 30, 35, 40, 50, 60, 70, 80, 90, 2000]
# k = [41,42,43,44,45,46,47,48,49]
# k = [51,52,53,54,55,56,57,58,59]
# k = [61,62,63,64,65,66,67,68,69]
# k = [71,72,73,74,75,76,77,78,79]
# k = [81,82,83,84, 85, 86,87,88,89]
# k = [70,75,80,85,90,95]
# k = [91,92,93,94]
# k = [100, 130, 160, 200, 300, 2000]
# k = [101,102,103,104]
# k = [106,107,108,109]
# k = [116,117,118,119]
# k = [105,110,115,120,125]
# k = [140, 145, 150, 155]
# k = [151,152,153,154]
# k = [156,157,158,159]
# k = [165,170,175,180,185,190,195]
# k = [194,195,196,197,198,199]
# k = [201,202,203,204,205,206,207,208,209]
# k = [210, 220, 230, 240]
# k = [242,244,246,248]
# k = [250,260,270,280,290]
# k = [305,310,315,320]
# k = [325,350,375,400,500]
# k = [410, 420, 430, 440, 450, 460, 470, 480, 490]
# k = [330,335,340,345]
# k = [355,360,365,370]
# k = [380,385,390,395]
# k = [132,134,136,138]
# k = [50, 100]
# k = [200]
# k = [550,600,650, 700 ]
# k = [510,520,530,540]
# k = [910, 920, 930, 940]
# k = [800,900, 950, 1000, 1050, 1100, 1200]
# k = [1250,1300,1350,1400]
# k = [1500, 1700, 2000]
# k = [1750, 1900, 1950]
# print k

#############################################################
"""amboss"""
#############################################################

s1 = "completeHKSsigmamatamboss"
s0= "completeHKSsigmamatambossDensity"
# s0= "completeHKSsigmamatambossDensity0-54"
# findKmostSimilarHKS(s1,2,(240, 240, 190),name=s1)  # middle point
# point = (258, 240, 177) # tip
# point = (231, 256, 177) # tip
# point = (231, 224, 177) # tip
# point = (258, 240, 223) # tip
# point = (231, 256, 223) # tip
# point = (231, 224, 223) # tip
# point = (237, 245, 210) # close to middle point
# point = (240, 240, 190) # centre
# point = (240, 240, 210) # centre
# findKmostSimilarHKS(s1,7,point,name=s1)  # tip
# findKmostSimilarHKS(s0,20,point,name=s0+'_')
# for i in k:
# 	findKmostSimilarHKS(s0, i, point, name=s0+'_')
# 	# findKmostSimilarHKS(s0,i,point,hksFilename2=s7,name=s0[-16:])

#############################################################
"""ambossBig"""
#############################################################



s11 = 'completeHKSsigmamatambossBigDensityRotx13Roty5'
s10 = 'completeHKSsigmamatambossBigDensityRotx13'
s7 = "completeHKSsigmamatambossBigDensityRotz30Roty56"
s6 = "completeHKSsigmamatambossBigDensityRoty45"
s5 = "completeHKSsigmamatambossBigDensityRotx90"
s0 = "completeHKSsigmamatambossBigDensity"
s00 = "completeHKSsigmamatambossBigDensity547"

s9 = 'completeHKSsigmamatambossBigRotx13Roty5'
s8 = 'completeHKSsigmamatambossBigRotx13'
s4= "completeHKSsigmamatambossBigRotz30Roty56"
s3 = "completeHKSsigmamatambossBigRoty45"
s2 = "completeHKSsigmamatambossBigRotx90"
s1 = "completeHKSsigmamatambossBig"

# s0= "completeHKSsigmamatambossBigDensity0-547"
### points for original
point = (297, 240, 270) # tip
# point = (212, 288, 270) # tip
# point = (212, 192, 270) # tip
# point = (297, 240, 130) # tip
# point = (212, 288, 130) # tip
# point = (212, 192, 130) # tip
# point = (230, 223, 170)  # close to centre
# point = (240, 240, 170) #  centre
# point = (240, 240, 230) #  centre
# findKmostSimilarHKS(s1,7,point,name=s1)
# findKmostSimilarHKS(s1,10,point,name=s1+str(point))
# findKmostSimilarHKS(s0,13,point,name=s0+'_')  # +str(point)
# for i in k:
# 	findKmostSimilarHKS(s0, i, point, name=s0+'_')
	# findKmostSimilarHKS(s0,i,point,hksFilename2=s7,name=s0[-16:])


#############################################################
"""bunnySmall, voxelisiert"""
#############################################################

s1 = "completeHKSsigmamatbunnySmall"
s2 = "completeHKSsigmamatbunnySmallRotx90"
s3 = "completeHKSsigmamatbunnySmallRoty45"
s4 = "completeHKSsigmamatbunnySmallRotz30Roty56"
# findKmostSimilarHKS(s1,22,(11, 48, 25),name=s1)  # left ear
# findKmostSimilarHKS(s1,11,(0, 46, 9),name=s1)  # right ear
# findKmostSimilarHKS(s1,60,(35, 28, 3),name=s1)  # snout
# findKmostSimilarHKS(s1,5,(32, 1, 9),name=s1) # left paw
# findKmostSimilarHKS(s1,8,(19, 0, 8),name=s1)# right paw
# findKmostSimilarHKS(s1,17,(26, 9, 49),name=s1) # tail


#############################################################
"""bunnyBig, voxelisiert"""
#############################################################

# k = [2,3,4,5,6,7,8,9,20,100, 150, 200]
# k = [6,7,8,9,10,11,12, 2000]
# k = [10, 11, 12, 13, 14, 15, 16, 17, 18, 19]
# k = [20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 2000]
# k = [31,32,33,34]
# k = [35, 36, 37, 38, 39]
k = [20, 25, 30, 35, 40, 50, 60, 70, 80, 90, 2000]
# k = [41,42,43,44,45,46,47,48,49]
k = [50, 51,52,53,54,55,56,57,58,59]
# k = [61,62,63,64,65,66,67,68,69]
# k = [71,72,73,74,75,76,77,78,79]
# k = [81,82,83,84, 85, 86,87,88,89]
# k = [70,75,80,85,90,95]
# k = [91,92,93,94, 95, 96, 97, 99]
# k = [100, 130, 160, 200, 300, 2000]
# k = [101,102,103,104]
# k = [106,107,108,109]
# k = [111,112,113,114,115,116,117,118,119]
# k = [105,110,115,120,125]
# k = [130,131,132,133,134,135,136,137,138,139]
# k = [140, 145, 150, 155]
# k = [151,152,153,154]
# k = [156,157,158,159]
# k = [165,170,175,180,185,190,195]
# k = [194,195,196,197,198,199]
# k = [201,202,203,204,205,206,207,208,209]
# k = [210, 220, 230, 240]
# k = [211, 212, 213, 214, 215, 216, 217, 218, 219]
# k = [242,244,246,248]
# k = [250,260,270,280,290]
# k = [305,310,315,320]
# k = [325,350,375,400,500]
# k = [330,335,340,345]
# k = [355,360,365,370]
# k = [380,385,390,395]
# k = [132,134,136,138]
# k = [50, 100]
# k = [200]
# k = [550,600,650, 700 ]
# k = [510,520,530,540]
# k = [910, 920, 930, 940]
# k = [800,900, 950, 1000, 1050, 1100, 1200]
# k = [1250,1300,1350,1400]
# k = [1500, 1700, 2000]
# print k

s1 = "completeHKSsigmamatbunnyBig"
s2 = "completeHKSsigmamatbunnyBigRotx90"
s3 = "completeHKSsigmamatbunnyBigRoty45"
s4 = "completeHKSsigmamatbunnyBigRotz30Roty56"
s5 = "completeHKSsigmamatbunnyBigRotx13"
s6 = "completeHKSsigmamatbunnyBigRotx13Roty5"
point = (33, 145, 78) # left ear
# point = (2, 141, 28 )  # right ear
point = (106, 82, 12)   # snout
# point = (98, 3, 29)  # left paw
# point = (62, 2, 21)  # right paw
# point = (73, 17, 146)  # tail

# for i in k:
# 	# findKmostSimilarHKS(s1,i,point,name=s1)
# 	findKmostSimilarHKS(s1,i,point,hksFilename2=s3,name=s1)







print "fertig in " + str(time.time() - startTime)