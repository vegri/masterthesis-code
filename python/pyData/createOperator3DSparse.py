# coding: utf-8
import time
import numpy as np
from bisect import bisect_left
# from multiprocessing import Pool


np.set_printoptions(linewidth=1800)

global counter
counter = 0

def getCoordinates(line,numOfXrows, numOfYrows):
        z= line / (numOfYrows * numOfXrows)                                                                                                                                                 # z-component
        y = (line - (line % numOfXrows)) / numOfXrows - (line - (line % (numOfYrows * numOfXrows))) / numOfXrows        # y-component
        x = line % numOfXrows  											# x-component
        return x,y,z

# @profile
def calcOperatorElement3D(x, y, z, diagonal, sigmaMat, numOfYrows, numOfXrows, takeMin):                                                                                                                                

    voxelVal =  sigmaMat[z][y][x]
    if voxelVal==0:
            return 0

    if not takeMin:
        if (diagonal == 0):
            origPixel = 0
            if (y+1)<len(sigmaMat[0]):
                    lowYneighbour = sigmaMat[z][y + 1][x]
                    if lowYneighbour!=0:
                            origPixel += (voxelVal + lowYneighbour) / 2.0
            if (y - 1) >= 0:
                    upYneighbour = sigmaMat[z][y - 1][x]
                    if upYneighbour!=0:
                            origPixel += (voxelVal + upYneighbour) / 2.0
            if (z+1)<len(sigmaMat):
                    backZneighbour = sigmaMat[z + 1][y][x]
                    if backZneighbour!=0:
                            origPixel += (voxelVal + backZneighbour) / 2.0
            if (z - 1) >= 0:
                    frontZneighbour = sigmaMat[z - 1][y][x]
                    if frontZneighbour!=0:
                            origPixel += (voxelVal + frontZneighbour) / 2.0
            if (x+1)<len(sigmaMat[0][0]):
                    rightXneighbour = sigmaMat[z][y][x + 1]
                    if rightXneighbour!=0:
                            origPixel += (voxelVal + rightXneighbour) / 2.0
            if (x - 1) >= 0:
                    leftXneighbour = sigmaMat[z][y][x - 1]
                    if leftXneighbour!=0:
                        origPixel += (voxelVal + leftXneighbour) / 2.0

		#if origPixel == 0:
		#	print "pixel stand alleine"
            if origPixel ==0:
                global counter
                counter += 1
            return -origPixel
        if diagonal == 1:
            if sigmaMat[z][y][x + 1]!=0:
		        return (sigmaMat[z][y][x + 1] + voxelVal) / 2.0
            else:
                return 0
        if diagonal == -1:
            if sigmaMat[z][y][x - 1]!=0:
                return (sigmaMat[z][y][x - 1] + voxelVal) / 2.0
            else:
                return 0
        if diagonal == numOfXrows:
		    if sigmaMat[z][y + 1][x]!=0:
		        return (sigmaMat[z][y + 1][x] + voxelVal) / 2.0
		    else:
		        return 0
        if diagonal == -numOfXrows:
		    if sigmaMat[z][y - 1][x]!=0:
		        return (sigmaMat[z][y - 1][x] + voxelVal) / 2.0
		    else:
		        return 0
        if diagonal == numOfXrows * numOfYrows:
		    if sigmaMat[z + 1][y][x]!=0:
		        return (sigmaMat[z + 1][y][x] + voxelVal) / 2.0
		    else:
		        return 0
        if diagonal == -numOfXrows * numOfYrows:
            if sigmaMat[z - 1][y][x]!=0:
                return (sigmaMat[z - 1][y][x] + voxelVal) / 2.0
            else:
                return 0
    else:
        # print "takemin=true"
        if (diagonal == 0):
		        origPixel = 0
		        if (y+1)<len(sigmaMat[0]):
		                lowYneighbour = sigmaMat[z][y + 1][x]
		                if lowYneighbour!=0:
		                        origPixel += min(voxelVal,   lowYneighbour)
		        if (y - 1) >= 0:
		                upYneighbour = sigmaMat[z][y - 1][x]
		                if upYneighbour!=0:
		                        origPixel += min(voxelVal,   upYneighbour)
		        if (z+1)<len(sigmaMat):
		                backZneighbour = sigmaMat[z + 1][y][x]
		                if backZneighbour!=0:
		                        origPixel += min(voxelVal,   backZneighbour)
		        if (z - 1) >= 0:
		                frontZneighbour = sigmaMat[z - 1][y][x]
		                if frontZneighbour!=0:
		                        origPixel += min(voxelVal,   frontZneighbour)
		        if (x+1)<len(sigmaMat[0][0]):
		                rightXneighbour = sigmaMat[z][y][x + 1]
		                if rightXneighbour!=0:
		                        origPixel += min(voxelVal,   rightXneighbour)
		        if (x - 1) >= 0:
		                leftXneighbour = sigmaMat[z][y][x - 1]
		                if leftXneighbour!=0:
		                        origPixel += min(voxelVal,   leftXneighbour)

		#if origPixel == 0:
		#	print "pixel stand alleine"
		        return -origPixel
        if diagonal == 1:
            if sigmaMat[z][y][x + 1]!=0:
               return min(voxelVal,  sigmaMat[z][y][x + 1])
            else:
                return 0
        if diagonal == -1:
            if sigmaMat[z][y][x - 1]!=0:
                return min(voxelVal,  sigmaMat[z][y][x - 1])
            else:
                return 0
        if diagonal == numOfXrows:
            if sigmaMat[z][y + 1][x]!=0:
                return min(voxelVal,  sigmaMat[z][y + 1][x])
            else:
                return 0
        if diagonal == -numOfXrows:
            if sigmaMat[z][y - 1][x]!=0:
                return min(voxelVal,  sigmaMat[z][y - 1][x])
            else:
                return 0
        if diagonal == numOfXrows * numOfYrows:
            if sigmaMat[z + 1][y][x]!=0:
                return min(voxelVal,  sigmaMat[z + 1][y][x])
            else:
                return 0
        if diagonal == -numOfXrows * numOfYrows:
            if sigmaMat[z - 1][y][x]!=0:
                    return min(voxelVal,  sigmaMat[z - 1][y][x])
            else: return 0
		

# @profile
def createSparseOperatorCOO(sigmaMat, numOfZrows, numOfYrows, numOfXrows, takeMin=False):
        startTime = time.time()
        print "len of sigmamat: " + str(len(sigmaMat))
        sigmaMat = sigmaMat.reshape((numOfZrows, numOfYrows, numOfXrows))
        print "numofzrows = " + str(numOfZrows)
        print "numOfYrows = " + str(numOfYrows)
        print "numOfXrows = " + str(numOfXrows)
        xTimesY = numOfXrows * numOfYrows
        matSize = xTimesY * numOfZrows
        print "matSize: " + str(matSize)
        rows = []
        cols = []
        data = []
        deletedInd = []
        counterDelInd = 0

        #### only save upper triangular of symmetric operator
        for row in range(0, matSize):

            x, y, z = getCoordinates(row,numOfXrows, numOfYrows)

            ### calcOperatorElement3D(x, y, z, diagonal, sigmaMat, numOfYrows, numOfXrows, takeMin)
            ### createSparseCOOArrays(x, row, col, data, rows, cols)
            opEl = calcOperatorElement3D(x, y, z, 0, sigmaMat, numOfYrows, numOfXrows, takeMin)
            if opEl == 0:
                    deletedInd.append(row)
                    counterDelInd += 1
                    continue
            createSparseCOOArrays(opEl, row-counterDelInd, row, data, rows, cols) #row-counterDelInd
            col = row + 1
            if (row + 1) % numOfXrows != 0:
                opEl = calcOperatorElement3D(x, y, z, 1, sigmaMat, numOfYrows, numOfXrows, takeMin)
                createSparseCOOArrays(opEl, row-counterDelInd, col, data, rows, cols)
            col = row + numOfXrows
            if (row % (numOfXrows * (numOfYrows - 1) + numOfXrows)) <= (
                                                numOfXrows * (numOfYrows - 1) - 1):
                opEl = calcOperatorElement3D(x, y, z, numOfXrows, sigmaMat, numOfYrows, numOfXrows, takeMin)
                createSparseCOOArrays(opEl, row-counterDelInd, col, data, rows, cols)
            col = row + xTimesY
            if col<matSize:
                opEl = calcOperatorElement3D(x, y, z, xTimesY, sigmaMat, numOfYrows, numOfXrows, takeMin)
                createSparseCOOArrays(opEl, row-counterDelInd, col, data, rows, cols)

                """if row % 100 == 0:
                        processedTime = time.time() - startTime
                        procFraction = float(row + 1) / matSize
                        timeEstimate = int(processedTime * (1 / procFraction - 1))
                        print "elements processed: " + str(row * matSize + col) + " (" + str(procFraction * 100) + "%)"
                        hours = timeEstimate / 60 / 60
                        min = timeEstimate % (60*60) /60
                        print "Estimated time remaining: %d h, %d min and %d sec" % (hours, min, timeEstimate%60 )"""
	
        print "erstellung operator:"
        print time.time() - startTime
        print "len delind: " + str(len(deletedInd))
        print "counter delind: " + str(counterDelInd)
        deleteZeroColsCOO(cols, deletedInd)
        completeCOOMatrix(data, rows, cols)
        print "len non-zero data: " + str(matSize-len(deletedInd))
        global counter
        print "counter of stand-alone pixels: " + str(counter)
        print "len of data: " + str(len(data))
        return deletedInd, data, rows, cols

# @profile
def createSparseCOOArrays(x, row, col, data, rows, cols):
        if x != 0:
                data.append(x)
                rows.append(row)
                cols.append(col)


# @profile
def completeCOOMatrix(data, rows, cols):
        ##### add lower triangular
        startTime = time.time()
        for entry in range(0, len(data)):
                if cols[entry] != rows[entry]: ### dont duplicate main diagonal
                        data.append(data[entry])
                        rows.append(cols[entry])
                        cols.append(rows[entry])
        print "coo komplettieren:"
        print time.time() - startTime


# def findIndex(a, x):
#         'Find leftmost value greater than x'
#         i = bisect_right(a, x)
#         if i != len(a):
#                 return a[i]
#         raise ValueError

def findIndex(a, x):
        'Find rightmost value less than x'
        i = bisect_left(a, x)
        if i:
                return i
        else:
                return 0

# @profile
def deleteZeroColsCOO(cols, delInd):
        startTime = time.time()
        for i in range(0, len(cols)):
                entry = cols[i]
                numDelCols = findIndex(delInd, entry)
                cols[i] -= numDelCols
        print "zeilen/spalten loeschen:"
        print time.time() - startTime



