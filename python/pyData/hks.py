# coding: utf-8
import numpy as np
import math
import struct
#from testMatrizen import *
import sys
import usefulMethods as um

#important info: calculated eigenvectors are stored in columns, NOT in rows. They are already normalised.

np.set_printoptions(linewidth=400)

"""TODO: ist row in delInd, return 0. kein grund durch die zeitschleife zu laufen, will eh keine hks berechnen.
 wie finde ich effizient heraus, ob row eine deletedRow ist? vergleiche mit einträgen von delind. ist gleich?
 dann ignoriere zeile. sobald einträge in delind größer, stoppe, aber merke dir den letzten index (als die zahl noch
 kleiner oder gleich war). für das nächste xyz - vorausgesetzt, wir wollen eh alle berechnen - muss ich dann erst bei
 diesem index anfangen zu vergleichen. falls nur ausgewählte pixel betrachtet werden sollen, muss ich immer alles durchgehen,
 da diese ja in beliebiger reihenfolge angegeben sein können.
"""
#@profile
def determNumOfDelRows(row, lastdelIndIdx, deletedInd):
    i = lastdelIndIdx
    rowWasDeleted = False

    # reason for using struct here: getting rid of the need to read complete file into memory -> memory efficient
    deletedInd.seek((i * 4)+4) # pass first 4 bytes, they are length of deletedInd
    indArr = deletedInd.read(4)
    ind = struct.unpack('i', indArr)[0]

    while (row >= ind):
        lastdelIndIdx = i + 1       ### save last index in delInd to not have to search from beginning every time
        if row == ind:    ### row was zero and got deleted
            rowWasDeleted = True    ### -> ignore it
            break
        else:
            i += 1
    return lastdelIndIdx, rowWasDeleted
#@profile
def determineRowNumber(x,y,z, sigmamat):
    ####in which row of u(x,y,z) do we find xi,yi?
    if (sigmamat.ndim == 2):
        return y * len(sigmamat[0]) + x
    elif (sigmamat.ndim == 3):
        return y * len(sigmamat[0][0]) + z * len(sigmamat[0][0]) * len(sigmamat[0]) + x
    else:
        print "dimension of array has to be 2 or 3"
        sys.exit("dimension of array has to be 2 or 3")



""" HEAT KERNEL """
#@profile
def heatKernel(row, t, lastDelIndIdx, eigval, eigvec, sigmaMat, printKernel):
    hk = 0
    heatTrace = 0

    ## determine order of magnitude
    expo = 0
    expo = t*eigval[-1]


    for i in range(0, len(eigval)):

        tmp = math.exp(t*eigval[i]-expo)
        # tmp = math.exp(t * eigval[i])
        heatTrace += tmp

        phi = eigvec[row - lastDelIndIdx][i]
        hk += phi * phi * tmp
        if printKernel :  #and math.log(t+1) >5.2
            print "t: " + str(math.log(t+1))
            print "t real: " + str(t)
            print "eigval: " + str(eigval[i])
            # print "mant: " + mant
            # print "mag: " + str(mag)
            print "c: %.4e" % expo
            print "expo: %.10e" %(t*eigval[i]) #%(t*(eigval[i])) #%(t*(eigval[i]-c))
            print "exp(t*(eigval[i]-c)): %.3e" %(tmp)
            print "exp(t*eigval[i]): %.3e" % (math.exp(t*eigval[i]))
            print "heattrace: %.3e" % heatTrace
            print "phi: " + str(phi)
            print "hk: %.3e" %(hk)
            print ""
    hk = hk/heatTrace if heatTrace != 0 else hk
    if printKernel :
        print "################################################################################################"
        print "trace: %.4e" %heatTrace
        print "HK/HEATTRACE: %.4e"  %hk
        print "################################################################################################"
    return hk



""" HEAT KERNEL SIGNATURE """
#@profile
def hkSignature(x, y, z, tstart, tend, tinterval, lastdelIndIdx, eigval, eigvec, deletedInd, lenDeletedInd, sigmamat, printKernel=False):
    yVal = []
    row = determineRowNumber(x, y, z, sigmamat)

    if deletedInd and lastdelIndIdx<lenDeletedInd:
        lastdelIndIdx, rowWasDeleted = determNumOfDelRows(row, lastdelIndIdx, deletedInd)
        if rowWasDeleted:
            #print "row was deleted, return 0"
            return 0, lastdelIndIdx


    for t in np.arange(tstart, tend, tinterval):
        hk = heatKernel(row, math.exp(t) - 1, lastdelIndIdx, eigval, eigvec, sigmamat, printKernel)
        yVal.append(hk)
    return yVal, lastdelIndIdx


#@profile
def getMinOfHKS(lst, currMin):
    minLst = min(lst)
    if minLst < currMin:
        return minLst
    return currMin

#@profile
def getMaxOfHKS(lst, currMax):
    maxLst = max(lst)
    if maxLst > currMax:
        return maxLst
    return currMax

#@profile
def calcHKScomplete(sigmamat, tstart, tend, eigval, eigvec, delInd, save=True, name='result'):
    """
    :does: calculate hksignature for all non-zero entries in sigmamat
    :return: list ready to be stored in hks format
    """
    maxVal = 0
    minVal = 1e9
    lastdelIndIdx=0
    tinterval = (tend-tstart)/100.

    lenDelIndArr = delInd.read(4)
    lenDelInd = struct.unpack('i', lenDelIndArr)[0]

    result = [sigmamat.ndim]  # dimension
    if sigmamat.ndim==3:
        result.append(len(sigmamat) * len(sigmamat[0]) * len(sigmamat[0][0]) - lenDelInd)  # number of voxels to be considered
    else:
        result.append(len(sigmamat) * len(sigmamat[0]) - lenDelInd)
    #result.append(int((tend - tstart) / tinterval))  # number of heatkernels/time points to be calculated
    xVal = []
    counter = 0
    for t in np.arange(tstart, tend, tinterval):
        counter += 1
        xVal.append(t)  # math.exp(t)
    result.append(counter) # number of heatkernels/time points to be calculated
    result.append(xVal)
    hksList = []
    #  hkSignature(x, y, z, tstart, tend, tinterval, lastdelIndIdx, eigval, eigvec, deletedInd, lenDeletedInd, sigmamat, printKernel=False):
    if (sigmamat.ndim==3):
        for i in range(0, len(sigmamat)):  # z
            for j in range(0, len(sigmamat[0])):  # y
                for k in range(0, len(sigmamat[0][0])):  # x
                    px, lastdelIndIdx = hkSignature(k, j, i, tstart, tend, tinterval, lastdelIndIdx, eigval, eigvec,
                                                    delInd, lenDelInd, sigmamat)
                    if px == 0:
                        continue
                    maxVal = getMaxOfHKS(px, maxVal)
                    minVal = getMinOfHKS(px, minVal)
                    hksList.append([(k,j,i), px])
    if (sigmamat.ndim==2):
        for i in range(0, len(sigmamat)):  # y
            for j in range(0, len(sigmamat[0])):  # x
                px, lastdelIndIdx = hkSignature(j, i, 0, tstart, tend, tinterval, lastdelIndIdx, eigval, eigvec, delInd, lenDelInd,
                                             sigmamat)
                if px == 0:
                    continue
                maxVal = getMaxOfHKS(px, maxVal)
                minVal = getMinOfHKS(px, minVal)
                hksList.append([(j,i), px])

    result.append(hksList)
    result.append(minVal)
    result.append(maxVal)
    if save:
        um.saveToHKS('hksFiles/completeHKSsigmamat'+name,result)
    return result


def calcHKSForPoints(listOfPixels, tstart, tend, eigval, eigvec, sigmamat, delInd, save=True, name='result', printKernel=False):
    maxVal = 0
    minVal = 1e9
    result = [sigmamat.ndim] #dimension
    tinterval = (tend - tstart) / 100.

    lenDelIndArr = delInd.read(4)
    lenDelInd = struct.unpack('i', lenDelIndArr)[0]

    # result.append(len(sigmamat)*len(sigmamat[0])*len(sigmamat[0][0])-len(delInd)) #number of voxels to be considered
    result.append(len(listOfPixels)) #number of voxels to be considered - ONLY WORKS IF NO ZERO-VOXELS ARE IN LISTOFPIXELS! therefore, counter2
    xVal = []
    counter = 0 #number of heatkernels/time points to be calculated
    for t in np.arange(tstart, tend, tinterval):
        counter += 1
        xVal.append(t)  # math.exp(t)
    result.append(counter)  # number of heatkernels/time points to be calculated
    result.append(xVal)
    hksList = []
    counter2 = 0 # number of non-zero voxels
    if (sigmamat.ndim == 3):
        for entry in listOfPixels:
            #print "wert von " + str(entry) + " in sigmamat:"
            #print sigmamat[entry[2]][entry[1]][entry[0]]
            px, _ = hkSignature(entry[0], entry[1], entry[2], tstart, tend, tinterval, 0, eigval, eigvec,
                                delInd, lenDelInd, sigmamat, printKernel)
            if px == 0:
		print "hks was zero for point " + str(entry) + ", density on point was " + str(sigmamat[entry[2], entry[1], entry[0]])
                continue
            counter2 += 1
            maxVal = getMaxOfHKS(px, maxVal)
            minVal = getMinOfHKS(px, minVal)
            hksList.append([entry, px])
    if (sigmamat.ndim == 2):
        for entry in listOfPixels:
            px, _ = hkSignature(entry[0], entry[1], 0, tstart, tend, tinterval, 0, eigval, eigvec, delInd, lenDelInd,
                                             sigmamat, printKernel)
            if px == 0:
                continue
            counter2 += 1
            maxVal = getMaxOfHKS(px, maxVal)
            minVal = getMinOfHKS(px, minVal)
            hksList.append([entry, px])

    if counter2<len(listOfPixels):
        result[1] = counter2
        print "there were zero-voxels in your voxel list"
    result.append(hksList)
    result.append(minVal)
    result.append(maxVal)
    if save:
        um.saveToHKS('hksFiles/pointsHKS'+name, result)
    # print result
    return result





