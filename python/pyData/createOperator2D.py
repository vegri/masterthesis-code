# coding:utf-8

import numpy as np


np.set_printoptions(linewidth=1800)


def calcOperatorElement2D(line, diagonal, sigmaMat, numOfYrows, takeMin):

    y = (line / numOfYrows)
    x = (line % numOfYrows)

    currentPixel = sigmaMat[y][x]

    if currentPixel==0:
        return 0

    if takeMin==False:
        if(diagonal==0):
            origPixel = 0
            if (x+1)<len(sigmaMat[0]):
                rightNeighbour = sigmaMat[y][x+1]
                if rightNeighbour!=0:
                    origPixel += (currentPixel+rightNeighbour)/2.0
            if (x - 1) >= 0:
                leftNeighbour = sigmaMat[y][x - 1]
                if leftNeighbour!=0:
                    origPixel += (currentPixel + leftNeighbour) / 2.0
            if (y - 1) >= 0:
                upperNeighbour = sigmaMat[y-1][x]
                if upperNeighbour!=0:
                    origPixel += (currentPixel + upperNeighbour) / 2.0
            if (y+1)<len(sigmaMat):
                lowerNeighbour = sigmaMat[y+1][x]
                if lowerNeighbour!=0:
                    origPixel += (currentPixel + lowerNeighbour) / 2.0
            return -origPixel
        if diagonal==1:
            return (currentPixel+sigmaMat[y][x+1])/2.0
        if diagonal == -1:
            return (currentPixel + sigmaMat[y][x-1]) / 2.0
        if diagonal == numOfYrows:
            return (currentPixel + sigmaMat[y+1][x]) / 2.0
        if diagonal == -numOfYrows:
            return (currentPixel + sigmaMat[y-1][x]) / 2.0
    else:
        print "minimum is taken"
        if(diagonal==0):
            origPixel = 0
            if (x+1)<len(sigmaMat[0]):
                rightNeighbour = sigmaMat[y][x+1]
                if rightNeighbour!=0:
                    origPixel += min(currentPixel,rightNeighbour)
            if (x - 1) >= 0:
                leftNeighbour = sigmaMat[y][x - 1]
                if leftNeighbour!=0:
                    origPixel += min(currentPixel, leftNeighbour)
            if (y - 1) >= 0:
                upperNeighbour = sigmaMat[y-1][x]
                if upperNeighbour!=0:
                    origPixel += min(currentPixel, upperNeighbour)
            if (y+1)<len(sigmaMat):
                lowerNeighbour = sigmaMat[y+1][x]
                if lowerNeighbour!=0:
                    origPixel += min(currentPixel, lowerNeighbour)
            return -origPixel
        if diagonal==1:
            return min(currentPixel, sigmaMat[y][x+1])
        if diagonal == -1:
            return min(currentPixel, sigmaMat[y][x-1])
        if diagonal == numOfYrows:
            return min(currentPixel, sigmaMat[y+1][x])
        if diagonal == -numOfYrows:
            return min(currentPixel, sigmaMat[y-1][x])

def createOperator2D(sigmaMat, takeMin=False):
    numOfRows = len(sigmaMat)
    numOfColumns = len(sigmaMat[0])
    # print "numofcolumns: " +str(numOfColumns)
    matSize=numOfRows*numOfColumns
    operator = np.zeros(shape=(matSize,matSize))

    for line in range(0, matSize):
        for row in range(0, matSize):
            if line==row:
                operator[line][row]= calcOperatorElement2D(line, 0, sigmaMat, numOfColumns, takeMin)
            if row-line== 1 and (line+1)%numOfColumns!=0:
                operator[line][row] = calcOperatorElement2D(line, 1, sigmaMat, numOfColumns, takeMin)
            if row-line==-1 and (line)%(numOfColumns)!=0:
                operator[line][row] = calcOperatorElement2D(line, -1, sigmaMat, numOfColumns, takeMin)
            if row-line==numOfColumns:
                # print "num colums = row- line = " + str(row-line)
                operator[line][row] =  calcOperatorElement2D(line, numOfColumns, sigmaMat, numOfColumns, takeMin)
            if row-line==-numOfColumns:
                # print "num colums = -(row- line) = " + str(-row + line)
                operator[line][row] = calcOperatorElement2D(line, -numOfColumns, sigmaMat, numOfColumns, takeMin)

    # deletedInd = []
    # return deletedInd, operator
    """es werden trotzdem die durchschnitte mit den nullpixeln gebildet, zwar nicht auf der hauptdiagonalen, aber
    auf den nebendiagonalen. das fällt nur im unittest nicht auf, weil die nullzeilen und spalten nur anhand der haupt-
    diagonalenelementes gelöscht werden, und damit auch die entsprechenden einträge wegfallen. das funzt so in 3D aber nicht"""
    return deleteZeroRowsAndCols(operator)


def deleteZeroRowsAndCols(operator):
    opSize = len(operator)
    #np.savetxt('operatorOrig.txt', operator, fmt='%1.0f')
    deletedInd = []
    i=0
    while i < opSize: ### save deleted indices
            if operator[i][i]==0:
                deletedInd.append(i)
            i+=1
    # print "done with while, number of deleted inds: " + str(len(deletedInd))
    for j in range(0, len(deletedInd)):
        operator = np.delete(operator, deletedInd[j]-j, 0) ### delete row with number deletedInd[j]-j
        operator = np.delete(operator, deletedInd[j]-j, 1) ### delete column with number   "
    np.savetxt('operatorDeleted.txt', operator, fmt='%1.1f')
    print operator
    return deletedInd, operator