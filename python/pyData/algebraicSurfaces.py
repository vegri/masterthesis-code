#coding: utf-8

import usefulMethods as um
import math
import numpy as np
from test import makeHistogramm
import time
#see http://homepage.univie.ac.at/herwig.hauser/bildergalerie/gallery.html

path1 = '/home/vegrimm/Desktop/Masterarbeit/python/'
path2 = "/home/vegrimm/Desktop/Masterarbeit/qtTest/"

globmin = []

def cube(x,y,z):
    if (x**6+y**6+z**6)<=1:
        return True
    else:
        return False

def star(x,y,z):
    if z**4*x**4+2*z**6*x**2-1*z**4*x**2*y**2 +2*z**6*y**2+2*z**4*y**4-6*z**4*x**2-6*z**4*y**2-10*x**2*y**2*z**2+15*x**2*y**2-1 <=0:
        return True
    else:
        return False

def pegTop(x,y,z): ## how to minimise number of vertices? giant!
    a = x*x + y*y
    b = (60 - a - z*z)
    # if (b**3 - 60*a*z**4) <= 0:
    if ((60 - x*x - y*y - z*z)**3 - 60*(x*x + y*y)*z**4) <= 0:
        return True
    else:
        return False

def dingdong(x,y,z): ## how to minimise number of vertices? giant!
    if (x*x + y*y + z*z*z - z*z) <= 0:
        return True
    else:
        return False

def zeppelin(x,y,z):
    if (x*y*z+y*z+2*z**5) <= 0:
        return True
    else:
        return False

def nepali(x,y,z):
    if ((x*y-z*z*z-1)**2 + (x*x+y*y-1)**3) <= 0:
        return True
    else:
        return False

def eve(x,y,z): ## results in lemon. why?
    if (5*x*x + 2*x*z*z + 5*y**6 + 15*y**4 + 5*z*z - 15*y**5 - 5*y**3) <=0:
        return True
    else:
        return False

def crixxi(x,y,z):

    if (y*y + z*z - 1)**2 + (x*x + y*y - 1) ** 3 <= 0:
        return True
    else:
        return False

def lemon(x,y,z):
    if (x*x + z*z - y*y*y*(1-y)**3) <= 0:
        return True
    else:
        return False

def knife(x,y,z):
   # if ((0.2 - (x*x + y - z*z)**2 + (math.cos(x + math.sin(x) / 8) + math.cos(y + math.sin(y) / 8) + math.cos(z + math.sin(z) / 8))) <= 0) \
   #         and (((x**2+y**2)<1) or ((y**2+z**2)<1) or((z**2+x**2)<1) ):
   if ((0.2 - (x * x + y - z * z) ** 2 + (
           math.cos(x + math.sin(x) / 8) + math.cos(y + math.sin(y) / 8) )) <= 0) \
           and (((x ** 2 + y ** 2) < 1) or ((y ** 2 + z ** 2) < 1) or ((z ** 2 + x ** 2) < 1)):
       return True
   else:
       return False

def amboss(x, y, z):
    tmp = ((x*x+y*y)**2/2.5-x*(x*x-3*y*y))**3+z*z-1
    # globmin.append(tmp)
    if  tmp<=0:
        # if tmp < globmin:
        #     globmin = tmp
        return True
    else:
        return False

def ambossDensity(x,y,z):
    return ((x*x+y*y)**2/2.5-x*(x*x-3*y*y))**3+z*z-1

def findmin(x,y,z, minx, miny,minz):
    if x<minx:
        minx=x
    if y<miny:
        miny=y
    if z<minz:
        minz=z
    return minx, miny, minz

def findmax(x,y,z, maxx, maxy,maxz):
    if x>maxx:
        maxx=x
    if y>maxy:
        maxy=y
    if z>maxz:
        maxz=z
    return maxx, maxy, maxz

def saveForQT(path2, filename, counterQT, forQT):
    with open(path2+filename+'.txt','w') as file:
        file.write(str(counterQt))
        file.write('\n')
        for i in range(0, len(forQT), 4):
            x = forQT[i]
            y = forQT[i+1]
            z = forQT[i+2]
            # x2, y2, z2 = matrixVecMult(x, y, z, rotMatX(pi*0.25))
            file.write(str(x) + " " + str(y) + " " + str(z) + " " + str(forQT[i + 3]))
            file.write('\n')

forHKS = []
forQT = []
xmax = 0 # für starBig und alles andere meistes 120; starBig geteilt durch 20, 20, 50
ymax = 20
zmax = 20

counter2 = 0
counter3 = 0
minx, miny, minz = 1000,1000,1000
maxx, maxy, maxz = -1000, -1000, -1000
# forHKS.extend((xmax*2,ymax*2,zmax*2))
forHKS.extend((480,480,470))

# for z in range(-zmax,zmax, 1):
#     for y in range(-ymax, ymax, 1):
#         for x in range(-xmax, xmax, 1):

def createSurfaceAmboss(forHKS, forQT, small=True):
    counterQt = 0
    counterZero = 0
    for z in range(-200,270):
        for y in range(-240, 240):
            for x in range(-240, 240):
                """ this is for rotating for hks """
                # # x2, y2, z2 = um.matrixVecMult(x, y, z, um.rotMatY(math.pi * 0.25))
                # # x2, y2, z2 = um.reflectionMat2(x,y,z,1,1,1,0)
                # # x2, y2, z2 = matrixVecMult(x, y, z, rotMatX(pi * 0.5))
                # if amboss(x/30., y/30., z/30.) : #x/190.,y/190.,z/190. # and x>0
                # # if amboss(x2 / 30., y2 / 30., z2 / 30.):
                #     counterQt += 1
                #     forHKS.append(1)
                #     forQT.extend((x + 240, y + 240, z + 200, 1))
                #     # forQT.extend((x , y , z , 1))
                if small:
                    density = ambossDensity(x / 10., y / 10., z / 10.)
                else:
                    density = ambossDensity(x/30., y/30., z/30.)
                if density <= 0:
                    counterQt += 1
                    if abs(density) < 0.1:  # 0.1 for small anvil, 0.01 for big anvil (although almost no difference)
                        # counter2 += 1
                        density = 1
                    else:
                        # counter3 += 1
                        # density = int(abs(density)+1)
                        # density = int(abs(density)*10)  # for small anvil
                        density = int(abs(density) * 100)  # for big anvil
                    globmin.append(density)
                    forHKS.append(density)
                    forQT.extend((x + 240, y + 240, z + 200, density))
                else:
                    counterZero += 1
                    forHKS.append(0)
    print "counter z: " + str(counterZero)
    print "non-zero vertices: " + str(counterQt)
    print len(forQT) / 4
    print len(forHKS)
    return counterQt


# sourcefile = 'ambossBig'
# sourcefile = 'amboss'
sourcefile = 'ambossBigDensity'
# sourcefile = 'ambossDensity'

# filename='amboss'  #geteilt durch 10, dim: -200 bis 270, zweimal -240 bis 240
# filename='ambossDensityRotx90'
# filename='ambossDensityRoty45'
# filename='ambossDensityRotz30Roty56'

# filename='ambossDensity'
# filename='ambossDensityRotx90'
# filename='ambossDensityRoty45'
# filename='ambossDensityRotz30Roty56'

# filename='ambossBig' # geteilt durch 30, dim: -200 bis 270, zweimal -240 bis 240
# filename='ambossBigRotx90'
# filename='ambossBigRoty45'
# interfilename='ambossBigRotz30'
# filename='ambossBigRotz30Roty56'
# interfilename='ambossBigRotx13'
# filename='ambossBigRotx13Roty5'

filename='ambossBigDensity'
filename='ambossBigDensity547'
# filename='ambossBigDensityRotx90'
# filename='ambossBigDensityRoty45'
# interfilename='ambossBigDensityRotz30'
# filename='ambossBigDensityRotz30Roty56'
# interfilename='ambossBigDensityRotx13'
# filename='ambossBigDensityRotx13Roty5'



### check forQT if points dont match with forHKS
# tmp = np.array(forHKS[3:])
# tmp = tmp.reshape((470, 480, 480))
# for z in range(0,470):
#     for y in range(0, 480):
#         for x in range(0, 480):
#             if tmp[z][y][x]==1:
#                 forQT.extend((x, y, z, 1))

startTime = time.time()

counterQt = createSurfaceAmboss(forHKS,forQT,small=False)
makeHistogramm(globmin, filename)
# um.saveBinList(path1+filename, forHKS, isInt=True)
saveForQT(path2, filename, counterQt, forQT)


# um.rotateWithScipy(sourcefile,filename,90)
# um.rotateWithScipy(sourcefile,filename,45, axes=(2,0))
# um.rotateWithScipy(sourcefile,interfilename,30, axes=(1,2))
# um.rotateWithScipy(interfilename,filename,56, axes=(2,0))
# um.rotateWithScipy(sourcefile,interfilename,13)
# um.rotateWithScipy(interfilename,filename,5, axes=(2,0))

print "time:"
print (time.time() - startTime)