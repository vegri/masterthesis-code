# coding: utf-8
import time
import numpy as np
from scipy import sparse, io
from multiprocessing import Pool
from scipy.sparse.linalg import eigsh
#import sparseMatrix



def calcOperatorElement3D(line, diagonal, sigmaMat, numOfYrows, numOfXrows):

    q= line / (numOfYrows * numOfXrows)                                                                         # z-component
    l = (line - (line % numOfXrows)) / numOfXrows - (line - (line % (numOfYrows * numOfXrows))) / numOfXrows    # y-component
    k = line % numOfXrows                                                                                       # x-component


    if (diagonal == 0):
        origPixel = 0
        try:
            origPixel += (sigmaMat[q][l][k] + sigmaMat[q][l + 1][k]) / 2.0
        except:
            origPixel += 0  # sigmaMat[q][l][k] / 2.0
        if (l - 1) >= 0:
            origPixel += (sigmaMat[q][l][k] + sigmaMat[q][l - 1][k]) / 2.0
        else:
            origPixel += 0  # sigmaMat[q][l][k] / 2.0
        try:
            origPixel += (sigmaMat[q][l][k] + sigmaMat[q + 1][l][k]) / 2.0
        except:
            origPixel += 0  # sigmaMat[q][l][k] / 2.0
        if (q - 1) >= 0:
            origPixel += (sigmaMat[q][l][k] + sigmaMat[q - 1][l][k]) / 2.0
        else:
            origPixel += 0  # sigmaMat[q][l][k] / 2.0
        try:
            origPixel += (sigmaMat[q][l][k] + sigmaMat[q][l][k + 1]) / 2.0
        except:
            origPixel += 0  # sigmaMat[q][l][k] / 2.0
        if (k - 1) >= 0:
            origPixel += (sigmaMat[q][l][k] + sigmaMat[q][l][k - 1]) / 2.0
        else:
            origPixel += 0  # sigmaMat[q][l][k] / 2.0

        return -origPixel
    if diagonal == 1:
        return (sigmaMat[q][l][k + 1] + sigmaMat[q][l][k]) / 2.0
    if diagonal == -1:
        return (sigmaMat[q][l][k - 1] + sigmaMat[q][l][k]) / 2.0
    if diagonal == numOfXrows:
        return (sigmaMat[q][l + 1][k] + sigmaMat[q][l][k]) / 2.0
    if diagonal == -numOfXrows:
        return (sigmaMat[q][l - 1][k] + sigmaMat[q][l][k]) / 2.0
    if diagonal == numOfXrows * numOfYrows:
        return (sigmaMat[q + 1][l][k] + sigmaMat[q][l][k]) / 2.0
    if diagonal == -numOfXrows * numOfYrows:
        return (sigmaMat[q - 1][l][k] + sigmaMat[q][l][k]) / 2.0



def createOperator3D(sigmaMat):
    numOfZrows = len(sigmaMat)
    numOfYrows = len(sigmaMat[0])
    numOfXrows = len(sigmaMat[0][0])
    matSize = numOfXrows * numOfYrows * numOfZrows
    # print "matsize: " + str(matSize)
    operator3D = np.zeros(shape=(matSize, matSize))
    print "creating operator..."
    for i in range(0, matSize):
        for j in range(0, matSize):
	    if (i*matSize+j)%10000==0:
		print "elements processed: " + str(i+j)
            # print "j = " + str(j)
            if i == j:
                # print "i == j"
                operator3D[i][j] = calcOperatorElement3D(i, 0, sigmaMat, numOfYrows, numOfXrows)
            if j - i == 1 and (i + 1) % numOfXrows != 0:
                # print "j - i == 1"
                operator3D[i][j] = calcOperatorElement3D(i, 1, sigmaMat, numOfYrows, numOfXrows)
            if j - i == -1 and i % numOfXrows != 0:
                # print "j - i == -1"
                operator3D[i][j] = calcOperatorElement3D(i, -1, sigmaMat, numOfYrows, numOfXrows)
            if j - i == numOfXrows and (i % (numOfXrows*(numOfYrows-1)+numOfXrows)) <= (numOfXrows*(numOfYrows-1)-1):
                # print "j - i == numOfXrows:"
                operator3D[i][j] = calcOperatorElement3D(i, numOfXrows, sigmaMat, numOfYrows, numOfXrows)
            if j - i == -numOfXrows and ((i-numOfXrows) % (numOfXrows*(numOfYrows-1)+numOfXrows)) <= (numOfXrows*(numOfYrows-1)-1):
                # print "j - i == -numOfXrows"
                operator3D[i][j] = calcOperatorElement3D(i, -numOfXrows, sigmaMat, numOfYrows, numOfXrows)
            if j - i == numOfYrows*numOfXrows:
                # print "j - i == numOfYrows*numOfXrows"
                operator3D[i][j] = calcOperatorElement3D(i, numOfYrows * numOfXrows, sigmaMat, numOfYrows, numOfXrows)
            if j - i == -numOfYrows*numOfXrows:
                # print "j - i == -numOfYrows*numOfXrows"
                operator3D[i][j] = calcOperatorElement3D(i, -numOfYrows* numOfXrows, sigmaMat, numOfYrows, numOfXrows)

    deletedInd = []
    # print operator3D
    print "created operator, now deleting lines"
    # makeSparseMatrix(operator)
    # np.savetxt('operatorOld.txt', operator, fmt='%1.0f')
    # return operator3D
    return deleteZeroRowsAndCols(operator3D, matSize)
    #np.savetxt('operatorOld.txt', operator3D, fmt='%1.0f')
    # return matSize, deletedInd, operator3D
    #return deleteZeroRowsAndCols(operator3D, matSize)


def deleteZeroRowsAndCols(operator, matSize):
    opSize = len(operator)
    #np.savetxt('operatorOrig.txt', operator, fmt='%1.0f')
    deletedInd = []
    i=0
    while i < opSize: ### save deleted indices
            if operator[i][i]==0:
                deletedInd.append(i)
            i+=1
    # print "done with while, number of deleted inds: " + str(len(deletedInd))
    for j in range(0, len(deletedInd)):
        operator = np.delete(operator, deletedInd[j]-j, 0) ### delete row with number deletedInd[j]-j
        operator = np.delete(operator, deletedInd[j]-j, 1) ### delete column with number   "
    np.savetxt('operatorDeleted.txt', operator, fmt='%1.1f')
    return deletedInd, operator
    # return matSize, deletedInd, makeSparseMatrix(operator)

def makeSparseMatrix(operator):
    sparseOp = sparse.csr_matrix(operator)
    #np.savetxt('operatorSparse.txt', sparseOp, fmt='%1.0f')
    io.mmwrite('operatorSparse.mtx', sparseOp) #, precision=2, symmetry='symmetric'
    return sparseOp