#coding:utf-8

import numpy as np
#import numpy.linalg as LA
import os
import time
import datetime
import math
import pyData.hks as hks
import pyData.createOperator2D as co2D
# import createOperator3DNumpy as conp
import pyData.createOperator3DSparse as cosp
from pyData.sigmamatX6 import sigmamatX6
from pyData.testMatrizen import *
# from pyData.calcEigenstuff import makeEigen # funzt mit pypy nicht, pypy kennt kein scipy
# from scipy import sparse
import pyData.usefulMethods as um
# from pyData.algebraicSurfaces import  matrixVecMult, rotMatX, rotMatY

#### careful: pypy can pickle data, but python cant read pickled data from pypy!

np.set_printoptions(linewidth=1000)

global path
path = os.path.dirname(os.path.realpath(__file__))
# print path
# path = os.path.abspath(os.path.join(path, os.pardir))


def createOperatorDataCT(name, save=True, takeMin=False):
    boneData = um.readBinList(name, isInt=True)  #### readBinList
    xdim, ydim, zdim = boneData[0], boneData[1], boneData[2]
    print xdim, ydim, zdim
    del boneData[:3]
    boneDataNP = np.array(boneData)
    #boneDataNP = boneDataNP.reshape((zdim, ydim, xdim))
    # print boneDataNP.shape
    delInd, data, rows, cols = cosp.createSparseOperatorCOO(boneDataNP, zdim, ydim, xdim, takeMin)
    if save:
        add = ''
        if takeMin:
            add='takeMin'
            um.saveBinList(path + "/cooData/data"+name+add, data, isInt=True) #saveBinList
        else:
            um.saveBinList(path + "/cooData/data" + name + add, data, isInt=False)  # saveBinList
        um.saveBinList(path + "/cooData/rows"+name+add, rows, isInt=True)  #saveList
        um.saveBinList(path + "/cooData/cols"+name+add, cols, isInt=True)
        um.saveBinList(path + "/deletedInds/delInd" + name+add, delInd, isInt=True)
    return delInd, data, rows, cols

def createOperatorAndEigData2dim(sigmamat, name, save=True, takeMin=False):
    deletedInd, result = co2D.createOperator2D(sigmamat, takeMin)
    val, vec = um.calcEigenstuffSVD(result)
    if save:
        um.saveEigvec(path + "/eigvecs/eigvec"+name, vec)
        um.saveList(path + "/eigvals/eigval"+name, val)
    return deletedInd, val, vec

def createOperator3dim(sigmamat, name, x, y, z, saveCoo=True, saveDelInd=True):
    delInd, data, rows, cols = cosp.createSparseOperatorCOO(sigmamat, z, y, x)
    if saveCoo:
        um.saveBinList(path + "/cooData/data" + name, data, isInt=False)
        um.saveBinList(path + "/cooData/rows" + name, rows, isInt=True)
        um.saveBinList(path + "/cooData/cols" + name, cols, isInt=True)
    if saveDelInd:
        um.saveBinList(path + "/deletedInds/delInd" + name, delInd, isInt=True)
    return delInd, data, rows, cols

def readEigenstuff(s, takeMin=False):
    startTime = time.time()
    add = ''
    if takeMin:
        add='takeMin'
    # deletedInd = um.readBinList(path + "/deletedInds/delInd" +s+add, True)
    val = um.readList(path + "/eigvals/eigval" +s+add, False)
    dim, numofVecs, vec = um.readEigvec(path + "/eigvecs/eigvec" +s+add)
    #val = um.readList("home/vegrimm/Desktop/opInv/eigvals/eigval" +s, False)
    #dim, numofVecs, vec = um.readEigvec2("home/vegrimm/Desktop/opInv/eigvecs/eigvec" +s)
    print "read delInd, val, vecs (" + str(time.time() - startTime) +")"
    # return deletedInd, val, vec
    return val, vec

def readCT(s):
    startTime = time.time()
    boneData = um.readBinList(s, True)
    xdim, ydim, zdim = boneData[0], boneData[1], boneData[2]
    del boneData[:3]
    boneDataArray = np.array(boneData)
    print "read CT data (" + str(time.time() - startTime) +")"
    return boneDataArray.reshape((zdim, ydim, xdim))

"""#####################################################################################################################"""


"""    create/save operator data for CT with pypy    """
startTime = time.time()
# s = "ambossBig" # 506799 operator same, hks with shift mod: 2911 sec
s = "ambossBigDensity"  #  506799 operator same, eigcalc:  3918 sec, hks: 857 sec
s = "ambossBigDensity547"
# s = "ambossBigRoty45" #  506851  , hks with shift mod: 3010 sec
s = "amboss" #  18770 operator same, eigcalc:
# s = "ambossDensity" #  18770 operator same,  eigcalc: 77 sec, hks: 87 sec

# s = "bunnyBig"  #  voxeliert, 673738 voxel, operator: 673738 voxel, eigcalc: 2276sec, hks: 966sec
# s = "bunnyBigRot90xAxis"  # 673738 voxefl, operator: same, eigcalc: 2385 sec, hks: 1516
# s = "bunnyBigRot45yAxis"  # 673627 voxel, operator same, eigcalc: 2643 sec, hks: 1008
# s = "bunnySmall"  # voxeliert, 24907 voxel, operator: 24906 voxel, eigcalc: 42 sec, hks: 38sec
# s = "bunnySmallRot90xAxis" # 24907 voxel, operator: 24906 voxel, eigcalc: 58 sec, hks: 49 sec
# s = "bunnySmallRot45yAxis"  # 24789 voxel, operator: 24788 voxel, eigcalc: 53 sec, hks: 44 sec

# s = '1_2_826_0_1_3680043_2_1125_1_41151057872594974555753564137181835spacing08-08-08'  # hks funzt nicht
# s = '1_2_826_0_1_3680043_2_1125_1_41151057872594974555753564137181835spacing08-08-08only1s'

# s2 = '1_2_826_0_1_3680043_2_1125_1_81585748112514251817903140826310781spacing08-08-08'
# s = '1_2_826_0_1_3680043_2_1125_1_81585748112514251817903140826310781spacing08-08-08only1s'
# s2 = '1_2_826_0_1_3680043_2_1125_1_81585748112514251817903140826310781spacing08-08-08lessDense'

# s3 = '1_2_826_0_1_3680043_2_1125_1_52973447532876540775084415735195358spacing07-07-07'
# s = '1_2_826_0_1_3680043_2_1125_1_52973447532876540775084415735195358spacing07-07-07only1s'
# s3 = '1_2_826_0_1_3680043_2_1125_1_52973447532876540775084415735195358spacing07-07-07lessDense'

# s4 = '1_2_826_0_1_3680043_2_1125_1_43544977205268791933160390680029048spacing077-077-077'
# s = '1_2_826_0_1_3680043_2_1125_1_43544977205268791933160390680029048spacing077-077-077only1s'

# s5 = '1_2_826_0_1_3680043_2_1125_1_6684150113536839605716867710154046spacing072-072-072'
# s = '1_2_826_0_1_3680043_2_1125_1_6684150113536839605716867710154046spacing072-072-072only1s'

# s6 = '1_2_826_0_1_3680043_2_1125_1_44650512280840673505894811634773432spacing063-063-063'
# s = '1_2_826_0_1_3680043_2_1125_1_44650512280840673505894811634773432spacing063-063-063only1s'

# s7 = '1_2_826_0_1_3680043_2_1125_1_54286138000349764725037931753924822spacing076-076-076'
# s = '1_2_826_0_1_3680043_2_1125_1_54286138000349764725037931753924822spacing076-076-076only1s'

# s8 = '1_2_826_0_1_3680043_2_1125_1_78514869166001886767502631794273095spacing065-065-065'
# s = '1_2_826_0_1_3680043_2_1125_1_78514869166001886767502631794273095spacing065-065-065only1s'
# s8 = '1_2_826_0_1_3680043_2_1125_1_78514869166001886767502631794273095spacing065-065-065lessDense'

# s3 = "1_2_826_0_1_3680043_2_1125_1_78514869166001886767502631794273095spacing06535"
# s4 = "1_2_826_0_1_3680043_2_1125_1_78514869166001886767502631794273095spacing0658"
s3 = "1_2_826_0_1_3680043_2_1125_1_78514869166001886767502631794273095spacing06535only1s"
s4 = "1_2_826_0_1_3680043_2_1125_1_78514869166001886767502631794273095spacing0658only1s"

# s='ambossBigRotx90'
# s='ambossBigRoty45'
# s='ambossBigRotz30Roty56'
# s1='ambossBigRotx13'
# s2='ambossBigRotx13Roty5'


# s0='ambossBigDensityRotx90'
# s1='ambossBigDensityRoty45'
# s2='ambossBigDensityRotz30Roty56'
# s3='ambossBigDensityRotx13'
# s4='ambossBigDensityRotx13Roty5'


# s='bunnyBigRotx90'
# s='bunnyBigRoty45'
# s='bunnyBigRotz30Roty56'
# s3='bunnyBigRotx13'   #  675707 voxels
# s4='bunnyBigRotx13Roty5' # 676308 voxels

# filenames = ['bunnySmallRotx90', 'bunnySmallRoty45', 'bunnySmallRotz30Roty56', 'bunnyBigRotx90', 'bunnyBigRoty45', 'bunnyBigRotz30Roty56']
# filenames = ['ambossBigDensityRotx90', 'ambossBigDensityRoty45', 'ambossBigDensityRotz30Roty56']
# filenames = [s8, s7, s5, s4, s3, s2]
filenames = [s3,s4]
# filenames = [s,s0,s1,s2]
# for s in filenames:
#     print s
#     createOperatorDataCT(s)

# createOperatorDataCT(s)
# print s
# print "erstellen delInd, data, rows, cols:"
# print time.time() - startTime



"""    when done, use makeEigen with python (uses scipy, therefore no pypy)    """
"""    calculate/save hks for CT    """

for s in filenames:
    startTime = time.time()
    print "start: " + str(datetime.datetime.now())
    print s
    boneData = readCT(s)
    # deletedInd, val, vec = readEigenstuff(s)  # , takeMin=True
    val, vec = readEigenstuff(s)
    deletedInd = open('/home/vegrimm/Desktop/Masterarbeit/python/deletedInds/delInd'+s, 'rb')
    hks.calcHKScomplete(boneData, 0, 10., val, vec, deletedInd, name=s) ##
    print "hks berechnet in:"
    print time.time() - startTime, (time.time() - startTime)/60.

# boneData = readCT(s)
# val, vec = readEigenstuff(s)
# deletedInd = open('/home/vegrimm/Desktop/Masterarbeit/python/deletedInds/delInd'+s, 'rb')

startTime = time.time()
print "start: " + str(datetime.datetime.now())
# hks.calcHKScomplete(boneData, 0, 5.5, val, vec, deletedInd, name=s)
# hks.calcHKScomplete(boneData, 0, 7., val, vec, deletedInd, name=s) ## for only1s   #  10.5, 0.07
# hks.calcHKScomplete(boneData, 0, 8., val, vec, deletedInd, name=s) ## for densities   #  10.5, 0.07
# result = hks.calcHKSForPoints([[379, 299, 463], [435, 299, 382], [374, 330, 399]], 0, 10.5, val, vec,boneData,deletedInd, name=s)  # Rotz30Roty56

# result = hks.calcHKSForPoints([[20,40,25], [35,51,32], [13,32,40], [33,11,47]], 0, 5.5, 0.05, val, vec,boneData,deletedInd)
# result = hks.calcHKSForPoints([[20,40,25], [35,51,32], [13,32,40], [33,11,47]], 0, 5.5, 0.05, val, vec,boneData,deletedInd)
#result = hks.calcHKSForPoints([[135 , 18, 212],[132, 91, 8],[163 , 32, 156]], 0, 1.5, 0.025, val, vec,boneData,deletedInd) #,[139 , 20, 127]
#result = hks.calcHKSForPoints([[268,31,191],[308,62,243],[244,59,250],[276,48,297],[177,96,190],[339,97,167]], 0, 5.5, 0.05, val, vec,boneData,deletedInd, name=s) ## 62430spacing05-05-05 ## correct order: nase, rechte obere augenhöhle, linke " ", stirn, jochbein links, jb rechts 
#result = hks.calcHKSForPoints([[170,21,122],[191,38,152],[152,37,157],[174,30,178],[113,57,120],[214,61,106]], 0, 0.25, 0.005, val, vec,boneData,deletedInd, name=s) ## 62430spacing08-08-08 ## order same as above
#result = hks.calcHKSForPoints([[124,18,128],[134,86,13],[117,31,196],[74,75,124],[191,61,125],[102,43,171],[154,48,176]], 0, 0.25, 0.005, val, vec,boneData,deletedInd, name=s) ## 18958spacing08-08-08 ## order: nase, kinn, stirn, jbl, jbr, ahl, ahr
# result = hks.calcHKSForPoints([[149,32,117],[159,74,13],[148,33,182],[98,68,111],[208,64,118],[125,45,153],[181,50,161]], 0, 5., 0.03, val, vec,boneData,deletedInd, name=s) ## 10781spacing08-08-08 ## order same as directly above
# result = hks.calcHKSForPoints([[149,32,117],[159,74,13],[148,33,182],[98,68,111],[208,64,118],[125,45,153],[181,50,161]], 0, 10., 0.03, val, vec,boneData,deletedInd, name=s) ## 10781spacing08-08-08only1s ## order same as directly above
# result = hks.calcHKSForPoints([[178,43,161],[168,76,20],[181,47,215],[106,79,145],[242,84,134],[154,53,194],[207, 58, 188]], 0, 10., 0.03, val, vec,boneData,deletedInd, name=s) ## 195358spacing07-07-07 ## order same as directly above
# result = hks.calcHKSForPoints([[178,43,161],[168,76,20],[181,47,215],[106,79,145],[242,84,134],[154,53,194],[207, 58, 188]], 0, 11., 0.03, val, vec,boneData,deletedInd, name=s+'takeMin') ## 195358spacing07-07-07only1s ## order same as directly above  ,[204,55,188]
#result = hks.calcHKSForPoints([[124,18,128],[134,86,13],[117,31,196],[74,75,124],[191,61,125],[102,43,171],[154,48,176]], 0, 5., 0.03, val, vec,boneData,deletedInd, name=s) ## 18958spacing08-08-08 ## order same as directly above
#result = hks.calcHKSForPoints([[221,44,140],[190,105,17],[239,44,216],[149,71,142],[273,119,133],[196,48,187],[254,68,183]], 0, 5., 0.03, val, vec,boneData,deletedInd, name=s) ## 54046spacing072-072-072
# result = hks.calcHKSForPoints([[159,29,166],[157,70,15],[158,33,205],[100,66,140],[214,62,139],[134,43,190],[181,42,189]], 0, 5., 0.03, val, vec,boneData,deletedInd, name=s) ## 924822spacing076-076-076 ## order same as directly above
# result = hks.calcHKSForPoints([[159,29,166],[157,70,15],[158,33,205],[100,66,140],[214,62,139],[134,43,190],[181,42,189]], 0, 10., 0.03, val, vec,boneData,deletedInd, name=s) ## 924822spacing076-076-076only1s ## order same as directly above
#um.saveToHKS('pointsHKSsigmamat'+s, result)
# result = hks.calcHKSForPoints([[0,0,0],[0,100,0],[0,50,0]], 0, 10., 0.03, val, vec,boneData,deletedInd, name=s) ## lemon
# result = hks.calcHKSForPoints([[240, 240, 190],[240, 240, 210]], 0, 10.5, 0.1, val, vec,boneData,deletedInd, name=s+'Mitte', printKernel=False) ## ambossDensity
# result = hks.calcHKSForPoints([[258, 240, 177],[258, 240, 223],[231, 256, 223], [231, 256, 177], [231, 224, 223], [231, 224, 177]], 0, 2., 0.05, val, vec,boneData,deletedInd, name=s+'Spitzen', printKernel=False) ## ambossDensity
# result = hks.calcHKSForPoints([[240, 240, 170],[240, 240, 230]], 0, 2.3, 0.05, val, vec,boneData,deletedInd, name=s+'Mitte' , printKernel=True) ## ambossBigDensity
# result = hks.calcHKSForPoints([[297, 240, 130],[297, 240, 270],[212, 191, 130], [212, 191, 270], [212, 288, 270], [212, 289, 130]], 0, 2.3, 0.05, val, vec,boneData,deletedInd, name=s+'Spitzen', printKernel=False) ## ambossBigDensity


#hks.plotHKSforPoints('pointsHKSsigmamat'+s)
# # print result
# # um.saveToHKS('completeHKSsigmamat'+s+'.hks', result)
# # hks.plotHKSforPoints('completeHKSsigmamat'+s+'.hks')
# # hks.singlePlot([[20,40,25], [35,51,32], [13,32,40], [33,11,47]], 0, 3.1, 0.05, val, vec, deletedInd, boneData)
# # hks.singlePlot([[6,0,2], [13,14,2], [47,0,2], [54,14,2]], 0, 3.1, 0.05, val, vec, delInd, sigmamatX6, True)
print "berechnung hks:"
print (time.time() - startTime), (time.time() - startTime)/60.

# data = um.readHKS(path+'/hksFiles/completeHKSsigmamat'+s)
# print data[4][0]
# data = um.readHKS(path+'/hksFiles/completeHKScompleteHKSsigmamat'+s)
# print data[4][0]

"""    Creating single plots for 3D testmatrizen    """
# s = 'sigmamatX6'
#createOperator3dim(sigmamatX6, s, 96, 33, 3)
# s = 'sigmamatX7'
# createOperator3dim(sigmamatX7, s, 2,2,2) #, saveCoo=False
# s = 'sigmamatX5'
# createOperator3dim(sigmamatX5, s, saveCoo=False)
s = 'sigmamatX4'

# delInd, data, rows, cols = createOperator3dim(sigmamatX4, s, 2,4,4)
# deletedInd = um.readBinList(path + "/deletedInds/delInd" +s, True)
# val = um.readList(path + "/eigvals/eigval" +s, False)
# dim_, numofVecs_, vec = um.readEigvec(path + "/eigvecs/eigvec" +s)
# result = hks.calcHKScomplete(sigmamatX6, 0.0, 10, 0.05, val, vec, deletedInd,name=s)
# result = hks.calcHKScomplete(sigmamatX7, 0.0, 5, 0.02, val, vec, deletedInd,name=s)
# result = hks.calcHKScomplete(sigmamatX5, 0.2, 5, 0.2, val, vec, deletedInd)
# result = hks.calcHKScomplete(sigmamatX4, 0.2, 2.5, 0.1, val, vec, deletedInd,name=s)
# hks.calcHKSForPoints([[6,0,1], [13,14,1], [47,0,1], [54,14,1]], 0, 10.1, 0.05, val, vec,sigmamatX6,deletedInd, name='hksFiles/sigmamatX6')
# hks.plotHKSforPoints('hksFiles/sigmamatX6')
# um.saveToHKS('completeHKS'+s, result)
# hks.plotHKSforPoints('completeHKS'+s)


# um.saveToHKS('completeHKSsigmamatX0.hks', result)



"""   Creating single plots for 2D   """
### careful: change variable sigmamat EVERYWHERE !!!!!!!!!!!!!!!!!1
s='sigmamat101'
s='sigmamat102'
#s='sigmamat101old'
#s='sigmamat12'
# delInd, val, vec = createOperatorAndEigData2dim(sigmamat102, s, takeMin=False)
#hks.calcHKSForPoints([[0,0],[1,0],[0,4],[6,4],[6,10]],0,4.5,0.05,val,vec,sigmamat3,delInd, name='hksFiles/'+s)
# hks.calcHKSForPoints([[2,0],[4,3],[7,0],[9,3]],0,6,0.02,val,vec,sigmamat102,delInd, name=s)
# hks.calcHKSForPoints([[2,0],[15,0],[4,6], [17,6]], 0, 7.5, 0.05, val, vec, sigmamat11, deletedInd, name='hksFiles/'+s)
#hks.calcHKSForPoints([[6,0],[10,14],[47,0], [51,14]],0,9,0.1,val,vec,sigmamat12,delInd, name='hksFiles/'+s)
# hks.plotHKSforPoints('hksFiles/sigmamat102')
#s = 'sigmamat7'
#delInd, val, vec = createOperatorAndEigData2dim(sigmamat7, s, takeMin=False)
#hks.calcHKScomplete(sigmamat7, 0.2, 4.5, 0.2, val, vec, delInd, name=s)

"""
profiling:
python -m cProfile -s time testMatrizen.py > output.txt
puts stdout in output (with all prints etc)
line profiling:
kernprof -l mainFile.py
for reading:
python -m line_profiler script_to_profile.py.lprof > output.txt
"""
