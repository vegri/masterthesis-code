all: 
	chromium-browser &
	google-chrome &
	wmctrl -s 1
	evince tex/build/notesHKS.pdf &
	texmaker tex/notesHKS.tex &
	wmctrl -s 2
	~/Desktop/pycharm.sh python/testMatrizen.py python/hks.py &
	disown -a
	wmctrl -s 0
