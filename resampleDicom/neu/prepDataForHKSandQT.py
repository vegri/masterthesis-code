# coding: utf-8
import numpy as np
import matplotlib.pyplot as plt
# import pickle
# from scipy.misc import toimage
import usefulMethods as um
import time
import os
import re
import sys


path = sys.argv[1]+'/'

def makeHistogramm(data, name, h_min=0, h_max=0, h_bins=76):
	#min value of histogram: h_min
	#max value of histogram: h_max
	#number of bins: h_bins
	if h_min==0:
		h_min=min(data)
	if h_max==0:
		h_max=max(data)
	bin_width=(h_max-h_min)/h_bins

	h_central_values=[]   	#empty list for central values of the bins
	h_entries=[] 		#empty list for number of entries corresponding to the individual bins

	for i in range(h_bins):
	    h_central_values.append(h_min+(i+0.5)*(h_max-h_min)/h_bins) #central value of the bins
	    h_entries.append(0)                               		#start with 0 entries in each bin

	###fill the histogram
	for x in data:                                            #go through all elements in data list
	    if (x>=h_min and x<h_max):                                #check if actual element fits into the range of our histogram
		h_entries[int(h_bins*(x-h_min)/(h_max-h_min))] += 1   #if so, increase the number of entries in the correct bin by one

	plt.plot(h_central_values, h_entries, color="blue", linestyle="steps-mid", linewidth=3)
	plt.plot(h_central_values, h_entries, 'ro', markersize=4, label='mean bin value')
	plt.legend(fontsize=10)
	plt.xlim(h_min - 5, h_max + 5)
	plt.ylabel("entries")
	# plt.xlabel(r"some parameter")
	# plt.suptitle("Verena Grimm", fontsize=14)
	plt.grid(b=True, which='both', color='0.65', linestyle='-')
	#plt.show()
	fig = plt.gcf()
	fig.savefig(name)#

def selectBoneAndSaveReadyForHKS(data, x, y, z, filename, only1s=False, lessDensity=False, save=True):
	counterBoneVertices = 0
	counterZeroVertices = 0
	startTime = time.time()
	#boneData = [0]*len(data)
	boneData = [x, y, z]
	path = '/home/vegrimm/Desktop/Masterarbeit/python/'
	for i in range(0, len(data)):
		tmp = data[i]
		if tmp < 550 or tmp > 2400:
			boneData.append(0)
			counterZeroVertices += 1
			#dummy=0
		else:
			counterBoneVertices += 1
			if only1s:
				boneData.append(1)
			elif lessDensity:
				boneData.append(int(round(tmp/10.)))
			else:
				boneData.append(tmp)
	print "anzahl nicht-null vertices: " + str(counterBoneVertices)
	print "anzahl null vertices: " + str(counterZeroVertices)
	print "raussuchen knochen für operator/hks:"
	print time.time() - startTime
	startTime = time.time()
	filename = filename[:-4] #cut off '.raw'
	if save:
		if only1s:
			um.saveBinList(path+filename+"only1s", boneData, isInt=True)
		elif lessDensity:
			um.saveBinList(path+filename+"lessDense", boneData, isInt=True)
		else:
			um.saveBinList(path+filename, boneData, isInt=True)  
		print "speichern für operator/hks: " + str(time.time() - startTime)
	return boneData, counterBoneVertices

def selectBoneAndSaveReadyForQT(data, filename, counterBoneVertices):
	startTime = time.time()
	boneData = data.reshape((z, y, x))
	counterBoneVertices2 = 0
	path = "/home/vegrimm/Desktop/Masterarbeit/qtTest/"
	filename = filename[:-4] #cut off '.raw'
	with open(path+filename+'.txt','w') as file:
		file.write(str(counterBoneVertices))
		file.write('\n')
		for zval in range(0,len(boneData)):
			for yval in  range(0,len(boneData[0])):
				for xval in  range(0,len(boneData[0][0])):
					density = boneData[zval][yval][xval]
					if density >= 550 and density <= 2400:  # 550
						counterBoneVertices2 +=1
						#file.write(str(xval/300.) + " " + str(yval/300.) + " " + str(zval/300.) + " " + str(density))
						file.write(str(xval) + " " + str(yval) + " " + str(zval) + " " + str(density))
						file.write('\n')
	print "raussuchen knochen für QT:"			
	print time.time() - startTime
	print "counterBoneVertices2:"
	print counterBoneVertices2


############################################################################################################################################################
############################################################################################################################################################


""" read dimension values from mhd file and save name of raw file """
x,y,z=0,0,0
for filename in os.listdir(path):
	#print filename
	#if re.match("[\w_-]+.mhd", filename):
	if '.mhd' in filename:
		print filename
		with open(os.path.join(path, filename), 'r') as f:
			for line in f:
				if 'DimSize' in line:
					p = re.compile(r'([\d]+)\s([\d]+)\s([\d]+)')
					dimList = p.search(line).groups()
					x=int(dimList[0])
					y=int(dimList[1])
					z=int(dimList[2])
	#if re.match("[\w_-]+.raw", filename):
	if '.raw' in filename:
		print "dataname found: " + filename
		dataname = filename
print x, y, z

""" read data from raw file """
data=0
data = np.fromfile(path+dataname, dtype=np.int16)
#boneData = data.reshape((z, y, x))

#print boneData[244][400]

print "reading with np.fromfile done"
#von 0 bis x*y, also die erste lage; jede weitere lage ist von [x*y:2*x*y] zu erreichen, etc. das ganze 138 mal, da 138 lagen zu verfügung stehen
# xtimesy=x*y
#slice = data[40*x:(xtimesy*41)] 
# print "min: " + str(min(data))  #### 
# print "max: " + str(max(data))  #### 


""" save pictures to decide where head begins """
#xtimesy=x*y
## for i in range(0, len(data), xtimesy): #len(data)
## 	slice = data[i:(i+xtimesy)]
## 	slice = slice.reshape((y, x))
## 	plt.imshow(slice)
## 	##plt.show()
## 	s = "bild" + str(counter)+".png"
## 	fig = plt.gcf()
## 	fig.savefig(s)
## 	counter +=1
## 	##plt.clf()
## 	##print str(i) + " : " + str(i+xtimesy)
#s = "bild" + str('GIANT2')+"kopie.png"
#slice = data[407*xtimesy:408*xtimesy]
#slice = slice.reshape((y, x))
##print slice
#for i in range(0,x,8):
#	#for i in range(0,31):
#	print slice[250][i:(i+8)]
##print "size of slice: " + str(len(slice))
##print xtimesy
##for j in range(0, 10):
#for i in range(0, x):
#	slice[250][i]=0
#	
#plt.imshow(slice)
#fig = plt.gcf()
#fig.savefig(s)
#plt.clf()

""" draw histogram """
#makeHistogramm(data, -2400, 1900, 76)


""" select bone values and save them for calculating operator/hks"""
boneData, counterBoneVertices = selectBoneAndSaveReadyForHKS(data, x, y, z, dataname, only1s=True, lessDensity=False, save=True)
##print "boneData selected, doing histogram"
##makeHistogramm(data, dataname[:-4]+'-histo', h_max=2500)




#counterBoneVertices= 931823
""" save data in a format that it can be displayed with qt (CT format) """
#selectBoneAndSaveReadyForQT(data, dataname, counterBoneVertices)



""" test reshape function and how data is drawn. result: default is reshape(z,y,x), as x-axis is fastest changing
axis in my case and results in correct reshaped sigmamat; furthermore, it is exactly drawn the way it is supposed
to be (x to the right, y going down)"""
# sigmamat = np.array([
# [[2,2,2,2,2],
#  [0,0,0,0,0],
#  [0,0,0,0,0],
#  [0,0,0,0,0]],
# [[2,2,2,2,2],
#  [0,2,2,2,0],
#  [0,2,2,2,0],
#  [0,0,0,0,0]],
# [[2,2,2,2,2],
#  [0,2,2,2,0],
#  [0,2,2,2,0],
#  [0,0,2,0,0]],
# [[2,2,2,2,2],
#  [0,2,2,2,0],
#  [0,2,2,2,0],
#  [0,0,0,0,0]],
# [[2,2,2,2,2],
#  [0,0,0,0,0],
#  [0,0,0,0,0],
#  [0,0,0,0,0]]
# ])
#
# sigmamat2 = np.array([
# 2,2,2,2,2,
#  0,0,0,0,0,
#  0,0,0,0,0,
#  0,0,0,0,0,
# 2,2,2,2,2,
#  0,2,2,2,0,
#  0,2,2,2,0,
#  0,0,0,0,0,
# 2,2,2,2,2,
#  0,2,2,2,0,
#  0,2,2,2,0,
#  0,0,2,0,0,
# 2,2,2,2,2,
#  0,2,2,2,0,
#  0,2,2,2,0,
#  0,0,0,0,0,
# 2,2,2,2,2,
#  0,0,0,0,0,
#  0,0,0,0,0,
#  0,0,0,0,0
# ])

# sigmamat2 = sigmamat2.reshape((5,4,5))
# print sigmamat2

# for i in range(0, len(sigmamat2), 20):
# 	slice = sigmamat2[i:(i + 20)]
# 	slice = slice.reshape((4, 5))
# 	print slice
# 	print""
# 	plt.imshow(slice)
# 	##plt.show()
# 	s = "sigmamat2" + str(i) + ".png"
# 	fig = plt.gcf()
# 	fig.savefig(s)
# 	counter += 1


""" save pictures of bone data """
# boneData = np.fromfile("boneData.npy", dtype=np.int16)

# for i in range(0, len(boneData), x): #len(data)
	# slice = boneData[i:(i+x)]
	# slice = slice.reshape((119, 119))
	# plt.imshow(slice)
	# ##plt.show()
	# s = "bone" + str(i)+".png"
	# fig = plt.gcf()
	# fig.savefig(s)
	# counter +=1
#	plt.clf()
	#print str(i) + " : " + str(i+x)


""" reshape bone data and save it""" #note: reshaping doesnt work. shape is not saved to file!
# with open("boneData.npy", "rb") as file_:
# 	boneData = np.load(file_)
# #boneData = np.fromfile("boneData.npy", dtype=np.int16) ### not working! array is in size larger than before (header?)
# print len(boneData)
# print 96*119*119
# print len(boneData)/float(x)
# boneDataNP = boneData.reshape((96,119,119))
# np.save('boneData', boneDataNP)



