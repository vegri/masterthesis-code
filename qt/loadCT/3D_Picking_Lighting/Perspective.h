#ifndef MAPS_H
#define MAPS_H

#include <QMainWindow>
#include <QGLWidget>
#include <QGridLayout>
#include <QFrame>
#include <vector>
#include <iostream>

#include "vecmath.h"

#if _MSC_VER
#include <gl/glu.h>
#elif __APPLE__
#include <OpenGL/glu.h>
#define RETINA_DISPLAY 1
#else
#include <GL/glu.h>
#endif

#define PI 3.14159265f

class CGView;

struct voxelAndHKS{
        Vector3d coordinates;
        std::vector<double> hks;
        void print(){
            std::cout << "x: " << coordinates.x() << ", y: " << coordinates.y() << ", z: " << coordinates.z() << std::endl;
            for(unsigned int i =0; i < hks.size(); i++){
                std::cout << hks[i] << " - ";
            }
            std::cout << std::endl;
        }
    };


class CGMainWindow : public QMainWindow {
    Q_OBJECT

public:

    CGMainWindow (QWidget* parent = 0, Qt::WindowFlags flags = Qt::Window);
    ~CGMainWindow ();

public slots:

    void loadCT();
    void loadHKS();
    void loadFeat();


protected:

    void keyPressEvent(QKeyEvent*);

private:

    CGView *ogl;
    void loadCT(QString filename);
};

class CGView : public QGLWidget {
    Q_OBJECT

public:

    CGView(CGMainWindow*,QWidget*);
    void initializeGL();

    /** transforms the picture coords (x,y,z) to world coords by
        inverting the projection and modelview matrix (as it it is
        after invocation of paintGL) and stores the result in v */
    void rayFromPixel(int x, int y, Vector3d &u, Vector3d &v);
    int  findClosestVertex(const Vector3d u, const Vector3d v, double epsPow2); // epsPow2 = epsilon^2
    int  findClosestTriangle(const Vector3d u, const Vector3d v, double epsPow2);
    double HeatmapColor(double min, double max, double val);



    Vector3d min, max, center;	// min, max and center of the coords of the loaded model
    int kernel=0;
    int numofKernels, maxDensity, minDensity;
    int xmin,ymin,zmin,xmax,ymax,zmax;
    double zoom;
    double phi,theta;
    double minKernel, maxKernel;
    std::vector<Vector3d> coord;			// the coords of the loaded CT
    std::vector<Vector3d> coordFeat;			// the coords of the loaded feature model
    std::vector<int> density;			// density values accompanying CT data
    std::vector<std::vector<int> > ifs;		// the faces of the loaded model, ifs[i] contains the indices of the i-th face
    std::vector<voxelAndHKS> voxelsWithKernels;
    Vector3d schwerpunkt;
    Vector3d clickedVertex;

protected:

    void paintGL();
    void resizeGL(int,int);

    void mouseMoveEvent(QMouseEvent*);
    void mousePressEvent(QMouseEvent*);
    void mouseReleaseEvent(QMouseEvent*);
    void wheelEvent(QWheelEvent*);

    CGMainWindow *main;
    int oldX,oldY;
    int selectedVertex;  // Index des ge-picked-ten Vertex
    int selectedTriangle;  // Index des ge-picked-ten Vertex

};

#endif
