TEMPLATE = app
TARGET = Perspective
QT += gui opengl
CONFIG += console

HEADERS += Perspective.h vecmath.h
SOURCES += Perspective.cpp vecmath.cpp

macx: QMAKE_MAC_SDK =  macosx10.11
win32:{LIBS += -lopengl32 -lglu32}
win64:{LIBS += -lopengl32 -lglu32}
unix:!macx {LIBS += -lGLU}
