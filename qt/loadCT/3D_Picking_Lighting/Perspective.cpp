#include <QApplication>
#include <QMenu>
#include <QMenuBar>
#include <QStatusBar>
#include <QFileDialog>
#include <QGLWidget>
#include <QKeyEvent>
#include <QMessageBox>
#include <QHBoxLayout>
#include "Perspective.h"

#include <cmath>
#include <fstream>
#include <sstream>
#include <iostream>
#include <limits>

#ifdef max
#undef max
#endif



CGMainWindow::CGMainWindow (QWidget* parent, Qt::WindowFlags flags)
    : QMainWindow (parent, flags) {
    resize (600, 600);


    // Create a menu
    QMenu *file = new QMenu("&File",this);
    file->addAction ("Load CT", this, SLOT(loadCT()), Qt::CTRL+Qt::Key_C);
    file->addAction ("Load Features", this, SLOT(loadFeat()), Qt::CTRL+Qt::Key_F);
    file->addAction ("Load HKS", this, SLOT(loadHKS()), Qt::CTRL+Qt::Key_D);
    file->addAction ("Quit", qApp, SLOT(quit()), Qt::CTRL+Qt::Key_Q);

    menuBar()->addMenu(file);

    // Create a nice frame to put around the OpenGL widget
    QFrame* f = new QFrame (this);
    f->setFrameStyle(QFrame::Sunken | QFrame::Panel);
    f->setLineWidth(2);

    // Create our OpenGL widget
    ogl = new CGView (this,f);

    // Put the GL widget inside the frame
    QHBoxLayout* layout = new QHBoxLayout();
    layout->addWidget(ogl);
    layout->setMargin(0);
    f->setLayout(layout);

    setCentralWidget(f);

    statusBar()->showMessage("Ready",1000);

    loadCT("/work/Masterarbeit/qtTest/ambossBigDensity547.txt");
}

CGMainWindow::~CGMainWindow () {}



void CGMainWindow::loadCT(QString filename) {

    ogl->voxelsWithKernels.clear();
    ogl->coord.clear();
    ogl->coordFeat.clear();
    ogl->schwerpunkt = 0;
    ogl->min = +std::numeric_limits<double>::max();
    ogl->max = -std::numeric_limits<double>::max();
    ogl->minDensity = +std::numeric_limits<int>::max();
    ogl->maxDensity = -std::numeric_limits<int>::max();
    ogl->xmin = +std::numeric_limits<int>::max();
    ogl->ymin = +std::numeric_limits<int>::max();
    ogl->zmin = +std::numeric_limits<int>::max();
    ogl->xmax =-std::numeric_limits<int>::max();
    ogl->ymax =-std::numeric_limits<int>::max();
    ogl->zmax =-std::numeric_limits<int>::max();


    std::cout << "loading file " << filename.toStdString() << std::endl;

    statusBar()->showMessage ("Loading CT ...");

    std::ifstream file(filename.toLatin1());
    int numOfVertices = 0;
    int counterZeroVertices = 0;

    file >> numOfVertices;
    std::cout << "number of vertices : " << numOfVertices << std::endl;

    ogl->coord.resize(numOfVertices);
    ogl->density.resize(numOfVertices);

    for(int i=0;i<numOfVertices;i++) {
        file >> ogl->coord[i][0] >> ogl->coord[i][1] >> ogl->coord[i][2] >> ogl->density[i];
        ogl->schwerpunkt += Vector3d(ogl->coord[i][0], ogl->coord[i][1], ogl->coord[i][2]);

        for(int j=0;j<3;++j) { //find min and max values
            if (ogl->coord[i][j] < ogl->min[j]) ogl->min[j] = ogl->coord[i][j];
            if (ogl->coord[i][j] > ogl->max[j]) ogl->max[j] = ogl->coord[i][j];
        }
        //find min/max values in every dimension
        if (ogl->coord[i][0] < ogl->xmin){
            ogl->xmin = ogl->coord[i][0];
        }
        if (ogl->coord[i][0] > ogl->xmax){
             ogl->xmax = ogl->coord[i][0];
        }
        if (ogl->coord[i][1] < ogl->ymin){
            ogl->ymin = ogl->coord[i][1];
         }
        if (ogl->coord[i][1] > ogl->ymax){
            ogl->ymax = ogl->coord[i][1];
        }
        if (ogl->coord[i][2] < ogl->zmin){
           ogl->zmin = ogl->coord[i][2];
        }
        if (ogl->coord[i][2] > ogl->zmax){
            ogl->zmax = ogl->coord[i][2];
        }

        if (ogl->density[i]==0){
            counterZeroVertices++;
        }
        if (ogl->density[i]<ogl->minDensity){
            ogl->minDensity = ogl->density[i];
        }
        if (ogl->density[i]>ogl->maxDensity){
            ogl->maxDensity = ogl->density[i];
        }
    }

    file.close();

    std::cout << "counterZeroVertices: " << counterZeroVertices << std::endl;
    std::cout << "minDensity: " << ogl->minDensity << std::endl;
    std::cout << "maxDensity: " << ogl->maxDensity << std::endl;

    std::cout << "schwerpunkt : " << ogl->schwerpunkt[0] << ", " << ogl->schwerpunkt[1] << ", " << ogl->schwerpunkt[2] << std::endl;
    ogl->schwerpunkt = ogl->schwerpunkt/numOfVertices;

    for(int i=0;i<numOfVertices;i++) {
        ogl->coord[i] -= ogl->schwerpunkt;
    }

    ogl->min -= ogl->schwerpunkt;
    ogl->max -= ogl->schwerpunkt;

    Vector3d extent = ogl->max - ogl->min;
    ogl->zoom = 1.5/extent.maxComp();
    ogl->center = (ogl->min+ogl->max)/2;
    std::cout << std::endl;
    ogl->updateGL();

    statusBar()->showMessage ("Loading CT done.",3000);
}

void CGMainWindow::loadCT() {
    QString filename = QFileDialog::getOpenFileName(this, "Load CT ...", QString(), "text files (*.txt)" ); //All files (*.*)
    if (filename.isEmpty()) return;
    loadCT(filename);
}

void CGMainWindow::loadFeat(){
    ogl->voxelsWithKernels.clear();
    ogl->coord.clear();
    ogl->coordFeat.clear();
    ogl->density.clear();
    ogl->schwerpunkt = 0;
    int numOfVertices= 0;

    QString filename = QFileDialog::getOpenFileName(this, "Load Features ...", QString(), "text files (*.feat)" ); // "All files (*.*)
    if (filename.isEmpty()) return;
    std::cout << "loading file " << filename.toStdString() << std::endl;
    statusBar()->showMessage ("Loading Features ...");

    std::ifstream file(filename.toLatin1());

    file >> numOfVertices;
    std::cout << "number of vertices : " << numOfVertices << std::endl;

    ogl->coordFeat.resize(numOfVertices);
    ogl->density.resize(numOfVertices);

    for(int i=0;i<numOfVertices;i++) {
        file >> ogl->coordFeat[i][0] >> ogl->coordFeat[i][1] >> ogl->coordFeat[i][2] >> ogl->density[i];
        ogl->schwerpunkt += Vector3d(ogl->coordFeat[i][0], ogl->coordFeat[i][1], ogl->coordFeat[i][2]);
    }

    file.close();

        std::cout << "schwerpunkt : " << ogl->schwerpunkt[0] << ", " << ogl->schwerpunkt[1] << ", " << ogl->schwerpunkt[2] << std::endl;
        ogl->schwerpunkt = ogl->schwerpunkt/numOfVertices;

        for(int i=0;i<numOfVertices;i++) {
        ogl->coordFeat[i] -= ogl->schwerpunkt;
        }
        ogl->zoom = 0.05;
        ogl->updateGL();
        statusBar()->showMessage ("Loading Features done.",3000);
}

void CGMainWindow::loadHKS() {

    ogl->voxelsWithKernels.clear();
    ogl->coord.clear();
    ogl->coordFeat.clear();
    ogl->schwerpunkt = 0;
    QString filename = QFileDialog::getOpenFileName(this, "Load hks ...", QString(), "All files (*.*)" );
    if (filename.isEmpty()) return;
    std::cout << "loading file " << filename.toStdString() << std::endl;
    statusBar()->showMessage ("Loading heat kernel signature ...");

    //std::cout << "testpoint 1 " << std::endl;
    std::ifstream file (filename.toStdString(), std::ios::in|std::ios::binary);
        int dimension, numofVoxNotZero;
        std::vector<double> timePoints;

        if (file.is_open()){

            file.read ((char*)&dimension, sizeof(dimension));
            std::cout << "dimension: " << dimension << std::endl;

            file.read ((char*)&numofVoxNotZero, sizeof(numofVoxNotZero));
            std::cout << "num of voxels not zero: " << numofVoxNotZero << std::endl;

            file.read ((char*)&ogl->numofKernels, sizeof(ogl->numofKernels));
            std::cout << "num of kernels: " << ogl->numofKernels << std::endl;

            timePoints.resize(ogl->numofKernels);
            file.read ((char*)timePoints.data(), timePoints.size() * sizeof(timePoints[0]));
            for(unsigned int i; i<timePoints.size(); i++){
                std::cout << timePoints[i] << ", " ;
            }
            std::cout << std::endl;

            ogl->voxelsWithKernels.resize(numofVoxNotZero);
            for(int i  = 0; i<numofVoxNotZero; i++){
                voxelAndHKS tmp;
                tmp.hks.resize(ogl->numofKernels);
                int x, y, z;
                //file.read ((char*)&tmp.x, sizeof(tmp.x));
                file.read ((char*)&x, sizeof(x));
                file.read ((char*)&y, sizeof(y));
                file.read ((char*)&z, sizeof(z));
                tmp.coordinates = Vector3d(x,y,z);
                ogl->schwerpunkt += Vector3d(x, y, z);
                file.read ((char*)tmp.hks.data(), tmp.hks.size() * sizeof(tmp.hks[0]));
                ogl->voxelsWithKernels[i]=tmp;
            }

            file.read ((char*)&ogl->minKernel, sizeof(ogl->minKernel));
            file.read ((char*)&ogl->maxKernel, sizeof(ogl->maxKernel));
            std::cout << "minkernel: " << ogl->minKernel << std::endl;
            std::cout << "maxkernel: " << ogl->maxKernel << std::endl;

            file.close();
            ogl->schwerpunkt = ogl->schwerpunkt/numofVoxNotZero;

            for(int i=0;i<numofVoxNotZero;i++) {
                ogl->voxelsWithKernels[i].coordinates[0] -= ogl->schwerpunkt.x();
                ogl->voxelsWithKernels[i].coordinates[1] -= ogl->schwerpunkt.y();
                ogl->voxelsWithKernels[i].coordinates[2] -= ogl->schwerpunkt.z();
            }
        }
        else std::cout << "Unable to open file";
        ogl->updateGL();
}


void CGMainWindow::keyPressEvent(QKeyEvent* event) {
    switch(event->key()) {
    case Qt::Key_Plus:
        if(ogl->kernel < ogl->numofKernels-1){
           ogl->kernel++;
        } else{
            ogl->kernel=0;
        }
        std::cout << "kernel:" << ogl->kernel << std::endl; break;
    case Qt::Key_Minus:
        if (ogl->kernel != 0){
            ogl->kernel--;
        } else{
            ogl->kernel = ogl->numofKernels-1;
        }
        std::cout << "kernel:" << ogl->kernel << std::endl; break;
    }
    ogl->updateGL();
}

CGView::CGView (CGMainWindow *mainwindow,QWidget* parent ) : QGLWidget (parent) {
    main = mainwindow;
}


void CGView::initializeGL() {
    zoom = 1.0;
    phi = -20.7;
    theta = 19.47;
    center = 0.0;
    selectedVertex = -1;
    selectedTriangle = -1;

    GLfloat mat_diffuse[] = { 1.0, 0.0, 1.0, 1.0 };
    GLfloat mat_specular[] = { 0.0,0.0, 0.0, 0.0 };
    GLfloat mat_shininess[] = { 50.0 };

    GLfloat light_position[] = { 0.0, 0.0, 1.0, 0.0 };
    glClearColor (0.0, 0.0, 0.0, 0.0);
    glShadeModel (GL_FLAT);

    glMaterialfv(GL_FRONT, GL_DIFFUSE, mat_diffuse);
    glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
    glMaterialfv(GL_FRONT, GL_SHININESS, mat_shininess);
    glLightfv(GL_LIGHT0, GL_POSITION, light_position);

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glEnable(GL_DEPTH_TEST); // enables Depth Test
}


void CGView::paintGL() {
    glEnable(GL_NORMALIZE);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    gluLookAt(0,0,20,0,0,0,0,1,0);
    glRotated(theta,1.0,0.0,0.0);
    glRotated(phi,0.0,1.0,0.0);
    glScaled(zoom,zoom,zoom);
    glTranslated(-center[0],-center[1],-center[2]);

    glClearColor(1,1,1,1.0);
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

     // coordinate system
    glDisable(GL_LIGHTING);
    glBegin(GL_LINES);
        glColor3d(1,0,0); // rot, x-Achse positiv
        glVertex3d(500,0.0,0.0);
        glVertex3d(0,0.0,0.0);
    glEnd();
    glBegin(GL_LINES);
        glColor3d(0.5,0,0); // dunkelrot, x-Achse negativ
        glVertex3d(0,0.0,0.0);
        glVertex3d(-500,0.0,0.0);
    glEnd();
    glBegin(GL_LINES);
        glColor3d(0,0,1); // blau, y-Achse positiv
        glVertex3d(0.0,500,0.0);
        glVertex3d(0.0,0,0.0);
    glEnd();
    glBegin(GL_LINES);
        glColor3d(0,0,0.5); // dunkelblau, y-Achse positiv
         glVertex3d(0.0,0.0,0.0);
        glVertex3d(0.0,-500,0.0);
    glEnd();
    glBegin(GL_LINES);
        glColor3d(0,1,0); // gruen, z-Achse positiv
        glVertex3d(0.0,0.0,500);
        glVertex3d(0.0,0.0,0);
    glEnd();
    glBegin(GL_LINES);
        glColor3d(0,0.5,0); // dunkelgruen, z-Achse positiv
        glVertex3d(0.0,0.0,0);
        glVertex3d(0.0,0.0,-500);
    glEnd();

    if (clickedVertex!=NULL){
    glPointSize(10.0);
        glColor3d(0,1,0);
        glBegin(GL_POINTS);
            glVertex3dv(clickedVertex.ptr());
        glEnd();
       }

    if (!coordFeat.empty()){
        glPointSize(15.0);
        glBegin(GL_POINTS);
        for(unsigned int j=0;j<coordFeat.size();j++){
            glColor4f(0,0,1, 0.25);
            if (density[j]==1){
                glColor3d(1,0,0);
                glVertex3dv(coordFeat[j].ptr());
            }

            else if (density[j]==2){
                glColor3d(1,1,0);
                glVertex3dv(coordFeat[j].ptr());
            }
            else{
                glVertex3dv(coordFeat[j].ptr());
               }
        }
        glEnd();
    }

    if (!coord.empty()){
        glPointSize(12.0);
        glPointSize(10.0);
        glBegin(GL_POINTS);
//        unsigned int intervalSize = coord.size()/4;
        float slope =  1.;
        float intercept = 0.;
        if (maxDensity!=minDensity){
            //std::cout << "maxDensity!=minDensity " << std::endl;
            //slope =  1./(maxDensity-minDensity);
            //intercept = -minDensity*slope;
            /*float intervalsize = (maxDensity-minDensity)/4;
            float a1 = 1/(intervalsize-minDensity);
            float b1 = minDensity/(minDensity-intervalsize);
            float a2 = -1/intervalsize; //1./(minDensity-intervalsize);
            float b2 = 2; //intervalsize/(intervalsize-minDensity)
            float a3 = 1/intervalsize;
            float b3 = -2; //2*intervalsize;
            float a4 = 1./(3*intervalsize-maxDensity);
            float b4 = maxDensity/(maxDensity-3*intervalsize);
            */
            for(unsigned int i=0;i<coord.size();++i){
                /*if (coord[i][0]+schwerpunkt.x()>212){  // für kleinen amboss: 231, für großen: 212
                int dens = density[i];
                if (dens<(minDensity+intervalsize)){
                    glColor4f(0,a1*dens+b1,1,1);
                    //std::cout << a1*dens+b1 << ", " << a2*dens+b2  << std::endl;
                }
                else if(dens >= (minDensity+intervalsize) && dens < (minDensity+2*intervalsize)) {
                    glColor4f(0,1,a2*dens+b2,1);
                }
                else if(dens >=(minDensity+2*intervalsize) && dens < (minDensity+3* intervalsize)) {
                    glColor4f(a3*dens+b3,1,0,1);
                }
                else{
                    glColor4f(1,a4*dens+b4,0,1);
                    //std::cout << "maxDensity!=minDensity " << std::endl;
                }*/
                //float colorFrag = slope*density[i]+intercept;
                //glColor4f(colorFrag,0,1-colorFrag, 0.25); //glColor4f(0,0,1, 0.5);
                glColor4f(0,0,1,0.25);
                glVertex3dv(coord[i].ptr());
                //               }
            }
        }
        else{
            glColor4f(0,0,1,0.25);
            for(unsigned int i=0;i<coord.size();++i){
                glVertex3dv(coord[i].ptr());
            }
        }
        //std::cout << "slope: " << slope << std::endl;
        //std::cout << "intercept " << intercept << std::endl;
        glEnd();
    }

    glPointSize(10.0);
    if (!voxelsWithKernels.empty()){
        double a = 1/(log(maxKernel)-log(minKernel));
        double c = -a*log(minKernel);
        glBegin(GL_POINTS);
        for(unsigned int i=0;i<voxelsWithKernels.size();++i){

            double colorFrag = a*log(voxelsWithKernels[i].hks[kernel])+c;
            glColor3d(colorFrag,0,1-colorFrag);
            glVertex3dv(voxelsWithKernels[i].coordinates.ptr());
        }
        glEnd();
    }

    glEnable(GL_LIGHTING);


}


void CGView::resizeGL(int width, int height) {
    glViewport(0,0,width,height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    if (width > height) {
        double ratio = width/(double) height;
        glFrustum(-ratio,ratio,-1.0,1.0,1.0,100.0);
    }
    else {
        double ratio = height/(double) width;
        glFrustum(-1.0,1.0,-ratio,ratio,1.0,100.0);
    }
    glMatrixMode (GL_MODELVIEW);
}


// returns in u and v: two points that are interpolated by the ray through
// pixel (x,y) and the camera center
// u and v are IN OBJECT COORDINATES !!!
// uese projection and modelview matrix (as they are after invocation of paintGL)
void CGView::rayFromPixel(int x, int y, Vector3d &u, Vector3d &v) {
#if RETINA_DISPLAY
    x *= 2;
    y *= 2;
#endif

    GLint viewport[4];
    glGetIntegerv(GL_VIEWPORT,viewport);

    float winZ;
    float winX = (float)x;
    float winY = (float)viewport[3] - (float)y;
    glReadPixels( winX, int(winY), 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, &winZ );

    GLdouble M[16], P[16];
    glGetDoublev(GL_PROJECTION_MATRIX,P);
    glGetDoublev(GL_MODELVIEW_MATRIX,M);

    // point on the object - if hit, or on the far plane otherwise
    gluUnProject(winX,winY,winZ,M,P,viewport,&v[0],&v[1],&v[2]);  //map window coordinates to object coordinates

    // point on the near plane (z=0)
    gluUnProject(winX,winY,0.0,M,P,viewport,&u[0],&u[1],&u[2]);
}




void CGView::mousePressEvent(QMouseEvent *event) {
    oldX = event->x();
    oldY = event->y();

    Vector3d u, v;
    rayFromPixel(event->x(), event->y(), u, v);
    int indClosestV = findClosestVertex(u, v, 1);
    if (indClosestV!=-1){
        if (!coord.empty()){
            clickedVertex = coord[indClosestV];
        }
        else if (!coordFeat.empty()){
            clickedVertex = coordFeat[indClosestV];
        }
        std::cout << "clickedVertex: " << clickedVertex.x() <<" "<< clickedVertex.y()<< " " << clickedVertex.z() << " " << density[indClosestV] << std::endl;
        std::cout << "original point: (" << (clickedVertex.x()+schwerpunkt.x()) <<", "<< (clickedVertex.y()+schwerpunkt.y()) << ", " << (clickedVertex.z()+schwerpunkt.z()) << ")" << std::endl;
        std::cout << "density: " << density[indClosestV] << std::endl;
    }

    updateGL();
}


// find the vertex on the model that is closer than eps to the ray and closest to the wiever
int CGView::findClosestVertex(const Vector3d u, const Vector3d v, double epsPow2)
{

    Vector3d d = v-u;
    d.normalize();


    double d_min = 10000;
    int i_min = -1;
    if (!coord.empty()){
        for(unsigned int i = 0 ; i < coord.size(); i++){
            Vector3d c = coord.at(i) - u;
            Vector3d h = c - d*c.dot(d);
            double h_len2 = h.lengthSquared();
            if( h_len2 < epsPow2)
                if (c.dot(d) < d_min){
                    i_min = i;
                    d_min = c.dot(d);
                }
        }
    }
    else if (!coordFeat.empty()){
        for(unsigned int i = 0 ; i < coordFeat.size(); i++){
            Vector3d c = coordFeat.at(i) - u;
            Vector3d h = c - d*c.dot(d);
            double h_len2 = h.lengthSquared();
            if( h_len2 < epsPow2)
                if (c.dot(d) < d_min){
                    i_min = i;
                    d_min = c.dot(d);
                }
        }
    }

    //std::cout << i_min << std::endl;
    return i_min;
}


void CGView::mouseReleaseEvent(QMouseEvent*) {}

void CGView::wheelEvent(QWheelEvent* event) {
    if (event->delta() < 0) zoom *= 1.1; else zoom *= 1/1.1;
    update();
}

void CGView::mouseMoveEvent(QMouseEvent* event) {
    phi += 0.2*(event->x()-oldX);
    theta += 0.2*(event->y()-oldY);
    oldX = event->x();
    oldY = event->y();

    updateGL();
}

int main (int argc, char **argv) {
    QApplication app(argc, argv);

    if (!QGLFormat::hasOpenGL()) {
        qWarning ("This system has no OpenGL support. Exiting.");
        return 1;
    }

    CGMainWindow *w = new CGMainWindow(NULL);

    w->show();

    return app.exec();
}

